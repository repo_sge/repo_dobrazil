﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface ITipoTalla
    {
        DataTable ListaTipoTalla(Int32 cod);
        DataTable ListaTipoTallaZ();
    }
}
