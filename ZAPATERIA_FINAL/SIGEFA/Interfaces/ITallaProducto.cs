﻿using SIGEFA.Entidades;
using System.Collections.Generic;
using System.Data;

namespace SIGEFA.Interfaces
{
    interface ITallaProducto
    {
        //registro de nueva talla para 1 producto
        int Insert(clsTallaProducto tallaproducto);
        //eliminacion de talla para 1 producto
        int Delete(clsTallaProducto tallaproducto);
        //lista tallas producto por idproducto
        List<clsTallaProducto> tallaxproducto(int idproducto);
        //metodo consulta tallas a travez de idtalla en tabla tallasxproducto, por producto en un almacen
        List<clsTalla> tallaxidtallaxproducto(int idproducto, int idalmacen);
        //actualiza stock cuando se registra nota de ingreso por inventario, opc=> 1 añade, 2 resta
        int actualizaStockTallaProducto(clsTallaProducto tallaproducto,decimal cantidad,int opc);
        //actualiza stock cuando se realiza una venta
        int actualizaStockTallaProductoVenta(clsTallaProducto tallaproducto, decimal cantidad);
        //muestra el stock de las tallas de un producto correspondiente
        decimal muestraStockTalla(clsTallaProducto tallaproducto);
        //lista tallas producto por idproducto x almacen
        List<clsTallaProducto> tallaxproductoxalmacen(int idproducto,int idalmacen);
    }
}
