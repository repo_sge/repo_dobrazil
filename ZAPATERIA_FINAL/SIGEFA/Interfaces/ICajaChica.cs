﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface ICajaChica
    {
        Boolean Insert(clsCajaChica NuevaCajaChica);
        Boolean Update(clsCajaChica CajaChica);
        Boolean Delete(Int32 Codigo);

        DataTable ListaCajaChica(Int32 codSucursal, Int32 tipo);
        DataTable ListaCajaChicaFechas(Int32 codSucursal, DateTime fecha1, DateTime fecha2, Int32 tipo);

        clsCajaChica CargaSaldoCajaChica(Int32 codSucursal);
        clsCajaChica VerificaSaldoCajaChica(Int32 codSucursal, Int32 tipo);

        Boolean RendicionCaja(Int32 Codigo);
        DataTable ListaRendiciones(Int32 codSucursal, Int32 tipo);
        DataTable ListaRendicionesXSucursal(Int32 codSucursal, Int32 tipo);
        DataTable ListaDetalleRendiciones(Int32 Codigo, Int32 tipo);
        Boolean AnulaRendicionCajaChica(Int32 Codigo);
       
        Boolean GestionaRendicionCajaChica(clsCajaChica CajaChica);
        Boolean GestionaRendicion(Int32 Codigo, String Observacion, Int32 Tipo);
        Boolean ApruebaRendicion(Int32 Codigo, String Comm);
        Boolean DesapruebaRendicion(Int32 Codigo, String Comm);

        //********************************************************
        Boolean InsertRendicion(clsCajaChica NuevaCajaChica);
        Boolean InsertDetalleRendicion(clsCajaChica NuevoDetalle);
        //********************************************************
        DataTable ListaRendicionesTesoreria();
        Boolean GeneraLiquidacion(clsCajaChica CajaChica);
        Boolean InsertLiquidacion(clsCajaChica CajaChica);
        DataTable ListaLiquidacionesVigentes(Int32 CodSucursal, Int32 tipo);


        //Implementado
        clsCajaChica VerificaSaldoCajaChicaDiaria(Int32 codSucursal, DateTime fecha1);
        DataTable ListaCajaChicaDiaria(Int32 codSucursal, DateTime fecha1);
        Boolean InsertMovimientoDiario(clsCajaChica NuevaCajaChica);
        Boolean UpdateMovimientoDiario(clsCajaChica CajaChica);
        Boolean DeleteMovimientoDiario(Int32 Codigo);
        Decimal SumaVentaEfectivoDia(Int32 codSucursal, DateTime fecha1);
        Boolean CerrarCaja(Int32 codSucursal, DateTime fecha1, Int32 tipConsulta);
        //Fin Implementado 

        DataTable ListaPagoCajaChica(Int32 tipo);
        Boolean CerrarCajaChica(Int32 codSucursal, Int32 tipo, Decimal montocierre, Int32 codcajachica);
        Decimal traersaldo();
        DataTable ListaDinero(Int32 tipo);
        Decimal TraeValor(Int32 codigo);
    }
}
