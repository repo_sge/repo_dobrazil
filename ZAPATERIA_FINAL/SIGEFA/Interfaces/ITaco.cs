﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface ITaco
    {
        DataTable ListaTaco();

        Boolean Delete(int Codtaco);

        Boolean Insert(clsTaco taco);

        Boolean Update(clsTaco taco);
    }
}
