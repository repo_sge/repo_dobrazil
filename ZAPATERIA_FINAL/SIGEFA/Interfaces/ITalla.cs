﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;
namespace SIGEFA.Interfaces
{
    interface ITalla
    {
        clsTalla CargaTallaDes(String talla);
        clsTalla CargaTallaCod(Int32 cod);
        DataTable MuestraTallaOC();
        DataTable ConsultaTallaCompra(Decimal tallamin, Decimal tallamax, Int32 tipot, Int32 line);
        DataTable ListaTallaporLinea(Int32 codline, Int32 min, Int32 max, Int32 tipot);
        clsTalla DevolverCodTalla(Int32 nom);

        clsTalla DevolverCodTalla(String nom, Int32 tipotalla);
    }
}
