﻿using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface IRepositorio
    {
        clsRepositorio listar_repositorio_xtscfm(clsRepositorio repositorio);
        int registrar_repositorio(clsRepositorio repositorio);
        int actualizar_repositorio(clsRepositorio repositorio);
        DataTable listar_repositorio_xtsfe(clsSerie numeracion, DateTime inicio, DateTime fin, int idestado);
        DataTable listar_repositorio_xtsfe_xcomprobante(clsSerie numeracion, clsFacturaVenta comprobante, String inicio, String fin);
        clsRepositorio listar_archivo_xrepositorio(clsRepositorio repositorio);
        clsRepositorio listar_xidrepositorio(int idrepositorio);
        int validarEnvio(int codfacturav);
    }
}
