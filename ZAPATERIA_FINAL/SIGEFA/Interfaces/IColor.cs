﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface IColor
    {
        DataTable ListaColorPrimario();
        DataTable ListaColorSecundario();
        Boolean VerificaColorSecundario(clsColorSecundario cols);
        DataTable ListaProductoZMarcaColor(Int32 codline, Int32 codmar, Int32 codColorS);
        Boolean VerificaColorPrimario(clsColorPrimario colp);

        Boolean deletePrimario(Int32 codcolor);
        Boolean insertPrimario(clsColorPrimario ser);
        Boolean updatePrimario(clsColorPrimario ser);

        Boolean deleteSecundario(Int32 codcolor);
        Boolean insertSecundario(clsColorSecundario ser);
        Boolean updateSecundario(clsColorSecundario ser);

    }
}
