﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System.Data;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmControlStock
    {
        IControlStock Myscontrol = new MysqlControlStock();

        public DataTable CargaControlStock()
        {
            try
            {
                return Myscontrol.CargaControlStock();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
}
