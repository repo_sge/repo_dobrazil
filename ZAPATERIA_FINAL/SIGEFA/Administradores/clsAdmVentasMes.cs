﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System.Data;

namespace SIGEFA.Administradores
{
    class clsAdmVentasMes
    {
        IVentasMes VentM = new MysqlVentasMes();

        public Int32 BuscarMes(Int32 Year)
        {
            try
            {
                return VentM.BuscarMes(Year);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public Boolean GuardaVentas(Int32 mespasado, Int32 anio, Int32 codalmacen, Int32 mesactual, Int32 dia)
        {
            try
            {
                return VentM.GuardaVentas(mespasado, anio, codalmacen, mesactual, dia);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataTable MostrarVentasMes(Int32 mes, Int32 anio, Int32 codalmacen)
        {
            try
            {
                return VentM.MostrarVentasMes(mes, anio, codalmacen);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
