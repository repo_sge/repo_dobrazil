﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace SYS_SGE.Controles.Librerias
{
    public class clsImagen
    {
        public static byte[] ImagenAbyte(Image Imagen)
        {
            MemoryStream memory = new MemoryStream();
            if (Imagen != null)
            {
                if (Imagen.RawFormat.Equals(ImageFormat.Jpeg)) { Imagen.Save(memory, ImageFormat.Jpeg); }
                // desde aqui modifique...
                else if (Imagen.RawFormat.Equals(ImageFormat.Gif)) { Imagen.Save(memory, ImageFormat.Gif); }
                else if (Imagen.RawFormat.Equals(ImageFormat.Bmp)) { Imagen.Save(memory, ImageFormat.Bmp); }
                else if (Imagen.RawFormat.Equals(ImageFormat.Png)) { Imagen.Save(memory, ImageFormat.Png); }
            }
            else
            {
               
            }
            return memory.ToArray();
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            return Image.FromStream(ms);
        }

        public static byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            if (imageIn.RawFormat.Equals(ImageFormat.Jpeg)) { imageIn.Save(ms, ImageFormat.Jpeg); }
            //desde aqui modifique...
            else if (imageIn.RawFormat.Equals(ImageFormat.Gif)) { imageIn.Save(ms, ImageFormat.Gif); }
            else if (imageIn.RawFormat.Equals(ImageFormat.Bmp)) { imageIn.Save(ms, ImageFormat.Bmp); }
            else if (imageIn.RawFormat.Equals(ImageFormat.Png)) { imageIn.Save(ms, ImageFormat.Png); }
            return ms.ToArray();
        }
    }
}
