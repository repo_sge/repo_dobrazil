namespace SIGEFA.Formularios
{
    partial class frmGuardaImagenProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGuardaImagenProducto));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDel_Fotografia = new System.Windows.Forms.Button();
            this.btnAdd_Fotografia = new System.Windows.Forms.Button();
            this.pbFoto = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnDel_Fotografia);
            this.groupBox1.Controls.Add(this.btnAdd_Fotografia);
            this.groupBox1.Controls.Add(this.pbFoto);
            this.groupBox1.Location = new System.Drawing.Point(98, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 239);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fotografía";
            // 
            // btnDel_Fotografia
            // 
            this.btnDel_Fotografia.Image = ((System.Drawing.Image)(resources.GetObject("btnDel_Fotografia.Image")));
            this.btnDel_Fotografia.Location = new System.Drawing.Point(238, 48);
            this.btnDel_Fotografia.Name = "btnDel_Fotografia";
            this.btnDel_Fotografia.Size = new System.Drawing.Size(23, 23);
            this.btnDel_Fotografia.TabIndex = 1;
            this.btnDel_Fotografia.UseVisualStyleBackColor = true;
            this.btnDel_Fotografia.Click += new System.EventHandler(this.btnDel_Fotografia_Click);
            // 
            // btnAdd_Fotografia
            // 
            this.btnAdd_Fotografia.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd_Fotografia.Image")));
            this.btnAdd_Fotografia.Location = new System.Drawing.Point(238, 19);
            this.btnAdd_Fotografia.Name = "btnAdd_Fotografia";
            this.btnAdd_Fotografia.Size = new System.Drawing.Size(23, 23);
            this.btnAdd_Fotografia.TabIndex = 0;
            this.btnAdd_Fotografia.UseVisualStyleBackColor = true;
            this.btnAdd_Fotografia.Click += new System.EventHandler(this.btnAdd_Fotografia_Click);
            // 
            // pbFoto
            // 
            this.pbFoto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbFoto.Location = new System.Drawing.Point(9, 19);
            this.pbFoto.Name = "pbFoto";
            this.pbFoto.Size = new System.Drawing.Size(223, 213);
            this.pbFoto.TabIndex = 38;
            this.pbFoto.TabStop = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.ImageIndex = 4;
            this.btnGuardar.ImageList = this.imageList1;
            this.btnGuardar.Location = new System.Drawing.Point(184, 312);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(78, 32);
            this.btnGuardar.TabIndex = 42;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // frmGuardaImagenProducto
            // 
            this.ClientSize = new System.Drawing.Size(463, 400);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Name = "frmGuardaImagenProducto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Imagen Producto";
            this.Load += new System.EventHandler(this.frmGuardaImagenProducto_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDel_Fotografia;
        private System.Windows.Forms.Button btnAdd_Fotografia;
        private System.Windows.Forms.PictureBox pbFoto;
        private System.Windows.Forms.ImageList imageList1;
        public System.Windows.Forms.Button btnGuardar;
    }
}