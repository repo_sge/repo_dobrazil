using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Entidades;
using SIGEFA.Administradores;

namespace SIGEFA.Formularios
{
    public partial class frmSerieProducto : DevComponents.DotNetBar.OfficeForm
    {
        public Int32 Proceso = 0; 
        clsSerieProducto ser = new clsSerieProducto();
        clsAdmSerieProducto admSerie = new clsAdmSerieProducto();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;

        public frmSerieProducto()
        {
            InitializeComponent();
        }

        private void frmSerieProducto_Load(object sender, EventArgs e)
        {
            CargarSerieProducto();
        }

        private void CargarSerieProducto()
        {
            try
            {
                dgvSerieProducto.DataSource = data;
                data.DataSource = admSerie.MuestraSerieProductos();
                data.Filter = String.Empty;
                filtro = String.Empty;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            groupBox2.Text = "Registro Nuevo";
            Proceso = 1;
        }

        private void CambiarEstados(Boolean Estado)
        {
            groupBox1.Visible = Estado;
            groupBox2.Visible = !Estado;
            btnGuardar.Enabled = !Estado;
            btnNuevo.Enabled = Estado;
            btnEditar.Enabled = Estado;
            btnEliminar.Enabled = Estado;
            btnReporte.Enabled = Estado;
            txtCodigo.Text = "";
            txtReferencia.Text = "";
            txtDescripcion.Text = "";
           
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            Proceso = 2;
            groupBox2.Text = "Editar Registro";
            txtCodigo.Text = ser.CodSerieProducto.ToString();
            txtReferencia.Text = ser.Referencia;
            txtDescripcion.Text = ser.Descripcion;
            txtMin.Text = ser.Tallamin.ToString();
            txtMax.Text = ser.Tallamax.ToString();
        }

        private void btnGrupos_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Proceso != 0 && txtDescripcion.Text != "")
                {
                   
                    ser.Referencia = txtReferencia.Text;
                    ser.Descripcion = txtDescripcion.Text;
                    ser.Tallamax = Convert.ToInt32(txtMax.Text);
                    ser.Tallamin = Convert.ToInt32(txtMin.Text);
                    if (Proceso == 1)
                    {
                        ser.CodUser = frmLogin.iCodUser;
                        if (admSerie.insert(ser))
                        {
                            MessageBox.Show("Los datos se guardaron correctamente", "Gestion Grupo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            CambiarEstados(true);
                            CargarSerieProducto();
                        }
                    }
                    else if (Proceso == 2)
                    {
                        ser.CodSerieProducto = Convert.ToInt32(txtCodigo.Text);
                        if (admSerie.update(ser))
                        {
                            MessageBox.Show("Los datos se guardaron correctamente", "Gestion Grupo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            CambiarEstados(true);
                            CargarSerieProducto();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
           
            
      
        }

        private void dgvSerieProducto_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (dgvSerieProducto.Rows.Count >= 1 && e.Row.Selected)
            {
                ser.CodSerieProducto = Convert.ToInt32(e.Row.Cells[codigo.Name].Value);                
                ser.Descripcion = e.Row.Cells[descripcion.Name].Value.ToString();
                ser.Tallamax = Convert.ToInt32(e.Row.Cells[tallamax.Name].Value);
                ser.Tallamin = Convert.ToInt32(e.Row.Cells[tallamin.Name].Value);
                btnEditar.Enabled = true;
                btnEliminar.Enabled = true;
            }
            else if (dgvSerieProducto.SelectedRows.Count == 0)
            {
                btnEditar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dlgResult = MessageBox.Show("Esta seguro que desea eliminar el producto seleccionado", "Notas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgResult == DialogResult.No)
            {
                return;
            }
            else
            {
                if (admSerie.delete(ser))
                {
                    MessageBox.Show("Los datos se guardaron correctamente", "Gestion Grupo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CambiarEstados(true);
                    CargarSerieProducto();
                }
            }
        }
    }
}