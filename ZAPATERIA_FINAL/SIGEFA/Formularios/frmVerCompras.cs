using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmVerCompras : DevComponents.DotNetBar.OfficeForm
    {

        public static BindingSource data = new BindingSource();
        clsAdmFactura admFac = new clsAdmFactura();
        clsAdmAperturaCierre AdmCaja = new clsAdmAperturaCierre();
        clsCaja Caja = new clsCaja();
        clsFactura fac = new clsFactura();
        clsPago pag = new clsPago();
        clsAdmPago admPago = new clsAdmPago();

        String filtro = String.Empty;

        public frmVerCompras()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmVerCompras_Load(object sender, EventArgs e)
        {
            CargaLista();
            
        }

        private void CambiaColor()
        {
            foreach (DataGridViewRow row in dgvFacturas.Rows)
            {
                DateTime fecha_vence = Convert.ToDateTime(row.Cells[fechapago.Name].Value);
                Int32 can = Convert.ToInt32(row.Cells[cancelado.Name].Value);
                if (fecha_vence <= Convert.ToDateTime(System.DateTime.Now.ToString()) && can == 0)
                {
                    row.DefaultCellStyle.BackColor = Color.Red;
                }
                if (can == 1)
                {
                    row.DefaultCellStyle.BackColor = Color.LemonChiffon;
                }
            }
        }

        private void CargaLista()
        {
            dgvFacturas.DataSource = data;
            data.DataSource = admFac.MuestraFactura(dtpDesde.Value.Date, dtpHasta.Value.Date, frmLogin.iCodAlmacen);
            CambiaColor();
            data.Filter = String.Empty;
            filtro = String.Empty;
        }

        private void dgvFacturas_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (dgvFacturas.Rows.Count >= 1 && e.Row.Selected)
            {
                fac.CodFactura = Convert.ToInt32(e.Row.Cells[codfactura.Name].Value);
            }
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
           /* if (dgvFacturas.CurrentRow != null)
            {
                DialogResult dglResult = MessageBox.Show("Esta seguro que desea anular la Factura seleccionada", "Facturación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dglResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    if (admFac.anular(fac.CodFactura))
                    {
                        MessageBox.Show("La Factura ha sido anulada correctamente", "Facturación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CargaLista();
                    }
                    else
                    {
                        MessageBox.Show("La Factura no se puede anular", "Facturación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }*/

            if (dgvFacturas.SelectedRows.Count > 0 && dgvFacturas.CurrentRow.Index != -1)
            {
                DataGridViewRow row = dgvFacturas.CurrentRow;
                DialogResult dlgResult = MessageBox.Show("Esta seguro que desea anular el documento seleccionado", "Notas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    pag = admPago.MuestraPagoVenta(frmLogin.iCodAlmacen, fac.CodFactura, 0);

                    if (pag != null && pag.FechaPago.Month == DateTime.Now.Month)
                    {

                        Caja = AdmCaja.ValidarAperturaDia(frmLogin.iCodSucursal, DateTime.Now.Date, 2, frmLogin.iCodAlmacen);

                        if (Caja.TotalDisponible != null)
                        {
                            if (admFac.anular(fac.CodFactura, frmLogin.iCodAlmacen, frmLogin.iCodUser))
                            {
                                MessageBox.Show("El documento ha sido anulado correctamente", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CargaLista();
                            }
                            else
                            {
                                MessageBox.Show("No se pudo anular la compra", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CargaLista();
                            }
                        }
                        else
                        {
                                    MessageBox.Show("Debe aperturar caja", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    CargaLista();

                        }


                    }
                    else
                    {
                        MessageBox.Show("Error al anular documento, mes ya cerrado", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void dgvFacturas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvFacturas.Rows.Count >= 1 && e.RowIndex != -1)
            {
                frmNotaIngresoAr form = new frmNotaIngresoAr();
                form.MdiParent = this.MdiParent;
                form.codFac = fac.CodFactura;
                form.Proceso = 3;
                form.Show();
            }
        }

        
    }
}