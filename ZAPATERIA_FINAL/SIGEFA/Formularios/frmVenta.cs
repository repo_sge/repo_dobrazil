﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging.Filters;
using CrystalDecisions.CrystalReports.Engine;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.Reportes;
using SIGEFA.Reportes.clsReportes;
using SIGEFA.SunatFacElec;
using Tesseract;

namespace SIGEFA.Formularios
{
    public partial class frmVenta : DevComponents.DotNetBar.Office2007Form
    {
        clsCaja aper = new clsCaja();
        clsAdmAperturaCierre AdmAper = new clsAdmAperturaCierre();
        clsReporteFactura ds = new clsReporteFactura();
        clsReporteFlujoCaja dsf = new clsReporteFlujoCaja();
        clsAdmTransaccion AdmTran = new clsAdmTransaccion();
        clsTransaccion tran = new clsTransaccion();
        clsAdmTipoDocumento Admdoc = new clsAdmTipoDocumento();
        clsTipoDocumento doc = new clsTipoDocumento();
        clsAdmSerie Admser = new clsAdmSerie();
        clsSerie ser = new clsSerie();
        clsAdmPedido Admped = new clsAdmPedido();
        clsPedido pedido = new clsPedido();
        clsAdmTipoCambio AdmTc = new clsAdmTipoCambio();
        clsTipoCambio tc = new clsTipoCambio();
        clsAdmProveedor AdmProv = new clsAdmProveedor();
        clsProveedor prov = new clsProveedor();
        clsAdmCliente AdmCli = new clsAdmCliente();
        public clsCliente cli = new clsCliente();
        clsAdmAutorizado AdmAut = new clsAdmAutorizado();
        clsAutorizado aut = new clsAutorizado();
        clsAdmNotaSalida AdmNota = new clsAdmNotaSalida();
        clsNotaSalida nota = new clsNotaSalida();
        clsAdmGuiaRemision AdmGuia = new clsAdmGuiaRemision();
        clsGuiaRemision guia = new clsGuiaRemision();
        clsAdmFormaPago AdmPago = new clsAdmFormaPago();
        clsFormaPago fpago = new clsFormaPago();
        clsListaPrecio Listap = new clsListaPrecio();
        clsAdmVendedor AdmVen = new clsAdmVendedor();
        clsFacturaVenta venta = new clsFacturaVenta();
        clsFacturaVenta factura = new clsFacturaVenta();
        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();
        clsMoneda moneda = new clsMoneda();
        clsAdmMoneda AdmMon = new clsAdmMoneda();
        clsAdmListaPrecio admLista = new clsAdmListaPrecio();
        clsValidar ok = new clsValidar();
        clsConsultasExternas ext = new clsConsultasExternas();
        clsCotizacion coti = new clsCotizacion();
        clsDetalleCotizacion detaCoti = new clsDetalleCotizacion();
        clsAdmCotizacion AdmCoti = new clsAdmCotizacion();
        clsAdmAlmacen Admalmac = new clsAdmAlmacen();
        clsPago Pag = new clsPago();
        clsAdmPago AdmPagos = new clsAdmPago();
        clsSeparacion separacion = new clsSeparacion();
        clsAdmTallaProducto admtallaproducto = new clsAdmTallaProducto();
        public List<Int32> config = new List<Int32>();
        public List<clsDetalleNotaSalida> detalle = new List<clsDetalleNotaSalida>();
        public List<clsDetalleFacturaVenta> detalle1 = new List<clsDetalleFacturaVenta>(); 
        public List<clsDetalleGuiaRemision> detalleg = new List<clsDetalleGuiaRemision>();
        public List<Int32> documento = new List<Int32>(); 
        public List<Int32> codsalida = new List<Int32>(); 
        private List<Int32> correlativo = new List<Int32>(); 
        private List<clsFacturaVenta> ltaventa = new List<clsFacturaVenta>();  
        private List<Int32> codpro = new List<Int32>();
        clsFormaPago forma = new clsFormaPago();
        public String CodNota, CodVenta;
        public Int32 CodTransaccion;
        public Int32 CodProveedor;
        public Int32 CodCliente;
        public Int32 CodDocumento;
        public Int32 CodSerie, CodSerieG=0, numG=0, manual=0;
        public String numSerie;
        public Int32 CodAutorizado;
        public Int32 CodPedido;
        public Int32 CodGuia;
        public Int32 Tipo;
        public Int32 codForma, codListaP;
        public Int32 Proceso = 0; //(1) Nuevo (2) Editar (3) Consulta
        public Int32 Procede = 0; //(1) Sin Guia (2)Con Guia
        public DataTable datoscarga2 = new DataTable();
        public DataTable datos = new DataTable();
        public Int32 tip;
        public Int32 CodVendedor;
        public Int32 CodSalConsulExt;
        public Int32 CodSeparacion = 0;

        public bool consultorext;

        public static BindingSource data = new BindingSource();

        Int32 CodLista = 0;
        Boolean Validacion = true;
        Decimal TipoCambio = 0, ret=0;
        public Int32 mon = 0;//MOD6

        public Int32 CodEmpresaTransporte;

        private String Salida = "";
        private Int32 codCotizacion;

        public String CodPago;
        public Int32 ventaok=0;

        clsVehiculoTransporte vehiculotransporte = new clsVehiculoTransporte();
        clsAdmVehiculoTransporte admVehiculoTransporte = new clsAdmVehiculoTransporte();
        clsConductor conductor = new clsConductor();
        clsAdmConductor admConductor = new clsAdmConductor();
        clsAdmEmpresaTransporte AdmET = new clsAdmEmpresaTransporte();
        clsAdmEmpresa admemp = new clsAdmEmpresa();
        clsEmpresaTransporte empT = new clsEmpresaTransporte();
        clsAdmTipoImpuesto admtimpuesto = new clsAdmTipoImpuesto();

        public Int32 CodigoCaja = 0;
        clsCaja Caja = new clsCaja();
        clsAdmAperturaCierre AdmCaja = new clsAdmAperturaCierre();

        public Facturacion facturacion = new Facturacion();

        clsReporteFactura ds1 = new clsReporteFactura();
        clsTipoDocumento tipdoc = new clsTipoDocumento();
        clsAdmTipoDocumento admtipdoc = new clsAdmTipoDocumento();

        Sunat MyInfoSunat;
        Reniec MyInfoReniec;
        IntRange red = new IntRange(0, 255);
        IntRange green = new IntRange(0, 255);
        IntRange blue = new IntRange(0, 255);

        private void VentaEnMoneda()//MOD6
        {
            Decimal TipoCambio = 0;

            TipoCambio = Convert.ToDecimal(txtTipoCambio.Text.Trim());

            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                if (mon == 1)
                {
                    if (Convert.ToInt32(cmbMoneda.SelectedValue) == 2)
                    {
                        row.Cells[preciounit.Name].Value = Convert.ToDecimal(row.Cells[preciounit.Name].Value) / TipoCambio;
                        row.Cells[importe.Name].Value = Convert.ToDecimal(row.Cells[importe.Name].Value) / TipoCambio;
                        row.Cells[montodscto.Name].Value = Convert.ToDecimal(row.Cells[montodscto.Name].Value) / TipoCambio;
                        row.Cells[valorventa.Name].Value = Convert.ToDecimal(row.Cells[valorventa.Name].Value) / TipoCambio;
                        row.Cells[igv.Name].Value = Convert.ToDecimal(row.Cells[igv.Name].Value) / TipoCambio;
                        row.Cells[precioventa.Name].Value = Convert.ToDecimal(row.Cells[precioventa.Name].Value) / TipoCambio;
                    }
                }
                else
                {
                    if (Convert.ToInt32(cmbMoneda.SelectedValue) == 1)
                    {
                        row.Cells[preciounit.Name].Value = Convert.ToDecimal(row.Cells[preciounit.Name].Value) * TipoCambio;
                        row.Cells[importe.Name].Value = Convert.ToDecimal(row.Cells[importe.Name].Value) * TipoCambio;
                        row.Cells[montodscto.Name].Value = Convert.ToDecimal(row.Cells[montodscto.Name].Value) * TipoCambio;
                        row.Cells[valorventa.Name].Value = Convert.ToDecimal(row.Cells[valorventa.Name].Value) * TipoCambio;
                        row.Cells[igv.Name].Value = Convert.ToDecimal(row.Cells[igv.Name].Value) * TipoCambio;
                        row.Cells[precioventa.Name].Value = Convert.ToDecimal(row.Cells[precioventa.Name].Value) * TipoCambio;
                    }
                }
            }
        }

        public frmVenta()
        {
            InitializeComponent();
        }

        public void frmVenta_Load(object sender, EventArgs e)
        {
            CargaBoleta();
            iniciaformulario();
            //CargaTransportista();
            //CargaVehiculoTrasnporte();            
            cargaTimpuesto();
        }

        private void cargaTimpuesto()
        {
            cbTimpuesto.DataSource =admtimpuesto.listar_tipoimpuesto_xestado();
            cbTimpuesto.DisplayMember = "descripcion";
            cbTimpuesto.ValueMember = "idtipoimpuesto";
            cbTimpuesto.SelectedIndex = 0;
        }

        private void CargaBoleta()
        {
            txtDocRef.Text = "BV";
            KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
            txtDocRef_KeyPress(txtDocRef, ee);
            //txtSerie.Text = "001";
            txtSerie_KeyPress(txtDocRef, ee);
            if (Proceso == 3)
            {
                txtCodigoCli.Text = factura.CodCliente.ToString();
            }
            else
            {
                txtCodigoCli.Text = cli.CodCliente.ToString();
            }
            
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDetalle.Text == "")
                {
                    RecorreDetalle();
                    if (Application.OpenForms["frmDetalleSalida"] != null)
                    {
                        Application.OpenForms["frmDetalleSalida"].Activate();
                    }
                    else
                    {
                        frmDetalleSalida form = new frmDetalleSalida();
                        form.Procede = 2;
                        form.Proceso = 1;
                        form.consultorext = checkBox1.Checked;
                        if (checkBox1.Checked == true)
                        {
                            form.CodVendedor = CodVendedor;
                            form.Procede = 42;
                            form.Proceso = 1;
                            form.consultorext = checkBox1.Checked;
                        }
                        form.Tipo = 2;
                        form.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                        form.Codlista = Convert.ToInt32(cbListaPrecios.SelectedValue);
                        form.tc = tc.Compra;
                        form.productoscargados = detalle1;
                        form.alma = Convert.ToInt32(cmbAlmacen.SelectedValue);
                        form.ShowDialog();

                    }
                }
                else { MessageBox.Show("No Puede Seguir Agregando más Detalles", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information); }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvDetalle.Rows.Count > 0 & dgvDetalle.SelectedRows.Count > 0)
            {
                
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow);
                    calculatotales();
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvDetalle.Rows.Count > 0 & dgvDetalle.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dgvDetalle.SelectedRows[0];
                if (Application.OpenForms["frmDetalleSalida"] != null)
                {
                    Application.OpenForms["frmDetalleSalida"].Activate();
                }
                else
                {
                    frmDetalleSalida form = new frmDetalleSalida();
                    form.Proceso = 2;
                    form.Procede = 2;
                    form.alma = Convert.ToInt32(cmbAlmacen.SelectedValue);
                    form.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                    form.tc = Convert.ToDecimal(txtTipoCambio.Text);
                    form.Codlista = Convert.ToInt32(cbListaPrecios.SelectedValue);
                    form.txtCodigo.Text = row.Cells[codproducto.Name].Value.ToString();
                    form.txtReferencia.Text = row.Cells[referencia.Name].Value.ToString();
                    form.BuscaProducto();
                    form.txtControlStock.Text = row.Cells[serielote.Name].Value.ToString();
                    form.txtCantidad.Text = String.Format("{0:#,##0.00}",row.Cells[cantidad.Name].Value);
                    form.txtPrecio.Text = String.Format("{0:#,##0.00}", row.Cells[preciounit.Name].Value);
                    form.txtDscto1.Text = String.Format("{0:#,##0.00}",row.Cells[dscto1.Name].Value);
                    form.txtPrecioNeto.Text = String.Format("{0:#,##0.00}",row.Cells[importe.Name].Value);
                    form.ShowDialog();
                }
            }
        }

        private void txtTransaccion_KeyDown(object sender, KeyEventArgs e)
        {
            if (!txtTransaccion.ReadOnly)
            {
                if (e.KeyCode == Keys.F1)
                {
                    if (Application.OpenForms["frmTransacciones"] != null)
                    {
                        Application.OpenForms["frmTransacciones"].Activate();
                    }
                    else
                    {
                        frmTransacciones form = new frmTransacciones();
                        form.Proceso = 4;
                        form.ShowDialog();
                        if (CodTransaccion != 0)
                        {
                            CargaTransaccion();
                            ProcessTabKey(true);
                        }
                    }

                }
            }
            
        }

        private void CargaTransaccion()
        {
            tran = AdmTran.MuestraTransaccion(CodTransaccion);
            tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
            txtTransaccion.Text = tran.Sigla;
            lbNombreTransaccion.Text = tran.Descripcion;
            lbNombreTransaccion.Visible = true;
            foreach (Control t in groupBox1.Controls)
            {
                if (t.Tag != null)
                {
                    if (t.Tag != "")
                    {
                        Int32 con = Convert.ToInt32(t.Tag);
                        if (tran.Configuracion.Contains(con))
                        {
                            t.Visible = true;
                        }
                        else
                        {
                            t.Visible = false;
                        }
                    }
                }
            }
        }

        private void CargaFormaPagos()
        {
            cmbFormaPago.DataSource = AdmPago.CargaFormaPagos(1);
            cmbFormaPago.DisplayMember = "descripcion";
            cmbFormaPago.ValueMember = "codFormaPago";
            //Se agrego estas lineas
            cmbFormaPago.SelectedIndex = 0;
            cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, null);
            //Fin se agrego estas lineas
        }

        private void CargaVendedores()
        {
            cbovendedor.DataSource = AdmVen.MuestraVendedoresDestaque();
            cbovendedor.DisplayMember = "apellido";
            cbovendedor.ValueMember = "codVendedor";
            cbovendedor.SelectedIndex = 0;
        }

        private void CargaVendedores2()
        {
            cbovendedor.DataSource = AdmVen.MuestraVendedoresDestaque2();
            cbovendedor.DisplayMember = "apellido";
            cbovendedor.ValueMember = "codVendedor";
            cbovendedor.SelectedIndex = 0;
        }

        private void CargaListaPrecios(int codForma)
        {
            cbListaPrecios.DataSource = admLista.MuestraListaPrecioxFormaPago(frmLogin.iCodSucursal, codForma);
            cbListaPrecios.DisplayMember = "nombre";
            cbListaPrecios.ValueMember = "codListaPrecio";
            if (cbListaPrecios.Items.Count > 0)
            {
                cbListaPrecios.SelectedIndex = 0;
            }
        }

        //public void llenardetalle2(Int32 codNota)
        //{
        //    data.DataSource = null;
        //    DataTable datoscarga = new DataTable();

        //    datoscarga = AdmVenta.MuestraDetalleGuiaVenta(frmLogin.iCodAlmacen,codNota);
        //    if (datoscarga != null)
        //    {
        //        datoscarga2.Merge(datoscarga);
        //    }

        //    datos = datoscarga2;

        //    for (int i = 0; i < datos.Rows.Count; i++)
        //    {
        //        for (int j = i + 1; j < datos.Rows.Count; j++)
        //        {
        //            if (Convert.ToDecimal(datos.Rows[i]["preciounitario"])
        //                    .Equals(Convert.ToDecimal(datos.Rows[j]["preciounitario"])) &&
        //                Convert.ToInt32(datos.Rows[i]["codProducto"])
        //                    .Equals(Convert.ToInt32(datos.Rows[j]["codProducto"])))
        //            {
        //                datos.Rows[i]["cantidad"] = Convert.ToDecimal(datos.Rows[i]["cantidad"]) +
        //                                            Convert.ToDecimal(datos.Rows[j]["cantidad"]);
        //                datos.Rows[j]["codSalida"] = Convert.ToInt32(datos.Rows[j]["codSalida"]);
        //                AdmNota.deletedetalle(Convert.ToInt32(datos.Rows[j]["codDetalle"]));
        //                datos.Rows.RemoveAt(j);
        //            }
        //        }
        //    }
        //    dgvDetalle.DataSource = datos;
        //    recalculadetalle();
        //    dgvDetalle.ClearSelection();
            
        //}

        private void recalculadetalle()
        {
            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                //if (Convert.ToInt32(row.Cells[stockPend.Name].Value) != 0)
                //{
                    row.Cells[importe.Name].Value = Convert.ToDecimal(row.Cells[cantidad.Name].Value) * Convert.ToDecimal(row.Cells[preciounit.Name].Value);
                    row.Cells[precioventa.Name].Value = Convert.ToDecimal(row.Cells[cantidad.Name].Value) * Convert.ToDecimal(row.Cells[preciounit.Name].Value);
                    row.Cells[valorventa.Name].Value = Convert.ToDecimal(row.Cells[importe.Name].Value) / Convert.ToDecimal(frmLogin.Configuracion.IGV / 100 + 1);
                    row.Cells[precioreal.Name].Value = Convert.ToDecimal(row.Cells[importe.Name].Value) / Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                    row.Cells[valoreal.Name].Value = Convert.ToDecimal(row.Cells[valorventa.Name].Value) / Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                    row.Cells[igv.Name].Value = Convert.ToDecimal(row.Cells[importe.Name].Value) - Convert.ToDecimal(row.Cells[valorventa.Name].Value);
                //}
            }
        }
        
        private void txtTransaccion_Leave(object sender, EventArgs e)
        {
            if (CodTransaccion == 0)
            {
                txtTransaccion.Focus();
            }
        }

        private void txtTransaccion_KeyPress(object sender, KeyPressEventArgs e)
        {            
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtTransaccion.Text != "")
                {
                    if (BuscaTransaccion())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de transacción no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private Boolean BuscaTransaccion()
        {
            tran = AdmTran.MuestraTransaccionS(txtTransaccion.Text, 1);
            if (tran != null)
            {
                CodTransaccion = tran.CodTransaccion;
                tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
                txtTransaccion.Text = tran.Sigla;
                lbNombreTransaccion.Text = tran.Descripcion;
                lbNombreTransaccion.Visible = true;
                foreach (Control t in groupBox1.Controls)
                {
                    if (t.Tag != null && t.Tag!="")
                    {
                        Int32 con = Convert.ToInt32(t.Tag);
                        if (tran.Configuracion.Contains(con))
                        {
                            t.Visible = true;
                        }
                        else
                        {
                            t.Visible = false;
                        }
                    }
                }
                return true;
            }
            else
            {
                lbNombreTransaccion.Text = "";
                lbNombreTransaccion.Visible = false;
                foreach (Control t in groupBox1.Controls)
                {
                    if (t.Tag != null)
                    {
                        t.Visible = false;
                    }
                }
                return false;
            }
        }

        List<clsNotaCredito> ncredito = new List<clsNotaCredito>();
        clsAdmNotaCredito admNotac = new clsAdmNotaCredito();

        private void CargaCliente()
        {
            //cli = AdmCli.MuestraCliente(CodCliente);
            //cli = AdmCli.CargaDeuda(cli);
            ncredito = admNotac.BuscarNotasXCliente(CodCliente);
            if (cli.Cantidad > 0)
            {
                DialogResult dlgResult = MessageBox.Show("El cliente selecionado presenta" + Environment.NewLine + "Facturas pendientes = " + cli.Cantidad + Environment.NewLine + "Deuda Total = " + cli.Deuda + " soles" + Environment.NewLine + "Linea de crédito = " + cli.LineaCredito + Environment.NewLine + " Desea continuar con la venta?", "Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    ret = 1;
                    txtCotizacion.Text = "";
                    return;
                }
                else
                {
                    cargadatoscliente();
                    ret = 0;
                }
            }
            else 
            {
                cargadatoscliente();
                ret = 0;
            }

            if (ncredito.Count > 0)
            {
                labelnotacredito.Visible = true;
            }
            else
            {
                labelnotacredito.Visible = false;
            }
        }

        //private void cargadatoscliente()
        //{
        //    txtCodCliente.Text = cli.Dni;
        //    if (cli.Ruc != "")
        //    {
        //        txtDocRef.Text = "FT";
        //        KeyPressEventArgs ee = new KeyPressEventArgs((char) Keys.Return);

        //        txtDocRef_KeyPress(txtDocRef, ee);
        //        txtSerie.Text = "001";
        //        txtSerie_KeyPress(txtDocRef, ee);
        //        //txtCodCliente.Text = cli.Ruc;
        //    }
        //    else
        //    {
                
        //        txtDocRef.Text = "BV";
        //        KeyPressEventArgs ee = new KeyPressEventArgs((char) Keys.Return);
        //        txtDocRef_KeyPress(txtDocRef, ee);
        //        txtSerie.Text = "001";
        //        txtSerie_KeyPress(txtDocRef, ee);
        //        //txtCodigoCli.Text = cli.Dni;
        //    }
                
        //    txtNombreCliente.Text = cli.RazonSocial;
        //    txtDireccionCliente.Text = cli.DireccionLegal;
        //    txtCodigoCli.Text = cli.CodCliente.ToString();
        //    if (cli.Moneda == 1)
        //    {
        //        txtLineaCredito.Text = cli.LineaCredito.ToString();
        //        txtLineaCreditoDisponible.Text = cli.LineaCreditoDisponible.ToString();
        //        txtLineaCreditoUso.Text = cli.LineaCreditoUsado.ToString();
        //        lbLineaCredito.Text = "Línea de Crédito (S/.):";
        //        label23.Text = "Línea Disponible (S/.):";
        //        label25.Text = "Línea C. en Uso (S/.):";
        //    }
        //    else 
        //    {
        //        txtLineaCredito.Text = cli.LineaCredito.ToString();
        //        txtLineaCreditoDisponible.Text = cli.LineaCreditoDisponible.ToString();
        //        txtLineaCreditoUso.Text = cli.LineaCreditoUsado.ToString();
        //        lbLineaCredito.Text = "Línea de Crédito ($.):";
        //        label23.Text = "Línea Disponible ($.):";
        //        label25.Text = "Línea C. en Uso ($.):";
        //    }
           
        //    cmbFormaPago.SelectedValue = cli.FormaPago;
        //    forma = AdmPago.BuscaFormaPagoVenta(cli.FormaPago);    
        //    if (cli.CodListaPrecio != null) 
        //    { 
        //        cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, null); 
        //        cbListaPrecios.SelectedValue = cli.CodListaPrecio; 
        //    }
        //    //cmbFormaPago.SelectedIndex = 0;
        //    if (cli.FormaPago != 0)
        //    {
        //        EventArgs ee = new EventArgs();
        //        cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
               
        //    }
        //    else
        //    {
        //        dtpFechaPago.Value = DateTime.Today;
        //    }
        //    if (cli.CodVendedor != 0)
        //    {
               
        //        cbovendedor.SelectedValue = cli.CodVendedor;
        //    }
        //    txtPDescuento.Text = cli.Descuento.ToString();
        //    cmbMoneda.SelectedValue = cli.Moneda;
        //}

        private void cargadatoscliente()
        {
            txtCodCliente.Text = cli.Dni;
            if (cli.Ruc != "" && cli.Dni == "")
            {
                txtDocRef.Text = "FT";
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                txtDocRef_KeyPress(txtDocRef, ee);
                //txtSerie.Text = "001";
                txtSerie_KeyPress(txtDocRef, ee);
                txtCodCliente.Text = cli.Ruc;
            }
            else if (cli.Dni != "" && cli.Ruc == "")
            {

                txtDocRef.Text = "BV";
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                txtDocRef_KeyPress(txtDocRef, ee);
                //txtSerie.Text = "001";
                txtSerie_KeyPress(txtDocRef, ee);
                txtCodigoCli.Text = cli.Dni;
            }
            else if (cli.Ruc != "" && cli.Dni != "")
            {
                if (MessageBox.Show("Si para FT(Factura) o No para BV(Boleta)", "Seleccione Tipo de Doc. Ref.", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    txtDocRef.Text = "BV";
                    KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                    txtDocRef_KeyPress(txtDocRef, ee);
                    //txtSerie.Text = "001";
                    txtSerie_KeyPress(txtDocRef, ee);
                    txtCodigoCli.Text = cli.Dni;
                }
                else
                {
                    txtDocRef.Text = "FT";
                    KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                    txtDocRef_KeyPress(txtDocRef, ee);
                    //txtSerie.Text = "001";
                    txtSerie_KeyPress(txtDocRef, ee);
                    txtCodCliente.Text = cli.Ruc;
                }
            }
            txtNombreCliente.Text = cli.RazonSocial;
            txtDireccionCliente.Text = cli.DireccionLegal;
            txtCodigoCli.Text = cli.CodCliente.ToString();
            if (cli.Moneda == 1)
            {
                txtLineaCredito.Text = cli.LineaCredito.ToString();
                txtLineaCreditoDisponible.Text = cli.LineaCreditoDisponible.ToString();
                txtLineaCreditoUso.Text = cli.LineaCreditoUsado.ToString();
                lbLineaCredito.Text = "Línea de Crédito (S/.):";
                label23.Text = "Línea Disponible (S/.):";
                label25.Text = "Línea C. en Uso (S/.):";

                if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
            }
            else
            {
                txtLineaCredito.Text = cli.LineaCredito.ToString();
                txtLineaCreditoDisponible.Text = cli.LineaCreditoDisponible.ToString();
                txtLineaCreditoUso.Text = cli.LineaCreditoUsado.ToString();
                lbLineaCredito.Text = "Línea de Crédito ($.):";
                label23.Text = "Línea Disponible ($.):";
                label25.Text = "Línea C. en Uso ($.):";
                if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
            }

            //cmbFormaPago.SelectedValue = cli.FormaPago;
            //Se cambio por la linea de arriba
            cmbFormaPago.SelectedIndex = 0;
            //Fin se cambio por la linea de arriba
            forma = AdmPago.BuscaFormaPagoVenta(cli.FormaPago);
            //if (cli.CodListaPrecio != null)
            //{
            //    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, null);
            //    txtplazo.Text = cmbFormaPago.Text;
            //    cbListaPrecios.SelectedValue = cli.CodListaPrecio;
            //}
            //cmbFormaPago.SelectedIndex = 0;
            if (cli.FormaPago != 0)
            {
                cmbFormaPago.SelectedValue = cli.FormaPago;
                EventArgs ee = new EventArgs();
                cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);

            }
            else
            {
                dtpFechaPago.Value = DateTime.Today;
            }
            if (cli.CodVendedor != 0)
            {

                cbovendedor.SelectedValue = cli.CodVendedor;
                txtvendedor.Text = cbovendedor.Text;
            }
            txtPDescuento.Text = cli.Descuento.ToString();
            cmbMoneda.SelectedValue = cli.Moneda;
            mon = cli.Moneda;//MOD6
            txtMoneda.Text = cmbMoneda.Text;
            txttasa.Text = cli.Tasa.ToString();
        }

        private Boolean BuscaCliente()
        {
            cli = AdmCli.BuscaCliente(txtCodCliente.Text, Tipo);
            
            if (cli != null)
            {
                cli = AdmCli.CargaDeuda(cli);
                if (cli.Cantidad > 0)
                {
                    DialogResult dlgResult = MessageBox.Show("El cliente selecionado presenta" + Environment.NewLine + "Facturas pendientes = " + cli.Cantidad + Environment.NewLine + "Deuda Total = " + cli.Deuda + " soles" + Environment.NewLine + "Linea de crédito = " + cli.LineaCredito + Environment.NewLine + " Desea continuar con la venta?", "Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dlgResult == DialogResult.No)
                    {
                        txtNombreCliente.Text = "";
                        CodCliente = 0;
                        
                        txtPDescuento.Text = "";
                        return false;                       
                    }
                    else
                    {
                        CodCliente = cli.CodCliente;
                        cargadatoscliente();
                        return true;
                    }
                }
                else
                {
                    CodCliente = cli.CodCliente;
                    cargadatoscliente();
                    return true; 
                }
            }
            else
            {
                MessageBox.Show("El Cliente no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodCliente.Text = "";
                txtNombreCliente.Text = "";
                CodCliente = 0;
                
                txtPDescuento.Text = "";
                return false;
            }
        }

        private void txtCodCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            
            
            {
                if (Application.OpenForms["frmClientesLista"] != null)
                {
                    Application.OpenForms["frmClientesLista"].Activate();
                }
                else
                {
                    frmClientesLista form = new frmClientesLista();
                    form.Proceso = 3;
                    //form.Tipo = cmbTipoCodigo.SelectedIndex;
                    form.ShowDialog();
                    //cli = form.cli
                    cli = AdmCli.MuestraCliente(form.cli.CodCliente);
                    CodCliente = cli.CodCliente;
                    
                    if (CodCliente != 0)
                    {
                        CargaCliente();
                        btnNuevo.Enabled = true; ProcessTabKey(true);
                        txtDocRef.Focus();
                    }                    
                }
            }
        }

        private void txtCodCliente_Leave(object sender, EventArgs e)
        {            
            //if (CodCliente == 0)
            //{
            //    txtCodCliente.Focus();
            //}
            //VerificarCabecera();
            //if (Validacion && Proceso == 1)
            //{
            //    btnGuardar.Enabled = true;
            //}
        }

        private void txtCodCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)Keys.Return)
            //{
            //    if (txtCodCliente.Text != "")
            //    {
            //        if (BuscaCliente())
            //        {
            //            ProcessTabKey(true);
            //        //}
            //        //else
            //        //{
            //        //    MessageBox.Show("El Cliente no existe, Presione F1 para consultar la tabla de ayuda", "NOTA DE SALIDA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        }
            //    }
            //}

            this.Cursor = Cursors.WaitCursor;

            ok.enteros(e);
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                try
                {
                    //Cursor = Cursors.WaitCursor;
                    switch (this.txtCodCliente.Text.Length)
                    {
                        case 1:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo un digito Ingresado",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 2:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo dos digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 3:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo tres digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 4:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cuatro digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 5:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cinco digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 6:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo seis digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 7:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo siete digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 8:
                            cli = AdmCli.ConsultaCliente(txtCodCliente.Text);

                            if (cli != null)
                            {
                                cli = AdmCli.MuestraCliente(cli.CodCliente);

                                if (cli != null)
                                {
                                    CodCliente = cli.CodCliente;
                                    txtNombreCliente.Text = cli.RazonSocial;
                                    txtDireccionCliente.Text = cli.DireccionLegal;

                                    //cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                                    //cli.DocumentoIdentidad.CodDocumentoIdentidad = 1;
                                    //cli.DocumentoIdentidad.CodigoSunat = 1;


                                    cargadatoscliente();
                                }
                            }
                            else
                            {
                                MessageBox.Show("El DNI ingresado no se encuentra registrado");
                                CodCliente = 0;
                            }

                            //chkBoleta.Checked = true;
                            //cmbFormaPago.Enabled = true;
                            break;

                        case 9:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso nueve digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            break;

                        case 10:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso diez digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 11:
                            cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
                            if (cli != null)
                            {
                                cli = AdmCli.MuestraCliente(cli.CodCliente);
                                if (cli != null)
                                {
                                    CodCliente = cli.CodCliente;
                                    txtNombreCliente.Text = cli.RazonSocial;
                                    txtDireccionCliente.Text = cli.DireccionLegal;

                                    //cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                                    //cli.DocumentoIdentidad.CodDocumentoIdentidad = 3;
                                    //cli.DocumentoIdentidad.CodigoSunat = 6;

                                    cargadatoscliente();
                                }
                                else
                                {
                                    CargarImagenSunat();
                                    CargaRUC();
                                    CodCliente = 0;
                                }
                            }
                            else
                            {
                                CargarImagenSunat();
                                CargaRUC();
                                CodCliente = 0;
                            }

                            //CargaCreditoCliente(cli);

                            //chkFactura.Checked = true;
                            //cmbFormaPago.Enabled = true;
                            break;

                        default:
                            ValidaLongitud();
                            break;
                    }


                    //txtCodigoVendedor.Focus();
                    //cbFamilia.Select();       
                    this.Cursor = Cursors.Default;

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message);
                    CargarImagenSunat();
                }
            }

            this.Cursor = Cursors.Default;

        }


        public void CargaCreditoCliente(clsCliente cli)
        {

            if (cli != null)
            {
                if (cli.Moneda == 1)
                {
                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                    txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                    txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                    lbLineaCredito.Text = "Línea de Crédito (S/.):";
                    label23.Text = "Línea Disponible (S/.):";
                    label25.Text = "Línea C. en Uso (S/.):";
                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                }
                else
                {
                    //txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                    //txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                    //txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                    //lbLineaCredito.Text = "Línea de Crédito ($.):";
                    //label23.Text = "Línea Disponible ($.):";
                    //label25.Text = "Línea C. en Uso ($.):";
                    //if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                }
                if (cli.FormaPago != 0)
                {
                    cmbFormaPago.SelectedValue = cli.FormaPago; //cli.FormaPago  --   6 Contado
                    EventArgs ee = new EventArgs();
                    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                }
                else
                {
                    dtpFechaPago.Value = DateTime.Today;
                }
            }

            else
            {

                cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
                if (cli != null)
                {


                    if (cli.Moneda == 1)
                    {
                        txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                        txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                        txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                        txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                        lbLineaCredito.Text = "Línea de Crédito (S/.):";
                        label23.Text = "Línea Disponible (S/.):";
                        label25.Text = "Línea C. en Uso (S/.):";
                        if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                    }
                    else
                    {
                        //txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                        //txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                        //txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                        //lbLineaCredito.Text = "Línea de Crédito ($.):";
                        //label23.Text = "Línea Disponible ($.):";
                        //label25.Text = "Línea C. en Uso ($.):";
                        //if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                    }
                    if (cli.FormaPago != 0)
                    {
                        cmbFormaPago.SelectedValue = cli.FormaPago; //cli.FormaPago  --   6 Contado
                        EventArgs ee = new EventArgs();
                        cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                    }
                    else
                    {
                        dtpFechaPago.Value = DateTime.Today;
                    }
                }
            }
        }

        private void CargaRUC()
        {
            if (this.txtCodCliente.Text.Length == 11)
            {
                LeerDatos();
            }
        }

        private void LeerDatos()
        {

            string cadena = "";

            //llamamos a los metodos de la libreria ConsultaReniec...
            MyInfoSunat.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
            switch (MyInfoSunat.GetResul)
            {
                case Sunat.Resul.Ok:
                    limpiarSunat();
                    txtCodCliente.Text = MyInfoSunat.Ruc.Trim();
                    txtDireccionCliente.Text = MyInfoSunat.Direcion.Trim();
                    txtNombreCliente.Text = MyInfoSunat.RazonSocial.Trim();

                    if (!txtCodCliente.Text.StartsWith("1"))
                    {
                        cadena = txtDireccionCliente.Text;
                        int indice = cadena.IndexOf('<');
                        cadena = cadena.Substring(0, cadena.Length - (cadena.Length - indice));
                        //cadena = cadena.Replace("</td>\r\n </tr>\r\n\r\n <tr>\r\n", " ");
                        txtDireccionCliente.Text = cadena;
                    }
                    else
                    {
                        txtDireccionCliente.Text = "-";
                    }

                    string textoOriginal = txtNombreCliente.Text;//transformación UNICODE
                    string textoNormalizado = textoOriginal.Normalize(NormalizationForm.FormD);
                    //coincide todo lo que no sean letras y números ascii o espacio
                    //y lo reemplazamos por una cadena vacía.Regex reg = new Regex("[^a-zA-Z0-9 ]");
                    Regex reg = new Regex("[^a-zA-Z0-9 ]");
                    string textoSinAcentos = reg.Replace(textoNormalizado, "");

                    txtNombreCliente.Text = textoSinAcentos;

                    //Ciudad(MyInfoSunat.Direcion);
                    BloqueaDatos();
                    break;
                case Sunat.Resul.NoResul:
                    limpiarSunat();
                    MessageBox.Show("No Existe RUC");
                    break;
                case Sunat.Resul.ErrorCapcha:
                    limpiarSunat();
                    MessageBox.Show("Ingrese imagen correctamente");
                    break;
                default:
                    MessageBox.Show("Error Desconocido");
                    break;
            }
            //CargarImagenSunat();
        }

        private void BloqueaDatos()
        {
            /*txtRUC.ReadOnly = true; */
            //txtDireccion.ReadOnly = true;
            txtNombreCliente.ReadOnly = false;
        }

        private void ValidaLongitud()
        {
            if (txtCodCliente.Text.Length == 0)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ningun digito Ingresado",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
            }
            else if (txtCodCliente.Text.Length > 11)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ha Ingresado " + txtCodCliente.Text.Length + " Digitos",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                txtCodCliente.SelectAll();
                txtCodCliente.Focus();
            }
        }

        private void CargarImagenSunat()
        {
            try
            {
                if (MyInfoSunat == null)
                    MyInfoSunat = new Sunat();
                this.pbCapchatS.Image = MyInfoSunat.GetCapcha;
                LeerCaptchaSunat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LeerCaptchaSunat()
        {
            //string ruta = Directory.GetCurrentDirectory()+"\\tessdata";
            //string RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata\\");
            try
            {
                using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
                {
                    using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
                    {
                        using (var pix = PixConverter.ToPix(image))
                        {
                            using (var page = engine.Process(pix))
                            {
                                var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                                string CaptchaTexto = page.GetText();
                                char[] eliminarChars = { '\n', ' ' };
                                CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                                CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                                CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z]+", string.Empty);
                                if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                    txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
                                else
                                    CargarImagenSunat();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarImagenReniec()
        {
            try
            {
                if (MyInfoReniec == null)
                    MyInfoReniec = new Reniec();
                this.pbCapchatS.Image = MyInfoReniec.GetCapcha;
                AplicacionFiltros();
                LeerCaptchaReniec();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AplicacionFiltros()
        {
            Bitmap bmp = new Bitmap(pbCapchatS.Image);
            FiltroInvertir(bmp);
            ColorFiltros();
            Bitmap bmp1 = new Bitmap(pbCapchatS.Image);
            FiltroInvertir(bmp1);
            Bitmap bmp2 = new Bitmap(pbCapchatS.Image);
            FiltroSharpen(bmp2);
        }

        private void FiltroInvertir(Bitmap bmp)
        {
            IFilter Filtro = new Invert();
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;
        }


        private void FiltroSharpen(Bitmap bmp)
        {
            IFilter Filtro = new Sharpen();
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;

        }
        private void ColorFiltros()
        {
            //Red Min - MAX
            red.Min = Math.Min(red.Max, byte.Parse("229"));
            red.Max = Math.Max(red.Min, byte.Parse("255"));
            //Verde Min - MAX
            green.Min = Math.Min(green.Max, byte.Parse("0"));
            green.Max = Math.Max(green.Min, byte.Parse("255"));
            //Azul Min - MAX
            blue.Min = Math.Min(blue.Max, byte.Parse("0"));
            blue.Max = Math.Max(blue.Min, byte.Parse("130"));
            ActualizarFiltro();
        }


        private void ActualizarFiltro()
        {
            ColorFiltering FiltroColor = new ColorFiltering();
            FiltroColor.Red = red;
            FiltroColor.Green = green;
            FiltroColor.Blue = blue;
            IFilter Filtro = FiltroColor;
            Bitmap bmp = new Bitmap(pbCapchatS.Image);
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;
        }


        private void LeerCaptchaReniec()
        {
            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
                {
                    using (var pix = PixConverter.ToPix(image))
                    {
                        using (var page = engine.Process(pix))
                        {
                            var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                            string CaptchaTexto = page.GetText();
                            char[] eliminarChars = { '\n', ' ' };
                            CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                            CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                            CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z0-9]+", string.Empty);
                            if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
                            //else
                            //    CargarImagenReniec();
                        }
                    }
                }
            }
        }


        private void CargaDNI()
        {
            MyInfoReniec.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
            switch (MyInfoReniec.GetResul)
            {
                case Reniec.Resul.Ok:
                    limpiarSunat();
                    txtCodCliente.Text = MyInfoReniec.Dni;
                    String apellidos = MyInfoReniec.ApePaterno + " " + MyInfoReniec.ApeMaterno;
                    txtNombreCliente.Text = MyInfoReniec.Nombres + " " + apellidos;
                    break;
                case Reniec.Resul.NoResul:
                    limpiarSunat();
                    MessageBox.Show("No Existe DNI");
                    break;
                case Reniec.Resul.ErrorCapcha:
                    limpiarSunat();
                    MessageBox.Show("Ingrese imagen correctamente");
                    break;
                default:
                    MessageBox.Show("Error Desconocido");
                    break;
            }
            //Comentar esta linea para consultar multiples DNI usando un solo captcha.
            CargarImagenReniec();
        }

        private void limpiarSunat()
        {
            txtNombreCliente.Text = "";
            txtSunat_Capchat.Text = string.Empty;
        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtTipoCambio.Visible)
                {
                    tc = AdmTc.CargaTipoCambio(dtpFecha.Value.Date, 2);
                    if (tc != null)
                    {
                        txtTipoCambio.Text = tc.Compra.ToString();
                        dtpFechaPago.Value = dtpFecha.Value.AddDays(fpago.Dias);
                    }
                    else
                    {
                        MessageBox.Show("No existe tipo de cambio registrado en esta fecha", "Tipo de Cambio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtpFecha.Value = DateTime.Now.Date;
                        dtpFecha.Focus();
                    }
                }
                cmbMoneda.Focus();
            }
            catch (Exception ex) { dtpFecha.Value = DateTime.Now.Date; dtpFecha.Focus(); }
        }

        private void dtpFecha_Leave(object sender, EventArgs e)
        {
            if (CodTransaccion == 0)
            {
                dtpFecha.Focus();
            }
        }

        private void dtpFecha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                ProcessTabKey(true);
            }
        }

        private void cmbMoneda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                ProcessTabKey(true);
            }
        }

        private void cmbMoneda_Leave(object sender, EventArgs e)
        {
            if (CodTransaccion == 0)
            {
                cmbMoneda.Focus();
            }
            if (cmbMoneda.SelectedValue != null)
            {
                if (cmbMoneda.SelectedText.Equals("NUEVOS SOLES"))
                {
                    label8.Visible = false;
                    txtTipoCambio.Visible = false;
                }
            }
        }

        private void txtDocRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtDocRef.Text != "")
                {
                    if (BuscaTipoDocumento())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de Documento no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private Boolean BuscaTipoDocumento()
        {
            doc = Admdoc.BuscaTipoDocumento(txtDocRef.Text);
            if (doc != null)
            {
                CodDocumento = doc.CodTipoDocumento;
                txtCodDocumento.Text = CodDocumento.ToString();
                return true;
            }
            else
            {
                CodDocumento = 0;
                txtCodDocumento.Text = CodDocumento.ToString();
                return false;
            }
        }

        private Boolean BuscaSerie()
        {
            //ser = Admser.BuscaSerie(txtSerie.Text,CodDocumento,frmLogin.iCodAlmacen);
            ser = Admser.BuscaSeriexDocumento(CodDocumento, frmLogin.iCodAlmacen);
            if (ser != null)
            {
                CodSerie = ser.CodSerie;
                return true;
            }
            else
            {
                CodSerie = 0;
                return false;
            }

        }
        
        private void txtDocRef_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmDocumentos"] != null)
                {
                    Application.OpenForms["frmDocumentos"].Activate();
                }
                else
                {
                    if (cli.Ruc != "")
                    {
                        frmDocumentos form = new frmDocumentos();
                        form.Proceso = 3;
                        form.ShowDialog();
                        doc = form.doc;
                        CodDocumento = doc.CodTipoDocumento;
                        txtCodDocumento.Text = CodDocumento.ToString();
                        txtDocRef.Text = doc.Sigla;
                        if (CodDocumento != 0) { ProcessTabKey(true); }
                    }
                }
            }
        }

        private void VerificarCabecera()
        {
            Validacion = true;
            if (CodTransaccion == 0 || CodDocumento == 0)
            {
                Validacion = false;
            }
            if (txtCodCliente.Visible && CodCliente == 0)
            {
                Validacion = false;
            }
            //if (txtPedido.Visible && CodPedido == 0)
            //{
            //    Validacion = false;
            //}
            if (txtAutorizacion.Visible && CodAutorizado == 0)
            {
                Validacion = false;
            }
            if (Validacion && Proceso == 1)
            {
                btnGuardar.Enabled = true;
            }
        }

        private void sololectura(Boolean estado)
        {
            txtTransaccion.ReadOnly = estado;
            dtpFecha.Enabled = !estado;
            txtCodCliente.ReadOnly = estado;
            cmbMoneda.Enabled = !estado;
            txtDocRef.ReadOnly = estado;
            txtNumero.Visible = estado;
            txtNumero.ReadOnly = estado;
            //txtPedido.ReadOnly = estado;
            txtComentario.ReadOnly = estado;
            txtAutorizacion.ReadOnly = estado;
            txtBruto.ReadOnly = estado;
            txtDscto.ReadOnly = estado;
            txtValorVenta.ReadOnly = estado;
            txtIGV.ReadOnly = estado;
            txtPrecioVenta.ReadOnly = estado;
            btnNuevo.Visible = !estado;
            btnEditar.Visible = !estado;
            btnEliminar.Visible = !estado;
            btnGuardar.Visible = !estado;
           // btnImprimir.Visible = estado;
            btnNuevaVenta.Visible = estado;
            ckbguia.Enabled = !estado;
            cbovendedor.Enabled = !estado;
            cbListaPrecios.Enabled = !estado;
            cmbFormaPago.Enabled = !estado;
            txtSerie.ReadOnly = estado;
            txtGuias.Enabled = !estado;
            txtCotizacion.Enabled = !estado;
            lblAlmacen.Visible=!estado;
            cmbAlmacen.Visible=!estado;
            txtDetalle.Enabled = !estado;
            groupBox5.Visible = !estado;
        }

        private void BloquearEdicion(Boolean estado)// para bloquear la edicion de la factura en caso de cargar datos de una cotizacion vigente
        {
            txtTransaccion.ReadOnly = estado;
            dtpFecha.Enabled = !estado;
            txtCodCliente.ReadOnly = estado;
            cmbMoneda.Enabled = !estado;
            txtDocRef.ReadOnly = estado;
            txtNumero.Visible = estado;
            txtNumero.ReadOnly = estado;
            //txtPedido.ReadOnly = estado;
            txtComentario.ReadOnly = estado;
            txtAutorizacion.ReadOnly = estado;
            txtBruto.ReadOnly = estado;
            txtDscto.ReadOnly = estado;
            txtValorVenta.ReadOnly = estado;
            txtIGV.ReadOnly = estado;
            txtPrecioVenta.ReadOnly = estado;
            btnNuevo.Visible = !estado;
            btnEditar.Visible = !estado;
            //btnEliminar.Visible = !estado;//MOD5
            //btnGuardar.Visible = !estado;
            //btnImprimir.Visible = estado;
            //btnNuevaVenta.Visible = estado;
            //ckbguia.Enabled = !estado;
            cbovendedor.Enabled = !estado;
            cbListaPrecios.Enabled = !estado;
            cmbFormaPago.Enabled = !estado;
            txtSerie.ReadOnly = estado;
            txtGuias.Enabled = !estado;
        }

        private void CargaDetalle()
        {
            if (Proceso == 3 && ventaok==0)
            {
                dgvDetalle.DataSource = AdmVenta.CargaDetalleSeparacion(Convert.ToInt32(venta.CodFacturaVenta),frmLogin.iCodAlmacen);
            }
            else { dgvDetalle.DataSource = AdmVenta.CargaDetalle(Convert.ToInt32(venta.CodFacturaVenta), frmLogin.iCodAlmacen);
                ventaok = 0;
            }
        }

        private void CargaDetalleCotizacion()
        {
            dgvDetalle.DataSource = AdmCoti.CargaDetalle(Convert.ToInt32(coti.CodCotizacion), frmLogin.iCodAlmacen);
        }

        private void CargaDetalleGuia()
        {
            dgvDetalle.DataSource = AdmGuia.CargaDetalle(Convert.ToInt32(guia.CodGuiaRemision));
        }

        private void txtDocRef_Leave(object sender, EventArgs e)
        {
            BuscaTipoDocumento();
           
        }

        private void CargaMoneda()
        {
            cmbMoneda.DataSource = AdmMon.CargaMonedasHabiles();
            cmbMoneda.DisplayMember = "descripcion";
            cmbMoneda.ValueMember = "codMoneda";
            cmbMoneda.SelectedIndex = 0;
        }

        public DateTime fecha1, fecha2;

       
        private void CargaTransportista()
        {
            cmbTransportista.DataSource = admConductor.CargaConductores();
            cmbTransportista.DisplayMember = "nombre";
            cmbTransportista.ValueMember = "codConductor";
            cmbTransportista.SelectedIndex = -1;
        }

        private void CargaVehiculoTrasnporte()
        {
            cmbVehiculo.DataSource = admVehiculoTransporte.CargaVehiculoTransportes();
            cmbVehiculo.DisplayMember = "placa";
            cmbVehiculo.ValueMember = "codVehiculoTransporte";
            cmbVehiculo.SelectedIndex = -1;
        }

        private void iniciaformulario()
        {
            CargaMoneda();
            dtpFecha.MaxDate = DateTime.Today.Date;
            tc = mdi_Menu.clstc;
            CargaFormaPagos();
            CargaVendedores();
            if (Proceso == 2)
            {
                CargaVenta();
            }
            else if (Proceso == 3)
            {
                CargaVenta();
                sololectura(true);
                groupBox3.Visible = false;
                groupBox5.Visible = false;
                btnImprimir.Visible = true;
            }
            else if (Proceso == 4)
            {
                txtcodpedido.Text = CodPedido.ToString().PadLeft(11, '0');
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                CargaPedido();
                CargaDetallePedido();
                txtcodpedido_KeyPress(txtcodpedido, ee);
                Procede = 4;//PEDIDO VENTA
               
            }
            else if (Proceso == 5)
            {
                txtcodpedido.Text = CodSeparacion.ToString().PadLeft(11, '0');
                Procede = 7;//SEPARACION VENTA
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                txtcodpedido_KeyPress(txtcodpedido, ee);
            }

            if (consultorext == true)
            {
                checkBox1.Checked = true;
                CargaVendedores2();
                cbovendedor.SelectedIndex = 0;
                CodVendedor = Int32.Parse(cbovendedor.SelectedValue.ToString());
                //  cbovendedor.SelectedValue = CodVendedor;
                //  cbovendedor.Enabled = false;
            }
            else
            {
                //Sin Vendedor
                checkBox1.Checked = false;
                //CargaVendedores();

            }
            txtCodigoCli.Visible = false;
        }

        private void cargaAlmacenes()
        {
            cmbAlmacen.DataSource = Admalmac.CargaAlmacen2(frmLogin.iCodEmpresa);
            cmbAlmacen.DisplayMember = "nombre";
            cmbAlmacen.ValueMember = "codAlmacen";
            cmbAlmacen.SelectedValue = frmLogin.iCodAlmacen; ;
        }

        private void frmVenta_Shown(object sender, EventArgs e)
        {
            txtTransaccion.Focus();
            txtTransaccion.Text = "FT";
            KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
            txtTransaccion_KeyPress(txtTransaccion,ee);
            cmbAlmacen.Visible = true;
            cargaAlmacenes();
            btnNuevo.Focus();
            if (Proceso == 1)
            {
                if (txtTipoCambio.Visible)
                {
                    if (tc == null)
                    {
                        MessageBox.Show("Debe registrar el tipo de cambio del día", "Tipo de Cambio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        txtTipoCambio.Text = tc.Compra.ToString();
                        txtcodpedido.Text = "0";
                        txtcodpedido.Enabled = false;
                    }
                }
            }
            else if (Proceso == 4)
            {
                Proceso = 1;
            }
        }

        public Boolean xgenerar = false;
        private void CargaVenta()
        {
            try
            {
                if (Proceso == 3 && ventaok==0 )
                {
                    venta = AdmVenta.CargaSeparacionVenta(Convert.ToInt32(CodVenta));
                }else
                { venta = AdmVenta.CargaFacturaVenta(Convert.ToInt32(CodVenta)); }
                ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                guia = AdmGuia.CargaGuiaVenta(Convert.ToInt32(CodVenta));
                
                if (venta != null)
                {
                    txtNumDoc.Text = venta.CodFacturaVenta;
                    CodTransaccion = venta.CodTipoTransaccion;
                    CargaTransaccion();                    

                    if (txtCodCliente.Enabled)
                    {
                        CodCliente = venta.CodCliente;
                        cli = AdmCli.MuestraCliente(CodCliente);
                        txtCodCliente.Text = venta.DNI;
                        txtNombreCliente.Text = venta.RazonSocialCliente;
                        txtDireccionCliente.Text = venta.Direccion;
                        txtLineaCredito.Text = cli.LineaCredito.ToString();
                        txtLineaCreditoDisponible.Text = cli.LineaCreditoDisponible.ToString();
                        txtLineaCreditoUso.Text = cli.LineaCreditoUsado.ToString(); 
                    }
                    dtpFecha.Value = venta.FechaSalida.Date;
                    cmbMoneda.SelectedValue = venta.Moneda;
                    txtTipoCambio.Text = venta.TipoCambio.ToString();
                    if (txtAutorizacion.Enabled)
                    {
                        //se guarda el codigo del autorizado y se cargan los datos de este
                    }
                    //if (txtDocRef.Enabled)
                    //{
                        CodDocumento = venta.CodTipoDocumento;
                        txtCodDocumento.Text = CodDocumento.ToString();
                        txtDocRef.Text = venta.SiglaDocumento;
                        txtSerie.Text = venta.Serie;
                        if (Procede != 4) txtNumero.Text = venta.NumDoc;
                        else txtNumero.Text = numSerie;
                    /* para poder generar a la hora de imprimir */
                        if (txtSerie.Text == "" && txtNumero.Text == "")
                        {
                            xgenerar = true;
                        }
                        //}
                        /* para poder generar a la hora de imprimir */
                    if (cbovendedor.Enabled)
                    {
                        if (venta.CodVendedor != 0)
                        {
                            cbovendedor.SelectedValue = venta.CodVendedor;
                        }
                    }
                    if (guia != null)
                    {
                        if (guia.CodFactura == Convert.ToInt32(venta.CodFacturaVenta))
                        {
                            ckbguia.Checked = true;
                            txtGuias.Text = guia.CodGuiaRemision;
                        }
                        else
                        {
                            ckbguia.Checked = false;
                            txtGuias.Text = "";
                        }
                    }
                    cmbFormaPago.SelectedValue = venta.FormaPago;
                    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, null);
                    cbListaPrecios.SelectedValue = venta.CodListaPrecio;
                    dtpFechaPago.Value = venta.FechaPago.Date;
                    txtComentario.Text = venta.Comentario;
                    txtDetalle.Text = venta.Detallecomentario;
                    txtBruto.Text = String.Format("{0:#,##0.00}", venta.MontoBruto);
                    txtDscto.Text = String.Format("{0:#,##0.00}", venta.MontoDscto);
                    txtValorVenta.Text = String.Format("{0:#,##0.00}", venta.Total - venta.Igv);
                    txtIGV.Text = String.Format("{0:#,##0.00}", venta.Igv);
                    txtPrecioVenta.Text = String.Format("{0:#,##0.00}", venta.Total);
                    CargaDetalle();

                }
                else
                {
                    MessageBox.Show("El documento solicitado no existe", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
              return;
            }
        }

       

        private void txtSerie_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.enteros(e);
            if (e.KeyChar == (char)Keys.Return)
            {
                //if (txtSerie.Text != "")
                //{
                    if (BuscaSerie())
                    {
                        txtSerie.Text = ser.Serie.ToString();
                        if (ser.PreImpreso)
                        {
                            txtNumero.Visible = true;
                            txtNumero.Enabled = false;
                            ckbguia.Visible = false;
                            txtNumero.Focus();
                            txtNumero.Text = "";
                        }
                        else
                        {
                            txtNumero.Text = "";
                            //txtNumero.Enabled = true;
                            txtNumero.Enabled = false;
                            //txtNumero.Visible = false;
                            txtNumero.Text = ser.Numeracion.ToString().PadLeft(8,'0');
                        }

                        ProcessTabKey(true);
                        cmbFormaPago.Focus();
                    }
                    //else
                    //{
                    //    MessageBox.Show("Serie no existe, Presione F1 para consultar la tabla de ayuda",
                    //            "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                //}
            }
            if (e.KeyChar == (char) Keys.Enter)
            {
                cmbFormaPago.Focus();
            }
        }

        private void txtSerie_Leave(object sender, EventArgs e)
        {
            if (BuscaSerie2())
            {
                txtSerie.Text = ser.Serie.ToString();
                if (ser.PreImpreso)
                {
                    txtNumero.Visible = true;
                    txtNumero.Text = "";
                    ckbguia.Visible = false;
                    txtNumero.Focus();
                }
                else
                {
                    txtNumero.Text = "";
                    txtNumero.Visible = true;
                    txtNumero.Text = ser.Numeracion.ToString();
                }
            }
            
        }
        private Boolean BuscaSerie2()
        {
            ser = Admser.MuestraSerie(CodSerie, frmLogin.iCodAlmacen);

            if (ser != null)
            {
                CodSerie = ser.CodSerie;
                return true;
            }
            else
            {
                CodSerie = 0;
                return false;
            }
        }
        private Boolean BuscaSerie3(int codDocumento)
        {
            ser = Admser.MuestraSerie(codDocumento, frmLogin.iCodAlmacen);

            if (ser != null)
            {
                CodSerie = ser.CodSerie;
                return true;
            }
            else
            {
                CodSerie = 0;
                return false;
            }
        }
        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                ProcessTabKey(true);
            }
        }

        private void txtNumero_Leave(object sender, EventArgs e)
        {
            //if (txtNumero.Text == "")
            //{
            //    txtNumero.Focus();
            //}
            //else
            //{
            //    VerificarCabecera();
               
            //}
        }

        private void dgvDetalle_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (Procede != 2 || Procede != 3)
            {
                if (Proceso == 1)
                {
                    if (txtPDescuento.Text != "")
                    {
                        calculatotales();
                        calculadescuentogeneral();
                    }
                    else
                    {
                        calculatotales();
                    }

                    if (dgvDetalle.RowCount > 0)
                    {
                        int Indice = 0;
                        Indice = dgvDetalle.RowCount - 1;

                        if (Convert.ToInt32(cmbMoneda.SelectedValue) == 1)
                        {
                            if (TipoCambio != 0)
                            {
                                dgvDetalle[8, Indice].Value = Convert.ToDecimal(dgvDetalle[8, Indice].Value)*TipoCambio;
                                dgvDetalle[9, Indice].Value = Convert.ToDecimal(dgvDetalle[9, Indice].Value)*TipoCambio;
                                dgvDetalle[13, Indice].Value = Convert.ToDecimal(dgvDetalle[13, Indice].Value)*
                                                               TipoCambio;
                                dgvDetalle[14, Indice].Value = Convert.ToDecimal(dgvDetalle[14, Indice].Value)*
                                                               TipoCambio;
                                dgvDetalle[15, Indice].Value = Convert.ToDecimal(dgvDetalle[15, Indice].Value)*
                                                               TipoCambio;
                                dgvDetalle[16, Indice].Value = Convert.ToDecimal(dgvDetalle[16, Indice].Value)*
                                                               TipoCambio;
                            }
                        }
                        else if (Convert.ToInt32(cmbMoneda.SelectedValue) == 2)
                        {
                        }
                    }
                }
            }
        }

        public void calculatotales()
        {
            if (Proceso != 0)
            {
                if (Procede != 3)
                {
                    Decimal bruto = 0;
                    Decimal descuen = 0;
                    Decimal valor = 0;
                    Decimal preciovent = 0;
                    Decimal igvt = 0;

                    foreach (DataGridViewRow row in dgvDetalle.Rows)
                    {
                        bruto = bruto + Convert.ToDecimal(row.Cells[importe.Name].Value);
                        descuen = descuen + Convert.ToDecimal(row.Cells[montodscto.Name].Value);
                        valor = valor + Convert.ToDecimal(row.Cells[valorventa.Name].Value);
                        preciovent = preciovent + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                        igvt = igvt + Convert.ToDecimal(row.Cells[igv.Name].Value);
                    }
                    txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
                    txtDscto.Text = String.Format("{0:#,##0.00}", descuen);
                    txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);

                    //txtIGV.Text = String.Format("{0:#,##0.00}", bruto - descuen - valor);
                    txtIGV.Text = String.Format("{0:#,##0.00}", igvt);
                    //txtPrecioVenta.Text = String.Format("{0:#,##0.00}", bruto);
                    txtPrecioVenta.Text = String.Format("{0:#,##0.00}", preciovent);
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void btnGuardar_Click(object sender, EventArgs e)
        {
            Decimal totalsoles = 0;
            aper = AdmAper.ValidarAperturaDia(frmLogin.iCodSucursal, DateTime.Now.Date, 1, frmLogin.iCodAlmacen);//1 caja ventas
            if (aper != null)
            {
                if (superValidator1.Validate())
                {
                    if (Convert.ToInt32(cli.Moneda) != Convert.ToInt32(cmbMoneda.SelectedValue))
                    {
                        if (Convert.ToInt32(cli.Moneda) == 2 || Convert.ToInt32(cmbMoneda.SelectedValue) == 1)
                            totalsoles = Convert.ToDecimal(txtPrecioVenta.Text) / Convert.ToDecimal(txtTipoCambio.Text);
                        else if (Convert.ToInt32(cli.Moneda) == 1 || Convert.ToInt32(cmbMoneda.SelectedValue) == 2)
                            totalsoles = Convert.ToDecimal(txtPrecioVenta.Text) * Convert.ToDecimal(txtTipoCambio.Text);

                    }
                    else
                    {
                        totalsoles = Convert.ToDecimal(txtPrecioVenta.Text);
                    }
                    if ((totalsoles > Convert.ToDecimal(txtLineaCreditoDisponible.Text)) && Convert.ToInt32(cmbFormaPago.SelectedValue) != 6)
                    {
                        MessageBox.Show("El Monto Excede a la Línea de Crédito");
                    }
                    else
                    {
                        if (Proceso != 0)
                        {
                            venta.CodEmpresa = frmLogin.iCodEmpresa;
                            venta.CodSucursal = frmLogin.iCodSucursal;
                            venta.CodAlmacen = Convert.ToInt32(cmbAlmacen.SelectedValue);
                            venta.CodTipoTransaccion = tran.CodTransaccion;
                            //if (txtCodigoCli.Text != "")
                            //{
                            //    venta.CodCliente = Convert.ToInt32(txtCodigoCli.Text);
                            //}
                            //else
                            //{
                            //    venta.CodCliente = 0;
                            //}
                            venta.CodTipoDocumento = doc.CodTipoDocumento;
                            venta.CodSerie = CodSerie;
                            /* VALIDO QUE LA VENTA SEA AL CONTADO PARA NO ENVIAR SERIE NI CORRELATIVO, TIPO DOC SI ENVIO  */
                            if (Convert.ToInt32(cmbFormaPago.SelectedValue) == 6)
                            {

                                venta.Serie = txtSerie.Text;
                                venta.NumDoc = txtNumero.Text;

                            }
                            else
                            {
                                venta.Serie = txtSerie.Text;
                                venta.NumDoc = txtNumero.Text;
                            }

                            /* VALIDO QUE LA VENTA SEA AL CONTADO PARA NO ENVIAR SERIE NI CORRELATIVO, TIPO DOC SI ENVIO  */
                            venta.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                            if (txtTipoCambio.Visible)
                            {
                                venta.TipoCambio = Convert.ToDecimal(txtTipoCambio.Text);
                            }
                            venta.FechaSalida = dtpFecha.Value;
                            venta.FechaPago = dtpFechaPago.Value;
                            venta.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
                            venta.CodListaPrecio = Convert.ToInt32(cbListaPrecios.SelectedValue);
                            venta.CodVendedor = Convert.ToInt32(cbovendedor.SelectedValue);
                            venta.Comentario = txtComentario.Text;
                            venta.MontoBruto = Convert.ToDecimal(txtBruto.Text);
                            //venta.MontoBruto = Convert.ToDecimal(txtValorVenta.Text);
                            venta.MontoDscto = Convert.ToDecimal(txtDscto.Text);
                            venta.Igv = Convert.ToDecimal(txtIGV.Text);
                            venta.Total = Convert.ToDecimal(txtPrecioVenta.Text);
                            venta.CodUser = frmLogin.iCodUser;
                            txtDetalle.Text = txtDetalle.Text.Replace(" - ", "");
                            txtDetalle.Text = txtDetalle.Text.Replace("\r\n", "\r\n - ");
                            if (txtDetalle.Text != "") txtDetalle.Text = " - " + txtDetalle.Text;
                            venta.Detallecomentario = txtDetalle.Text;
                            if (txtCotizacion.Text == "" || txtCotizacion.Text == ".") { venta.CodCotizacion = 0; } else { venta.CodCotizacion = Convert.ToInt32(txtCotizacion.Text); }
                            venta.Estado = 1;
                            venta.Consultorext = checkBox1.Checked;
                            venta.Codsalidaconsulext = CodSalConsulExt;
                            venta.CodPedido = Convert.ToInt32(txtcodpedido.Text);
                            venta.CodSeparacion = Convert.ToInt32(separacion.CodSeparacion);
                            
                            factura = AdmVenta.FechaCorrelativoAnterior(venta.CodSerie);

                            if (Proceso == 1 || Proceso == 5)
                            {
                                if (factura.FechaSalida > venta.FechaSalida.Date)
                                {
                                    MessageBox.Show("Error No se puede Registrar los Datos. Verifique Fecha");
                                }
                                else
                                {
                                    RecorreDetalle();
                                    venta.Detalle = detalle1;

                                    if (detalle1.Count > 0)
                                    {
                                        venta.Pendiente = venta.Total;
                                        clsCliente cliente1 = AdmCli.ConsultaCliente(txtCodCliente.Text);
                                        clsCliente cliente = AdmCli.MuestraCliente(cliente1.CodCliente);

                                        if (cliente.Tipodocidentidad.Idtipodocumentoidentidad == 1)
                                        {
                                            cliente.Tipodocidentidad.Codsunat = "1";
                                        }
                                        else if (cliente.Tipodocidentidad.Idtipodocumentoidentidad == 2)
                                        {
                                            cliente.Tipodocidentidad.Codsunat = "6";
                                        }

                                        venta.CodCliente = cliente.CodCliente;

                                        
                                        if (ncredito.Count > 0)
                                        {
                                            frmCancelarPago form = new frmCancelarPago();
                                            form.CodNota = venta.CodFacturaVenta;
                                            form.VentComp = 1;
                                            form.tipo = 3;
                                            form.CodCliente = cli.CodCliente;
                                            form.ShowDialog();
                                            this.Close();
                                        }
                                        else
                                        {
                                            //se comprueba que el pago sea al contado y que la trnasaccion sea ingreso por compra
                                            if (fpago.Dias > 0 && venta.CodTipoTransaccion == 7)
                                            {
                                                //        if (venta.CodSeparacion != 0)
                                                //        {
                                                //            MessageBox.Show("Debe de retirar de caja de separacion " + venta.Total, "Venta",
                                                //MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                //        }

                                                //ingresarpago();//aqui cambiar esto 

                                                if (AdmVenta.insert(venta))
                                                {

                                                    CodVenta = venta.CodFacturaVenta;

                                                    MessageBox.Show("Los datos se guardaron correctamente",
                                                    "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                                    //FACTURACION ELECTRONICA

                                                    await facturacion.GeneraDocumento(cliente, venta, detalle1);
                                                    txtNumDoc.Text = venta.CodFacturaVenta.PadLeft(11, '0');
                                                    fnImprimir();
                                                    this.Close();
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Ocurrió un problema al registrar la venta.", "Registro de Venta",
                                                                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                }
                                            }
                                            else
                                            {
                                                frmCancelarPago form = new frmCancelarPago();
                                                form.CodNota = venta.CodFacturaVenta;
                                                form.tipo = 3;
                                                form.tip = 3;
                                                form.Monto = venta.Total;
                                                form.venta = venta;
                                                form.montoPag = 0;
                                                form.VentComp = 1;
                                                form.ShowDialog();

                                                if (form.caja_aperturada)
                                                {
                                                    if (form.ventana_cobro)
                                                    {
                                                        if (form.ventaRecibida)
                                                        {
                                                            if (venta.CodFacturaVenta != null)
                                                            {
                                                                CodVenta = venta.CodFacturaVenta;
                                                                txtNumDoc.Text = venta.CodFacturaVenta.PadLeft(11, '0');
                                                                Proceso = 0;
                                                                //btnImprimir.Visible = true;

                                                                if (venta.FormaPago != 6)
                                                                {
                                                                    xgenerar = true;
                                                                    this.Close();
                                                                }

                                                                await facturacion.GeneraDocumento(cliente, venta, detalle1);
                                                                fnImprimir();
                                                                this.Close();

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                          
                                        }

                                    }
                                }
                            }   
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe Aperturar Caja", "Apertura Caja", MessageBoxButtons.OK,
                       MessageBoxIcon.Warning);
                this.Close();
            }
        }

        private void VerificaSaldoCaja()
        {
            Caja = AdmCaja.ValidarAperturaDia(frmLogin.iCodSucursal, DateTime.Now.Date, 1, frmLogin.iCodAlmacen);//1 caja ventas
            CodigoCaja = Caja.Codcaja;
        }

        private void ingresarpago()
        {
            //Se quito para enviar el pago directo cuando es contado por defecto efectivo
            //frmCancelarPago form = new frmCancelarPago();
            //form.CodNota = venta.CodFacturaVenta;
            //form.tipo = 3;
            //form.tip = 3;
            //form.Monto = venta.Total;
            //form.venta = venta;
            //form.montoPag = 0;
            //form.ShowDialog();
            //FIn se quito para enviar el pago directo cuando es contado por defecto efectivo

            VerificaSaldoCaja();

            Pag.CodNota = venta.CodFacturaVenta.ToString();
            Pag.CodLetra = 0;
            Pag.CodTipoPago = 5; //metodo de pago
            Pag.CodMoneda = venta.Moneda;
            //Pag.CodCobrador = Convert.ToInt32(cbovendedor.SelectedValue); //Cobrador
            Pag.CodCobrador = Convert.ToInt32(frmLogin.iCodUser);
            Pag.Tipo = true;// total o parcial
            Pag.IngresoEgreso = true;//ingreso
            Pag.TipoCambio = Convert.ToDecimal(venta.TipoCambio);
            Pag.MontoPagado = Convert.ToDecimal(venta.Total);
            Pag.MontoCobrado = Convert.ToDecimal(venta.Total);
            Pag.Vuelto = 0;
            Pag.codCtaCte = 0;
            Pag.CtaCte = "";
            Pag.NOperacion = "";
            Pag.NCheque = "";
            Pag.FechaPago = venta.FechaPago.Date;
            Pag.Observacion = "";
            Pag.CodUser = frmLogin.iCodUser;
            Pag.CodAlmacen = frmLogin.iCodAlmacen;
            //Pag.CodSerie = CodSerie;
            Pag.CodSucursal = frmLogin.iCodSucursal;
            Pag.CodDoc = 18;
            Pag.Serie = "";
            Pag.NumDoc = "";
            Pag.Referencia = "";
            Pag.Codcaja = CodigoCaja; 
            Pag.CodBanco = 0;
            Pag.CodTarjeta = 0;
            Pag.Aprobado = 4;
            //montoPag = 1;
            if (AdmPagos.insert(Pag))
            {
                xgenerar = true;
                fnImprimir();
                //Reiniciar Formulario
                //nuevaVenta();
                this.Close();
            }
        }

        private void nuevaVenta()
        {
            frmVenta form = new frmVenta();
            form.MdiParent = this.MdiParent;
            form.Proceso = 1;
            form.Show();
            this.Close();
            
        }

        private void RecorreDetalle()
        {
            detalle.Clear();
            detalle1.Clear();
            if (dgvDetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    añadedetalle(row);
                }
            }
            //nota.Detalle = detalle;
        }
        private void añadedetalle(DataGridViewRow fila)
        {
            clsDetalleFacturaVenta deta = new clsDetalleFacturaVenta();
            deta.Tipoimpuesto = admtimpuesto.listar_tipoimpuesto_xid(Convert.ToInt32(cbTimpuesto.SelectedValue));
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodVenta = Convert.ToInt32(venta.CodFacturaVenta);
            deta.CodAlmacen = Convert.ToInt32(cmbAlmacen.SelectedValue);
            deta.Descripcion = Convert.ToString(fila.Cells[descripcion.Name].Value);
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            deta.SerieLote = fila.Cells[serielote.Name].Value.ToString();
            deta.Cantidad = Convert.ToDecimal(fila.Cells[cantidad.Name].Value);
            deta.PrecioUnitario = Convert.ToDecimal(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDecimal(fila.Cells[importe.Name].Value);
            //deta.Subtotal = Convert.ToDecimal(fila.Cells[importe.Name].Value);
            //deta.Subtotal = Convert.ToDecimal(fila.Cells[valorventa.Name].Value);
            deta.Descuento1 = Convert.ToDecimal(fila.Cells[dscto1.Name].Value);
            deta.MontoDescuento = Convert.ToDecimal(fila.Cells[montodscto.Name].Value);
            deta.Igv = Convert.ToDecimal(fila.Cells[igv.Name].Value);
            //  deta.Importe = Convert.ToDecimal(fila.Cells[precioventa.Name].Value);
            deta.Importe = Convert.ToDecimal(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDecimal(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDecimal(fila.Cells[valoreal.Name].Value);
            deta.CodUser = frmLogin.iCodUser;
            deta.CantidadPendiente = Convert.ToDecimal(fila.Cells[cantidad.Name].Value);
            deta.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
            //deta.CodTalla= Convert.ToInt32(fila.Cells[codTalla.Name].Value);

            if (Procede == 3)//cotizacion
            {
                deta.CodDetalleCotizacion = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
            }
            else// venta 
            {
                deta.CodDetalleCotizacion = 0;
            }
            
            if (Procede == 4)//pedido
            {
                deta.CodDetallePedido = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
            }
            else// venta
            {
                deta.CodDetallePedido = 0;
            }
            if (Procede == 7)
            {
                deta.CodDetalleSeparacion = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
            }
            else
            {
                deta.CodDetalleSeparacion = 0;
            }
            detalle1.Add(deta);
        }

        private void RecorreDetalleGuia()
        {
            detalle.Clear();
            if (dgvDetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    añadedetalleguia(row);
                }
            }
            //nota.Detalle = detalle;
        }
        private void añadedetalleguia(DataGridViewRow fila)
        {
            clsDetalleGuiaRemision deta = new clsDetalleGuiaRemision();
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodGuiaRemision = Convert.ToInt32(guia.CodGuiaRemision);
            deta.CodAlmacen = frmLogin.iCodAlmacen;
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            deta.SerieLote = fila.Cells[serielote.Name].Value.ToString();
            deta.Cantidad = Convert.ToDecimal(fila.Cells[cantidad.Name].Value);
            if (Convert.ToBoolean(guia.Facturado)) { deta.CantidadPendiente = 0; deta.Pendiente = false; } else { deta.CantidadPendiente = deta.Cantidad; deta.Pendiente = true; }
            deta.CodUser = frmLogin.iCodUser;
            detalleg.Add(deta);
        }

        private void txtPedido_Leave(object sender, EventArgs e)
        {
            //VerificarCabecera();
            //if (Validacion && Proceso == 1)
            //{
            //    btnDetalle.Enabled = true;
            //}
        }

        private void txtAutorizacion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmAutorizado"] != null)
                {
                    Application.OpenForms["frmAutorizado"].Activate();
                }
                else
                {
                    frmAutorizado form = new frmAutorizado();
                    form.Proceso = 3;                   
                    form.ShowDialog();
                    aut = form.aut;
                    CodAutorizado = aut.CodAutorizado;
                    if (CodAutorizado != 0) { CargaAutorizado(); ProcessTabKey(true); }
                }
            }
        }

        private void CargaAutorizado()
        {
            aut = AdmAut.MuestraAutorizado(CodAutorizado);
            txtAutorizacion.Text = aut.CodAutorizado.ToString();
            lbAutorizado.Text = aut.Nombre;            
        }

        private void txtAutorizacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.enteros(e);
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtAutorizacion.Text != "")
                {
                    if (BuscaAutorizado())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("El codigo no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private Boolean BuscaAutorizado()        
        {
            aut = AdmAut.MuestraAutorizado(Convert.ToInt32(txtAutorizacion.Text));
            if (aut != null)
            {
                lbAutorizado.Text = aut.Nombre;
                CodAutorizado = aut.CodAutorizado;
                return true;                
            }
            else
            {
                lbAutorizado.Text = "";
                CodAutorizado = 0;
                return false;
            }
        }

        private void txtAutorizacion_Leave(object sender, EventArgs e)
        {
            //if (CodAutorizado== 0)
            //{
            //    txtAutorizacion.Focus();
            //}
            //VerificarCabecera();
        }

        private void txtSerie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmSerie"] != null)
                {
                    Application.OpenForms["frmSerie"].Activate();
                }
                else
                {
                    frmSerie form = new frmSerie();
                    form.Proceso = 3;
                    form.DocSeleccionado = CodDocumento;
                    form.ShowDialog();
                    ser = form.ser;
                    CodSerie = ser.CodSerie;
                    manual=Convert.ToInt32(ser.PreImpreso);
                    if (CodSerie != 0)
                    {
                        txtSerie.Text = ser.Serie;                        
                        //if (Procede != 4) txtNumero.Text = ser.Numeracion.ToString();
                        //else txtNumero.Text = numSerie;
                    }
                    if (CodSerie != 0) { ProcessTabKey(true); }
                }
            }
        }

       
        private void txtCodDocumento_TextChanged(object sender, EventArgs e)
        {   
            txtSerie.Text = "";
            txtNumero.Text = "";
            CodSerie = 0;           
        }

        private void txtComentario_Leave(object sender, EventArgs e)
        {
            //VerificarCabecera();
        }

        public void cmbFormaPago_SelectionChangeCommitted(object sender, EventArgs e)
        {
            fpago = AdmPago.CargaFormaPago(Convert.ToInt32(cmbFormaPago.SelectedValue));
            if (fpago.Dias > forma.Dias)
            {
                DialogResult result =
                    MessageBox.Show("Esta forma de pago excede a la Forma de Pago del Cliente" + Environment.NewLine +
                                    "Máx.FormaPago del Cliente = " + forma.Descripcion, "Facturación Venta",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (result == DialogResult.OK)
                {
                    cmbFormaPago.SelectedValue = fpago.CodFormaPago;
                    //cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, eeee);
                }
            }
            
        }

        private void cbListaPrecios_SelectionChangeCommitted(object sender, EventArgs e)
        {
                CodLista = Convert.ToInt32(cbListaPrecios.SelectedValue);            
                actualizaprecios();
                calculatotales();
                //btnNuevo.Enabled = true;
                btnEditar.Enabled = true;
                btnEliminar.Enabled = true;
                cbovendedor.Focus();            
        }

        private void actualizaprecios()
        {
            try
            {

                if (Proceso != 0)
                {
                    if (Procede != 3)
                    {
                        Int32 codProduct = 0;
                        Decimal precioa, cantidada, brutoa, montodescuentoa, valorventaa, igva, precioventaa, precioreala, valorreala, factorigva;
                        DataTable precios = admLista.CargaListaPrecios(Convert.ToInt32(cbListaPrecios.SelectedValue));

                        foreach (DataGridViewRow row in dgvDetalle.Rows)
                        {
                            codProduct = Convert.ToInt32(row.Cells[codproducto.Name].Value);
                            foreach (DataRow r in precios.Rows)
                            {

                                if (codProduct == Convert.ToInt32(r["codProducto"].ToString()))
                                {
                                    if (cmbMoneda.SelectedIndex == 1)
                                    {
                                        precioa = Convert.ToDecimal(r["precio"]);
                                        row.Cells[preciounit.Name].Value = String.Format("{0:#,##0.00}", r["precio"]);
                                        cantidada = Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                                        brutoa = cantidada * precioa;
                                        row.Cells[importe.Name].Value = String.Format("{0:#,##0.00}", brutoa);

                                        precioventaa = brutoa * (1 - (Convert.ToDecimal(row.Cells[dscto1.Name].Value) / 100)) * (1 - (Convert.ToDecimal(row.Cells[dscto2.Name].Value) / 100)) * (1 - (Convert.ToDecimal(row.Cells[dscto3.Name].Value) / 100));
                                        montodescuentoa = brutoa - precioventaa;
                                        row.Cells[montodscto.Name].Value = String.Format("{0:#,##0.00}", montodescuentoa);
                                        if (r["precioneto"].ToString().Equals(r["precio"].ToString()))
                                        {
                                            valorventaa = precioventaa;
                                        }
                                        else
                                        {
                                            factorigva = frmLogin.Configuracion.IGV / 100 + 1;
                                            valorventaa = precioventaa / factorigva;
                                        }
                                        igva = precioventaa - valorventaa;
                                        precioreala = precioventaa / cantidada;
                                        valorreala = valorventaa / cantidada;
                                        row.Cells[precioventa.Name].Value = String.Format("{0:#,##0.00}", precioventaa);
                                        row.Cells[valorventa.Name].Value = String.Format("{0:#,##0.00}", valorventaa);
                                        row.Cells[precioreal.Name].Value = String.Format("{0:#,##0.00}", precioreala);
                                        row.Cells[valoreal.Name].Value = String.Format("{0:#,##0.00}", valorreala);
                                        row.Cells[igv.Name].Value = String.Format("{0:#,##0.00}", igva);
                                    }
                                    else
                                    {
                                        precioa = Convert.ToDecimal(r["precio"]) * Convert.ToDecimal(txtTipoCambio.Text);
                                        //row.Cells[preciounit.Name].Value = String.Format("{0:#,##0.00}", r["precio"]);
                                        row.Cells[preciounit.Name].Value = String.Format("{0:#,##0.00}", precioa);
                                        cantidada = Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                                        brutoa = cantidada * precioa;
                                        row.Cells[importe.Name].Value = String.Format("{0:#,##0.00}", brutoa);

                                        precioventaa = brutoa * (1 - (Convert.ToDecimal(row.Cells[dscto1.Name].Value) / 100)) * (1 - (Convert.ToDecimal(row.Cells[dscto2.Name].Value) / 100)) * (1 - (Convert.ToDecimal(row.Cells[dscto3.Name].Value) / 100));
                                        montodescuentoa = brutoa - precioventaa;
                                        row.Cells[montodscto.Name].Value = String.Format("{0:#,##0.00}", montodescuentoa);
                                        if (r["precioneto"].ToString().Equals(r["precio"].ToString()))
                                        {
                                            valorventaa = precioventaa;
                                        }
                                        else
                                        {
                                            factorigva = frmLogin.Configuracion.IGV / 100 + 1;
                                            valorventaa = precioventaa / factorigva;
                                        }
                                        igva = precioventaa - valorventaa;
                                        precioreala = precioventaa / cantidada;
                                        valorreala = valorventaa / cantidada;
                                        row.Cells[precioventa.Name].Value = String.Format("{0:#,##0.00}", precioventaa);
                                        row.Cells[valorventa.Name].Value = String.Format("{0:#,##0.00}", valorventaa);
                                        row.Cells[precioreal.Name].Value = String.Format("{0:#,##0.00}", precioreala);
                                        row.Cells[valoreal.Name].Value = String.Format("{0:#,##0.00}", valorreala);
                                        row.Cells[igv.Name].Value = String.Format("{0:#,##0.00}", igva);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void txtPDescuento_TextChanged(object sender, EventArgs e)
        {
            calculadescuentogeneral();
        }

        private void txtPDescuento_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.enteros(e);
            if (e.KeyChar == (char)Keys.Return)
            {
                calculadescuentogeneral();             
            }            
        }

        private void calculadescuentogeneral()
        {
            //Decimal brutodg = 0;
            //Decimal dsctodg = 0;
            //Decimal DsctoGlobal = 0;
            //Decimal precioventadg = 0;
            //Decimal valorventadg = 0;

            //if (txtBruto.Text != "") { brutodg = Convert.ToDecimal(txtBruto.Text); } else { brutodg = 0; }
            //if (txtDscto.Text != "") { dsctodg = Convert.ToDecimal(txtDscto.Text); } else { dsctodg = 0; }

            //if (txtPDescuento.Text != "" && txtPrecioVenta.Text != "")
            //{
            //    DsctoGlobal = (Convert.ToDecimal(txtBruto.Text) - dsctodg) * (Convert.ToDecimal(txtPDescuento.Text) / 100);
            //    txtDsctoGobal.Text = String.Format("{0:#,##0.00}", DsctoGlobal.ToString());
            //    precioventadg = brutodg - dsctodg - DsctoGlobal;
            //    txtPrecioVenta.Text = String.Format("{0:#,##0.00}", precioventadg);
            //    //valorventadg = precioventadg / (1 + (frmLogin.Configuracion.IGV / 100));
            //    txtValorVenta.Text = String.Format("{0:#,##0.00}", valorventadg);
            //    txtIGV.Text = String.Format("{0:#,##0.00}", precioventadg - valorventadg);
            //    //txtValorVenta.Text = String.Format("{0:#,##0.00}", precioventadg * frmLogin.Configuracion.IGV / 100);
            //    //txtIGV.Text = String.Format("{0:#,##0.00}", precioventadg * (1 - (frmLogin.Configuracion.IGV / 100)));
            //}
            //else
            //{
            //    DsctoGlobal = 0;
            //    txtDsctoGobal.Text = String.Format("{0:#,##0.00}", DsctoGlobal.ToString());
            //    calculatotales();
            //}   
        }

        public void fnImprimir()
        {
            try
            {
                //if (BuscaSerie())
                //{
                //    if (BuscaSerie2())
                //    {
                //        txtSerie.Text = ser.Serie;
                //    }

                //}

                if (venta.CodTipoDocumento == 1)//Es Boleta
                {
                    printaBoleta();
                }
                else if(venta.CodTipoDocumento==2)//Es Factura
                {
                    printaFactura();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void btnImprimir_Click(object sender, EventArgs e)
        {
            //Boolean rpta;
            try
            {          

                fnImprimir();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            
        }

        private void printaRecibo(string CodPago)
        {
            try
            {
                /* MUESTRO LA FACTURA PARA IMPRIMIRLA */
                CRImpresionPago rpt = new CRImpresionPago();
                frmRptImpresionPago frm = new frmRptImpresionPago();
                CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                rptoption.PrinterName = ser.NombreImpresora;//Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]);
                rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza"); 
                rpt.SetDataSource(dsf.ReporteImpresionPago(Convert.ToInt32(CodPago), frmLogin.iCodAlmacen));
                frm.cRVImpresionPago.ReportSource = rpt;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        clsTipoDocumento doc2 = new clsTipoDocumento();
        clsSerie seri2 = new clsSerie();
        private String siglaPago, seriePago, numeroPago;
        private Int32 codDocumentoPago = 0;
        private bool ActualizaCobro(string CodPago)
        {
            String sigl = "";
            Boolean devuelve = false;
            try
            {
                sigl = "RC";
                if (valida_serie(sigl))
                {
                    seri2 = null;
                    seri2 = Admser.BuscaSeriexDocumento(codDocumentoPago, frmLogin.iCodAlmacen);
                    if (seri2 != null)
                    {
                        seriePago = seri2.Serie;
                        numeroPago = seri2.Numeracion.ToString();
                        if (AdmPagos.ActualizaPagoAprobado(seriePago, numeroPago, Convert.ToInt32(CodPago)))
                        {
                            devuelve = true;
                        }
                        else
                        {
                            devuelve = false;
                        }
                    }
                }

                return devuelve;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private Boolean valida_serie(string sigl)
        {
            doc2 = null;
            try
            {
                doc2 = Admdoc.BuscaTipoDocumento(sigl);
                if (doc2 != null)
                {
                    codDocumentoPago = doc2.CodTipoDocumento;
                    siglaPago = doc2.Sigla;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void printaFactura()
        {
            try
            {
                DataSet jes = new DataSet();
                ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                frmRptFactura frm = new frmRptFactura();
                CRReporteFactura rpt = new CRReporteFactura();

                //datos para buscar el QR, cargamos datos de la empresa
                clsEmpresa empre = admemp.CargaEmpresa(frmLogin.iCodEmpresa);

                rpt.Load("CRReporteFactura.rpt");
                jes = ds1.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta));

                Byte[] logoEmp = CargarImagen(@"C:\DOCUMENTOS-" + empre.Ruc + "\\CERTIFIK\\QR\\" + empre.Ruc + "-01-F" + venta.Serie + "-" + venta.NumDoc + ".jpeg");


                foreach (DataTable mel in jes.Tables)
                {
                    foreach (DataRow changesRow in mel.Rows)
                    {
                        changesRow["firma"] = logoEmp;
                    }

                    if (mel.HasErrors)
                    {
                        foreach (DataRow changesRow in mel.Rows)
                        {
                            if ((int)changesRow["Item", DataRowVersion.Current] > 100)
                            {
                                changesRow.RejectChanges();
                                changesRow.ClearErrors();
                            }
                        }
                    }
                }


                rpt.SetDataSource(jes);
                CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                rptoption.PrinterName = ser.NombreImpresora;
                rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza");           
                rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(50, 5, 0, 10));
                rpt.PrintToPrinter(1, false, 1, 1);


                if (AdmVenta.ActualizaEstadoImpreso(Convert.ToInt32(venta.CodFacturaVenta)))
                {
                    rpta = true;
                }
                else
                {
                    rpta = false;
                }
                rpt.Close();
                rpt.Dispose();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void printaBoleta()
        {
            try
            {
                DataSet jes = new DataSet();
                ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                frmRptFactura frm = new frmRptFactura();
                CRReporteFactura rpt = new CRReporteFactura();

                //datos para buscar el QR, cargamos datos de la empresa
                //clsEmpresa empre = admemp.CargaEmpresa(frmLogin.iCodEmpresa);

                rpt.Load("CRReporteFactura.rpt");
                jes = ds1.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta));

                Byte[] logoEmp = CargarImagen(@"C:\DOCUMENTOS-" + frmLogin.RUC + "\\CERTIFIK\\QR\\" + frmLogin.RUC + "-03-B" + venta.Serie +"-"+venta.NumDoc +".jpeg");

                foreach (DataTable mel in jes.Tables)
                    {
                    foreach (DataRow changesRow in mel.Rows)
                    {
                        changesRow["firma"] = logoEmp;
                    }

                    if (mel.HasErrors)
                    {
                        foreach (DataRow changesRow in mel.Rows)
                        {
                            if ((int)changesRow["Item", DataRowVersion.Current] > 100)
                            {
                                changesRow.RejectChanges();
                                changesRow.ClearErrors();
                            }
                        }
                    }
                }


                rpt.SetDataSource(jes);
                CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                rptoption.PrinterName = ser.NombreImpresora;
                rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza");           
                rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(50, 5, 0, 10));
                rpt.PrintToPrinter(1, false, 1, 1);


                //if (AdmVenta.ActualizaEstadoImpreso(Convert.ToInt32(venta.CodFacturaVenta)))
                //{
                //    rpta = true;
                //}
                //else
                //{
                //    rpta = false;
                //}
                rpt.Close();
                rpt.Dispose();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static Byte[] CargarImagen(string rutaArchivo)
        {
            if (rutaArchivo != "")
            {
                try
                {
                    FileStream Archivo = new FileStream(rutaArchivo, FileMode.Open);//Creo el archivo
                    BinaryReader binRead = new BinaryReader(Archivo);//Cargo el Archivo en modo binario
                    Byte[] imagenEnBytes = new Byte[(Int64)Archivo.Length]; //Creo un Array de Bytes donde guardare la imagen
                    binRead.Read(imagenEnBytes, 0, (int)Archivo.Length);//Cargo la imagen en el array de Bytes
                    binRead.Close();
                    Archivo.Close();
                    return imagenEnBytes;//Devuelvo la imagen convertida en un array de bytes
                }
                catch
                {
                    return new Byte[0];
                }
            }
            return new byte[0];
        }

        private bool ActualizaCorrelativos(int CodSerie, string txtSeries, string txtNumeros, string CodVenta)
        {
            try
            {
                if (AdmVenta.actualizaFactura_venta(CodSerie, txtSeries, txtNumeros, CodVenta))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        

        private Boolean BuscaGuia()
        {
            guia = AdmGuia.BuscaGuiaRemision(txtGuias.Text, frmLogin.iCodAlmacen);            
            if (guia != null)
            {
                CodGuia = Convert.ToInt32(guia.CodGuiaRemision);
                return true;
            }
            else
            {
                CodGuia = 0;
                return false;
            }
        }

        private void CargaGuia()
        {
            try
            {
                guia = AdmGuia.CargaGuiaRemision(Convert.ToInt32(CodGuia));
                if (guia != null)
                {
                    txtGuias.Text = guia.CodGuiaRemision;
                    

                    if (txtCodCliente.Enabled)
                    {
                        CodCliente = guia.CodCliente;

                        if (txtCodCliente.Enabled)
                        {
                            CodCliente = guia.CodCliente;
                            cli = AdmCli.MuestraCliente(CodCliente);
                            txtCodCliente.Text = cli.CodigoPersonalizado;
                            if (cli.Ruc != "")
                            {
                                txtDocRef.Text = "FT";
                                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                                txtTransaccion_KeyPress(txtDocRef, ee);
                                
                                txtTransaccion_KeyPress(txtDocRef, ee);
                            }
                            else
                            {
                                txtDocRef.Text = "BV";
                                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                                txtTransaccion_KeyPress(txtDocRef, ee);
                              
                                txtTransaccion_KeyPress(txtDocRef, ee);
                            }

                            txtNombreCliente.Text = cli.RazonSocial;
                            
                            if (cli.CodListaPrecio != 0)
                            {
                                EventArgs ee = new EventArgs();
                                cbListaPrecios_SelectionChangeCommitted(cbListaPrecios, ee);
                            }
                            else
                            {
                                CodLista = 0;
                            }

                           
                            if (cli.FormaPago != 0)
                            {
                                EventArgs ee = new EventArgs();
                                cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                            }
                            else
                            {
                                dtpFechaPago.Value = DateTime.Today;
                            }

                            txtPDescuento.Text = cli.Descuento.ToString();

                        }   
                    }
                    //dtpFecha.Value = guia.FechaEmision;                  
                    txtComentario.Text = guia.Comentario;

                    CargaDetalleGuia();
                }
                else
                {
                    MessageBox.Show("El documento solicitado no existe", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                return;
            }
 
        }

        public void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            frmVenta form2 = new frmVenta();
            form2.MdiParent = this.MdiParent;
            form2.consultorext = consultorext;
            form2.Proceso = 1;
            form2.Show();
            this.Close();
        }

        private void dgvDetalle_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (Procede == 1)
            {
                if (Proceso == 1)
                {
                    if (txtPDescuento.Text != "")
                    {
                        calculatotales();
                        calculadescuentogeneral();
                    }
                    else
                    {
                        calculatotales();
                    }

                    if (dgvDetalle.RowCount > 0)
                    {
                        int Indice = 0;
                        Indice = dgvDetalle.RowCount - 1;

                        if (cmbMoneda.SelectedIndex == 0)
                        {
                            if (TipoCambio != 0)
                            {
                                dgvDetalle[8, Indice].Value = Convert.ToDecimal(dgvDetalle[8, Indice].Value)*TipoCambio;
                                dgvDetalle[9, Indice].Value = Convert.ToDecimal(dgvDetalle[9, Indice].Value)*TipoCambio;
                                dgvDetalle[13, Indice].Value = Convert.ToDecimal(dgvDetalle[13, Indice].Value)*
                                                               TipoCambio;
                                dgvDetalle[14, Indice].Value = Convert.ToDecimal(dgvDetalle[14, Indice].Value)*
                                                               TipoCambio;
                                dgvDetalle[15, Indice].Value = Convert.ToDecimal(dgvDetalle[15, Indice].Value)*
                                                               TipoCambio;
                                dgvDetalle[16, Indice].Value = Convert.ToDecimal(dgvDetalle[16, Indice].Value)*
                                                               TipoCambio;
                            }
                        }
                        else if (cmbMoneda.SelectedIndex == 1)
                        {
                        }
                    }
                }
            }
        }

        private void dgvDetalle_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (Procede == 1 || Procede == 2)
            {
                if (dgvDetalle.Columns[e.ColumnIndex].Name == "precioventa")
                {
                    if (Proceso == 1 || Proceso == 2)
                    {
                        if (txtPDescuento.Text != "")
                        {
                            calculatotales();
                            calculadescuentogeneral();
                        }
                        else
                        {
                            calculatotales();
                        }
                    }
                }
            }
        }  

     

    
        private void cmbMoneda_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (dgvDetalle.RowCount > 0)
            {
                VentaEnMoneda();
            }
            calculatotales();
            mon = Convert.ToInt32(cmbMoneda.SelectedValue);
            calculatotales();
            txtComentario.Focus();
        }

        private void txtGuias_KeyDown(object sender, KeyEventArgs e)
        {
          
           
        }

        

        public void txtCotizacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtCotizacion.Text != "")
                {
                    if (BuscaCotizacion())
                    {
                        CargaCotizacion();
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Cotizacion no existe o ya no esta vigente", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ext.limpiar(this.Controls);
                        cargaAlmacenes();
                    }
                }
            }
        }

        private Boolean BuscaCotizacion()
        {
            coti = AdmCoti.BuscaCotizacion(txtCotizacion.Text, frmLogin.iCodAlmacen);
            if (coti != null)
            {
                codCotizacion = Convert.ToInt32(coti.CodCotizacion);
                return true;
            }
            else
            {
                codCotizacion = 0;
                return false;
            }

        }

        private void CargaCotizacion()
        {
            try
            {
                coti = AdmCoti.CargaCotizacion(Convert.ToInt32(codCotizacion), frmLogin.iCodAlmacen);
                if (coti != null)
                {
                    txtCotizacion.Text = coti.CodCotizacion;
                    if (txtCodCliente.Enabled)
                    {
                        CodCliente = coti.CodCliente;
                        CargaCliente();
                        //
                        if (ret == 0)
                        {
                           
                                txtCodigoCli.Text = coti.CodCliente.ToString();
                                txtCodCliente.Text = coti.CodigoPersonalizado;
                                txtNombreCliente.Text = coti.Nombre;
                                txtDireccionCliente.Text = coti.Direccion;                            
                                txtLineaCredito.Text = cli.LineaCredito.ToString();
                                txtLineaCreditoDisponible.Text = cli.LineaCreditoDisponible.ToString();
                                txtLineaCreditoUso.Text = cli.LineaCreditoUsado.ToString();

                            
                            if (coti.RUCCliente != "")// hay que automatizar esto dependiendo de la sucursal la serie varia por sucursal
                            {
                                txtDocRef.Text = "FT";
                                KeyPressEventArgs ee = new KeyPressEventArgs((char) Keys.Return);
                                txtDocRef_KeyPress(txtDocRef, ee);
                               // txtSerie.Text = "001";
                                txtSerie_KeyPress(txtDocRef, ee);
                            }
                            else
                            {
                                txtDocRef.Text = "BV";
                                KeyPressEventArgs ee = new KeyPressEventArgs((char) Keys.Return);
                                txtDocRef_KeyPress(txtDocRef, ee);
                               // txtSerie.Text = "001";
                                txtSerie_KeyPress(txtDocRef, ee);
                            }
                          
                            cmbMoneda.SelectedValue = coti.Moneda;
                            txtTipoCambio.Text = coti.TipoCambio.ToString();
                            
                            txtComentario.Text = coti.Comentario;
                            txtBruto.Text = String.Format("{0:#,##0.00}", coti.MontoBruto);
                            txtDscto.Text = String.Format("{0:#,##0.00}", coti.MontoDscto);
                            txtValorVenta.Text = String.Format("{0:#,##0.00}", coti.Total - coti.Igv);
                            txtIGV.Text = String.Format("{0:#,##0.00}", coti.Igv);
                            txtPrecioVenta.Text = String.Format("{0:#,##0.00}", coti.Total);
                            CargaDetalleCotizacion();
                            BloquearEdicion(true);
                        }
                    }
                   
                }
                else
                {
                    MessageBox.Show("El documento solicitado no existe", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void txtCotizacion_Leave(object sender, EventArgs e)
        {
            //VerificarCabecera();
        }

        public void txtGuias_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtGuias.Text != "")
                {
                    if (BuscaGuia())
                    {
                        CargaGuia();
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Pedido no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        
        public List<Int32> carga_Correlativos()
        {
            Int32 j = 0;
            datos = AdmVenta.ListaFacturaVenta(frmLogin.iCodAlmacen);
            ser = Admser.MuestraSerie(8, frmLogin.iCodAlmacen);
            correlativo.Clear();
            for (int i = ser.Inicio; i < ser.Numeracion; i++)
            {
                if(j< datos.Rows.Count)
                {
                    if(i == Convert.ToInt32(datos.Rows[j]["numDocumento"]))
                    {
                        j++;
                        fecha1 = Convert.ToDateTime(datos.Rows[j-1]["fechasalida"]);
                    }
                    
                    else
                    {
                        correlativo.Add(i);
                        fecha2 = Convert.ToDateTime(datos.Rows[j]["fechasalida"]);
                    }
                    
                }
            }
            return correlativo;
        }

        private Boolean rpta;

        public Boolean valida_existente(Int32 serie)
        {
            datos = AdmVenta.ListaFacturaVenta(frmLogin.iCodAlmacen);
            for (Int32 j = 0; j < datos.Rows.Count; j++)
            {
                if (serie == Convert.ToInt32(datos.Rows[j]["numDocumento"]))
                {
                    rpta = false;
                }
                else rpta = true;
            }
            return rpta;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmFacturasManuales frm = new frmFacturasManuales();
            carga_Correlativos();
            frm.num_correlativo = carga_Correlativos();
            frm.ShowDialog();
        }

       

        private void cmbAlmacen_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cmbAlmacen.Enabled = false;
            btnNuevo.Focus();
        }

        private void dgvDetalle_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            cmbAlmacen_SelectionChangeCommitted(sender, e);
            Int32 codProducto = 0;
            if (dgvDetalle.Rows.Count >= 1 && e.Row.Selected)
            {
                codProducto = Convert.ToInt32(e.Row.Cells[codproducto.Name].Value.ToString());
            }
            CargaFotografia(codProducto);
        }

        private clsAdmFotografia admfotografia = new clsAdmFotografia();

        private void CargaFotografia(Int32 codProducto)
        {
            clsEntFotografia entfoto = new clsEntFotografia();
            entfoto = admfotografia.CargaFotografia(codProducto);
            if (entfoto != null)
            {
                pbFoto.Image = entfoto.Fotografia;
                pbFoto.SizeMode = PictureBoxSizeMode.StretchImage;
                //btnGuardar.Visible = false;
            }
        }

        private void ckbguia_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbguia.Checked == true) groupBox5.Visible = true;
            else groupBox5.Visible = false;
        }

        private void txtRazonSocialTransporte_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmEmpresaTransporte"] != null)
                {
                    Application.OpenForms["frmEmpresaTransporte"].Activate();
                }
                else
                {
                    frmEmpresaTransporte form = new frmEmpresaTransporte();
                    form.Proceso = 3;
                    form.ShowDialog();
                    empT = form.emp;
                    CodEmpresaTransporte = empT.CodEmpresaTranporte;
                    if (CodEmpresaTransporte != 0) { CargaEmpresaTransporte(); ProcessTabKey(true); }
                }
            }
        }

        private void CargaEmpresaTransporte()
        {
            empT = AdmET.MuestraEmpresaTranporte(empT.CodEmpresaTranporte);
            if (empT != null)
            {
                txtRazonSocialTransporte.Text = empT.RazonSocial;
            }
            else
            {
                txtRazonSocialTransporte.Text = "";
            }
        }

       private void txtSerieG_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.F1)
           {
               if (Application.OpenForms["frmSerie"] != null)
               {
                   Application.OpenForms["frmSerie"].Activate();
               }
               else
               {
                   frmSerie form = new frmSerie();
                   form.DocSeleccionado = 11;
                   //form.Sigla = doc.Sigla;
                   form.Proceso = 3;
                   form.ShowDialog();
                   ser = form.ser;
                   CodSerieG = ser.CodSerie;
                   numG = ser.Numeracion;
                   if (CodSerieG != 0)
                   {
                       if (ser.PreImpreso)
                       {
                           CodSerieG = ser.CodSerie;
                           txtSerieG.Text = ser.Serie;
                           txtcodserie.Text = ser.CodSerie.ToString();
                           txtNumeroG.Visible = true;
                           txtNumeroG.Enabled = true;
                           txtNumeroG.Text = "";
                       }
                       else
                       {
                           CodSerieG = ser.CodSerie;
                           txtSerieG.Text = ser.Serie;
                           txtcodserie.Text = ser.CodSerie.ToString();
                           txtNumeroG.Visible = false;
                       }

                   }
                   if (CodSerieG != 0) { ProcessTabKey(true); }
               }
           }
       }

       public Boolean VerificarDetracciones()
       {
          /* Boolean grav = false;
           Decimal sumadet = 0;
           if (CodDocumento == 2)
           {
               if (dgvDetalle.Rows.Count > 0)
               {
                   foreach (DataGridViewRow row in dgvDetalle.Rows)
                   {
                       if (Convert.ToDecimal(row.Cells[igv.Name].Value) != 0)
                       {
                           grav = true;
                       }
                       else
                       {
                           if (Convert.ToDecimal(row.Cells[precioventa.Name].Value) == 0)
                           {
                               grav = true;
                           }
                           else
                           {
                               sumadet = sumadet + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                           }
                       }
                   }
               }
               if (sumadet <= 700)
               {
                   return true;
               }
               else
               {
                   if (grav)
                   {
                       MessageBox.Show("Operacion no permitida, por estar afecta a detracción!", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       return false;
                   }
                   else
                   {
                       return true;
                   }
               }
           }
           else
           {
               return true;
           }*/
           return true;
       }

       protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
       {

           if (keyData == (Keys.Control | Keys.G))
           {
               btnGuardar.PerformClick();
               return true;
           }
           else
           {
               return false;
           }
       }

       private void customValidator1_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           if (Proceso != 0 && e.ControlToValidate.Visible)
               if (e.ControlToValidate.Text != "")
                   e.IsValid = true;
               else
                   e.IsValid = false;
           else
               e.IsValid = true;
       }

       private void customValidator2_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           if (Proceso != 0 && e.ControlToValidate.Visible)
               if (e.ControlToValidate.Text != "")
                   e.IsValid = true;
               else
                   e.IsValid = false;
           else
               e.IsValid = true;
       }

       private void customValidator3_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           ComboBox c = (ComboBox)e.ControlToValidate;
           if (txtRazonSocialTransporte.Text == "")
               if (c.Enabled)
                   if (Proceso != 0 && c.Visible)
                       if (c.SelectedIndex != -1)
                           e.IsValid = true;
                       else
                           e.IsValid = false;
                   else
                       e.IsValid = true;
               else
                   e.IsValid = true;
           else e.IsValid = true;
       }

       private void customValidator4_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           ComboBox c = (ComboBox)e.ControlToValidate;
           if (txtRazonSocialTransporte.Text == "")
               if (c.Enabled)
                   if (Proceso != 0 && c.Visible)
                       if (c.SelectedIndex != -1)
                           e.IsValid = true;
                       else
                           e.IsValid = false;
                   else
                       e.IsValid = true;
               else
                   e.IsValid = true;
           else e.IsValid = true;
       }

       private void customValidator5_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           if (Proceso != 0)
               if (dgvDetalle.Rows.Count > 0)
                   e.IsValid = true;
               else
                   e.IsValid = false;
           else
               e.IsValid = true;
       }

       private void customValidator6_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           if (Proceso != 0)
               if (e.ControlToValidate.Text != "")
                   e.IsValid = true;
               else
                   e.IsValid = false;
           else
               e.IsValid = true;
       }

       private void customValidator7_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           ComboBox c = (ComboBox)e.ControlToValidate;
           if (c.Enabled)
               if (Proceso != 0)
                   if (c.SelectedIndex != -1)
                       e.IsValid = true;
                   else
                       e.IsValid = false;
               else
                   e.IsValid = true;
           else
               e.IsValid = true;
       }

       private void customValidator8_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           ComboBox c = (ComboBox)e.ControlToValidate;
           if (c.Enabled)
               if (Proceso != 0)
                   if (c.SelectedIndex != -1)
                       e.IsValid = true;
                   else
                       e.IsValid = true;
               else
                   e.IsValid = true;
           else
               e.IsValid = true;
       }

       private void customValidator9_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           if (Proceso != 0 && e.ControlToValidate.Visible)
               if (e.ControlToValidate.Text != "")
                   e.IsValid = true;
               else
                   e.IsValid = false;
           else
               e.IsValid = true;
       }

       private void customValidator10_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           if (Proceso != 0 && e.ControlToValidate.Visible)
               if (e.ControlToValidate.Text != "")
                   e.IsValid = true;
               else
                   e.IsValid = false;
           else
               e.IsValid = true;
       }

       private void customValidator11_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
       {
           if (Proceso != 0 && e.ControlToValidate.Visible)
               if (e.ControlToValidate.Text != "")
                   e.IsValid = true;
               else
                   e.IsValid = false;
           else
               e.IsValid = true;
       }

       private void txtNumero_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Enter)
           {
               cmbFormaPago.Focus();
           }
       }

       private void cmbFormaPago_KeyDown(object sender, KeyEventArgs e)
       {
       }

       private void cbListaPrecios_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Enter)
           {
               cbovendedor.Focus();
           }
       }

       private void cbovendedor_SelectionChangeCommitted(object sender, EventArgs e)
       {
           dtpFecha.Focus();
       }

       private void cbovendedor_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Enter)
           {
               dtpFecha.Focus();
           }
       }

       private void dtpFecha_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Enter)
           {
               cmbMoneda.Focus();
           }
       }

       private void cmbMoneda_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Enter)
           {
               txtComentario.Focus();
           }
       }

       private void txtComentario_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Enter)
           {
               cmbAlmacen.Focus();
           }
       }

       private void cmbAlmacen_KeyDown(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.Enter)
           {
               btnNuevo.Focus();
           }
       }

       private void checkBox1_CheckedChanged(object sender, EventArgs e)
       {
           if (checkBox1.Checked == true)
           {
               checkBox1.Text = "ConsultorExterno";

           }
           else
           {
               checkBox1.Text = "Venta Normal";
           }
       }

        private void cbTimpuesto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtcodpedido_TextChanged(object sender, EventArgs e)
        {

        }

        public void txtcodpedido_KeyPress(object sender, KeyPressEventArgs e)
       {
           if (e.KeyChar == (char)Keys.Return)
           {
               if (txtcodpedido.Text != "")
               {
                   /*if (BuscaPedido())
                   {
                       if (pedido.Pendiente == 1)
                       {
                           CargaPedido();
                           btnGuardar.Focus();
                       }
                       else
                       {
                           MessageBox.Show("Pedido ya esta facturado, ingresar datos correctamente!", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                           LimpiarPedido();
                       }
                   }
                   else
                   {
                       MessageBox.Show("Pedido no existe, ingresar datos correctamente!", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       // LimpiarPedido();
                   }*/


                   if (Procede == 4)
                   {
                       if (BuscaPedido())
                       {
                           if (pedido.Pendiente == 1)
                           {
                               CargaPedido();
                               btnGuardar.Focus();
                           }
                           else
                           {
                               MessageBox.Show("Pedido ya esta facturado, ingresar datos correctamente!", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                               LimpiarPedido();
                           }
                       }
                       else
                       {
                           MessageBox.Show("Pedido no existe, ingresar datos correctamente!", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                           // LimpiarPedido();
                       }
                   }
                   else if (Procede == 7)
                   {
                       if (BuscarSeparacion())
                       {
                           if (separacion.Pendiente == 0)
                           {
                               CargaSeparacion();
                               btnGuardar.Focus();
                           }
                           else
                           {
                               MessageBox.Show("Todavia no se Cancela!", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                               LimpiarPedido();
                           }
                       }
                       else
                       {
                           MessageBox.Show("no existe, ingresar datos correctamente!", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       }
                   }
               }
           }
       }

       clsAdmSeparacion Admsepa = new clsAdmSeparacion();

       private bool BuscarSeparacion()
       {
           separacion = Admsepa.BuscarSeparacionXid(Convert.ToInt32(txtcodpedido.Text), frmLogin.iCodAlmacen);
           if (separacion != null)
           {
               CodSeparacion = Convert.ToInt32(separacion.CodSeparacion);
               return true;
           }
           else
           {
               CodSeparacion = 0;
               return false;
           }
       }
       
       private bool BuscaPedido()
       {
            pedido = Admped.BuscaPedido(txtcodpedido.Text, frmLogin.iCodAlmacen);
            
            if (pedido != null)
            {
                CodPedido = Convert.ToInt32(pedido.CodPedido);
                return true;
            }
            else
            {
                CodPedido = 0;
                return false;
            }        
       }

       private void CargaPedido()
       {
           try
           {
               pedido = Admped.CargaPedido(Convert.ToInt32(CodPedido));
               if (pedido != null)
               {
                   //txtcodpedido.Text = Convert.ToInt32(pedido.CodPedido).ToString();
                   if (txtCodCliente.Enabled)
                   {
                       CodCliente = pedido.CodCliente;
                       if (CodCliente > 0) 
                       {
                           CargaCliente();
                       }
                       /*txtCodCliente.Text = pedido.CodigoPersonalizado;
                       if (pedido.CodTipoDocumento == 1)
                       {
                           txtNombreCliente.Text = pedido.Nombre;
                       }
                       else
                       {
                           txtCodCliente.Text = pedido.RUCCliente;                           
                           txtNombreCliente.Text = pedido.RazonSocialCliente;
                       }
                       txtDireccionCliente.Text = pedido.Direccion;
                       btnGuardar.Enabled = true;*/
                   }
                   dtpFecha.Value = pedido.FechaPedido;
                   //CargaMoneda();
                   cmbMoneda.SelectedValue = pedido.Moneda;
                   txtCodigoCli.Text = pedido.CodCliente.ToString();
                   txtTipoCambio.Text = pedido.TipoCambio.ToString();

                   if (txtAutorizacion.Enabled)
                   {
                       txtAutorizacion.Text = pedido.CodAutorizado.ToString();
                       lbAutorizado.Visible = true;
                       lbAutorizado.Text = pedido.NombreAutorizado.ToString();
                   }
                   if (txtDocRef.Enabled)
                   {
                       txtDocRef.Focus();                       
                   }
                   txtComentario.Text = pedido.Comentario;
                   txtBruto.Text = String.Format("{0:#,##0.00}", pedido.MontoBruto);
                   txtDscto.Text = String.Format("{0:#,##0.00}", pedido.MontoDscto);
                   txtValorVenta.Text = String.Format("{0:#,##0.00}", pedido.Total - pedido.Igv);
                   txtIGV.Text = String.Format("{0:#,##0.00}", pedido.Igv);
                   txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pedido.Total);
                   CargaDetallePedido();
                   btnGuardar.Focus();
               }
               else
               {
                   MessageBox.Show("El documento solicitado no existe", "Nota de Ingreso", MessageBoxButtons.OK, MessageBoxIcon.Information);
               }
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }

       private void CargaSeparacion()
       {
           try
           {
               separacion = Admsepa.BuscarSeparacion(CodSeparacion, frmLogin.iCodAlmacen);
               if (separacion != null)
               {
                   if (txtCodCliente.Enabled)
                   {
                       CodCliente = separacion.CodCliente;
                       if (CodCliente > 0)
                       {
                           CargaCliente();
                       }

                       //dtpFecha.Value = Convert.ToDateTime(separacion.FechaPedido);
                       cmbMoneda.SelectedValue = separacion.Moneda;
                       txtCodigoCli.Text = separacion.CodCliente.ToString();
                       txtTipoCambio.Text = separacion.TipoCambio.ToString();

                       txtComentario.Text = separacion.Comentario;
                       txtBruto.Text = String.Format("{0:#,##0.00}", separacion.Bruto);
                       txtDscto.Text = String.Format("{0:#,##0.00}", separacion.MontoDescuento);
                       txtValorVenta.Text = String.Format("{0:#,##0.00}", separacion.Total - separacion.Igv);
                       txtIGV.Text = String.Format("{0:#,##0.00}", separacion.Igv);
                       txtPrecioVenta.Text = String.Format("{0:#,##0.00}", separacion.Total);
                       CargaDetalleSeparacion();
                       btnGuardar.Focus();
                   }
               }
           }
           catch (Exception ex)
           {
           }
       }

       private void CargaDetalleSeparacion()
       {
           try
           {
               DataTable newDataDetalle = new DataTable();
               dgvDetalle.Rows.Clear();
               newDataDetalle = Admsepa.CargaDetalle(Convert.ToInt32(separacion.CodSeparacion));
               foreach (DataRow row in newDataDetalle.Rows)
               {
                   dgvDetalle.Rows.Add(row[0].ToString(), "0", row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(),
                       row[5].ToString(), row[6].ToString(), row[7].ToString(), row[8].ToString(), row[9].ToString(),
                       row[10].ToString(), row[11].ToString(), row[12].ToString(), row[13].ToString(), row[14].ToString(),
                       row[15].ToString(), row[16].ToString(), row[18].ToString(), row[17].ToString(),
                       "0", "0", row[22].ToString(), DateTime.Now);
               }
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }


       private void CargaDetallePedido()
       {
           //DgvDetalle.DataSource = Admped.CargaDetalle(Convert.ToInt32(pedido.CodPedido));
           Decimal totalVventa=0;
           Decimal totaligv=0;
           Decimal totalPventa=0;
           Decimal totaldesc=0;
           Decimal totalbruto = 0;
           try
           {
               DataTable newDataDetalle = new DataTable();
               dgvDetalle.Rows.Clear();
               
               if (Proceso == 4)
               {
                   newDataDetalle = Admped.CargaDetalle(Convert.ToInt32(CodPedido));
               }
               else { newDataDetalle = Admped.CargaDetalle(Convert.ToInt32(pedido.CodPedido)); }
               
               foreach (DataRow row in newDataDetalle.Rows)
               {
                   dgvDetalle.Rows.Add(row[0].ToString(), "0", row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(),
                       row[5].ToString(), row[6].ToString(), row[7].ToString(), row[8].ToString(), row[9].ToString(),
                       row[10].ToString(), row[11].ToString(), row[12].ToString(), row[13].ToString(), row[14].ToString(),
                       row[15].ToString(), row[16].ToString(), row[18].ToString(), row[17].ToString(),
                       "0", "0", row[22].ToString(), DateTime.Now);
                   totalVventa = totalVventa + Convert.ToDecimal(row[14].ToString());
                   totaligv = totaligv + Convert.ToDecimal(row[15].ToString());
                   totalPventa = totalPventa + Convert.ToDecimal(row[16].ToString());
                   totaldesc = totaldesc + Convert.ToDecimal(row[11].ToString());
                   totalbruto = totalbruto + Convert.ToDecimal(row[14].ToString());

              }

               txtValorVenta.Text = String.Format("{0:#,##0.00}",totalVventa.ToString());
               txtIGV.Text = String.Format("{0:#,##0.00}",totaligv.ToString());
               txtPrecioVenta.Text = String.Format("{0:#,##0.00}",totalPventa.ToString());
               txtDscto.Text = String.Format("{0:#,##0.00}",totaldesc.ToString());
               txtBruto.Text = String.Format("{0:#,##0.00}",totalbruto.ToString());

           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }


       public void LimpiarPedido()
       {
           if (dgvDetalle.RowCount > 0)
           {
               DataTable dt1 = (DataTable)dgvDetalle.DataSource;
               dt1.Clear();
           }
           txtCodCliente.Text = "";
           CodCliente = 0;
           CodPedido = 0;
           txtNombreCliente.Text = "";
           txtTransaccion.Text = "";
           txtDocRef.Text = "";
           txtSerie.Text = "";
           txtNumero.Text = "";
           txtcodpedido.Text = "";
           txtcodpedido.Focus();
           if (cmbFormaPago.Items.Count > 0) cmbFormaPago.SelectedIndex = 0;
       }
    }
}
