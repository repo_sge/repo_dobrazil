using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Printing;

namespace SIGEFA.Formularios
{
    public partial class frmCodigoBarras : DevComponents.DotNetBar.OfficeForm
    {
        public String Producto = "";
        Image barcode = null;

        public frmCodigoBarras()
        {
            InitializeComponent();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                BarcodeLib.Barcode b = new BarcodeLib.Barcode();

                //BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                b.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                //BarcodeLib.TYPE type = BarcodeLib.TYPE.UPCA;
                b.BarWidth = 1;
                b.AspectRatio = 3;
                b.IncludeLabel = true;
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
                panelResultado.BackgroundImage = b.Encode(BarcodeLib.TYPE.CODE128, txtCodigoBarra.Text, Color.Black, Color.White, b.Width, b.Height);
                panelResultado.Location = new Point((this.panelResultado.Location.X + this.panelResultado.Width / 2) - panelResultado.Width / 2, (this.panelResultado.Location.Y + this.panelResultado.Height / 2) - panelResultado.Height / 2);


                barcode = panelResultado.BackgroundImage;
                ////BarcodePDF417 codigobarras = new BarcodePDF417();
                ////codigobarras.Options = BarcodePDF417.PDF417_USE_ASPECT_RATIO;
                ////codigobarras.ErrorLevel = 5;
                ////codigobarras.YHeight = 6f;
                ////codigobarras.SetText(txtCodigoBarra.Text);
                ////System.Drawing.Bitmap bm = new System.Drawing.Bitmap(codigobarras.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
                ////panelResultado.BackgroundImage = bm;

                btnGuardar.Enabled = true;
            }
            catch (Exception ex)
            {
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Image imgFinal = (Image)panelResultado.BackgroundImage.Clone();
            SaveFileDialog CajaDeDialogoGuardar = new SaveFileDialog();
            CajaDeDialogoGuardar.AddExtension = true;
            CajaDeDialogoGuardar.Filter = "Image PNG (*.png)|*.png";
            CajaDeDialogoGuardar.ShowDialog();
            if (!string.IsNullOrEmpty(CajaDeDialogoGuardar.FileName))
            {
                imgFinal.Save(CajaDeDialogoGuardar.FileName, ImageFormat.Png);
            }
            imgFinal.Dispose();
        }

        private void frmCodigoBarras_Load(object sender, EventArgs e)
        {
            txtCodigoBarra.Text = Producto;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrintDocument pd = new PrintDocument();
           
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            // Set the printer name. 
            //pd.PrinterSettings.PrinterName = "\\NS5\\hpoffice";
            pd.PrinterSettings.PrinterName = "ZDesigner GC420t (EPL)";
            //pd.PrinterSettings.PrinterName = "EPSON TM-T20II Receipt en SGE-ANDRE";
            pd.Print();
        }

        void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {

            //Font printFont = new Font("3 of 9 Barcode", 17);
            Font printFont1 = new Font("Times New Roman", 9, FontStyle.Bold);


            SolidBrush br = new SolidBrush(Color.Black);

            ev.Graphics.DrawString("DO BRAZIL", printFont1, br, 50, 15);
            ev.Graphics.DrawImage(barcode, 40, 35);

            ev.Graphics.DrawString("DO BRAZIL", printFont1, br, 270, 15);
            ev.Graphics.DrawImage(barcode, 260, 35);

            //ev.Graphics.DrawString(txtCodigoBarra.Text, printFont1, br, 10, 85);
        }

    }
}