﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.Reportes;

namespace SIGEFA.Formularios
{

    public partial class frmVentas : DevComponents.DotNetBar.Office2007Form
    {
        clsAdmNotaSalida AdmNotaS = new clsAdmNotaSalida();
        clsAdmAperturaCierre AdmCaja = new clsAdmAperturaCierre();
        clsNotaSalida nota = new clsNotaSalida();
        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();
        clsFacturaVenta venta = new clsFacturaVenta();
        clsPago pag = new clsPago();
        clsCaja Caja = new clsCaja();
        clsAdmPago admPago = new clsAdmPago();
        clsAdmRepositorio admrepo = new clsAdmRepositorio();

        public Int32 Proceso = 0; //(1)Eliminar (2)Editar (3)Consulta

        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;        



        public frmVentas()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CargaLista()
        {
            dgvVentas.DataSource = data;
            data.DataSource = AdmVenta.Ventas(frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value);
            data.Filter = String.Empty;
            filtro = String.Empty;
            dgvVentas.ClearSelection();
        }

        private void btnIrPedido_Click(object sender, EventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow != null)
            {
                DataGridViewRow row = dgvVentas.CurrentRow;
                if (dgvVentas.Rows.Count >= 1)
                {
                    frmVenta form = new frmVenta();
                    form.MdiParent = this.MdiParent;
                    form.CodVenta = venta.CodFacturaVenta;
                    form.ventaok = 1;
                    form.Proceso = 3; 
                    form.Show();
                }
            }
        }

        private void frmPedidosPendientes_Load(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void dgvPedidosPendientes_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && e.Row.Selected)
            {
                venta.CodFacturaVenta= e.Row.Cells[codigo.Name].Value.ToString();               
            }
        }

        private void dgvPedidosPendientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {            
            if (dgvVentas.Rows.Count >= 1 && e.RowIndex != -1)
            {
                frmVenta form = new frmVenta();
                form.MdiParent = this.MdiParent;
                form.CodVenta = venta.CodFacturaVenta;
                form.Proceso = 3;
                form.Show();
            }
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            if (dgvVentas.SelectedRows.Count > 0 && dgvVentas.CurrentRow.Index != -1)
            {
                DataGridViewRow row = dgvVentas.CurrentRow;
                DialogResult dlgResult = MessageBox.Show("Esta seguro que desea anular el documento seleccionado", "Notas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    if (dgvVentas.Rows.Count >= 1 && Convert.ToInt32(dgvVentas.CurrentRow.Cells[codigo.Name].Value) > 0)
                    {
                        venta = AdmVenta.CargaFacturaVenta(Convert.ToInt32(row.Cells[codigo.Name].Value));
                        int enviado = admrepo.ValidarEnvio(Convert.ToInt32(row.Cells[codigo.Name].Value));

                        if (!AdmVenta.ValidaAnulacionVenta(Convert.ToInt32(dgvVentas.CurrentRow.Cells[codigo.Name].Value)))
                        {
                            if (enviado == 0)
                            {
                                if (venta.FechaRegistro.Date == DateTime.Now.Date)
                                {
                                    if (AdmVenta.anular(Convert.ToInt32(venta.CodFacturaVenta)))
                                    {
                                        MessageBox.Show("El documento ha sido anulado correctamente", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                        DataTable dtPagos = admPago.GetPagosVenta(frmLogin.iCodAlmacen, Convert.ToInt32(dgvVentas.CurrentRow.Cells[codigo.Name].Value));

                                        foreach (DataRow fila in dtPagos.Rows)
                                        {
                                            admPago.AnularPago(Convert.ToInt32(fila[0]));
                                        }

                                        CargaLista();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Para anular esta venta distinta a la venta actual, primero tienes que enviarla a sunat para poder generar una NC", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                abrirNotaCredito();
                            }

                        }
                        else
                        {
                            MessageBox.Show("La venta ya se encuentra anulada", "VENTAS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Seleccione una venta...", "VENTAS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                CargaLista();
            }
        }

        public void abrirNotaCredito()
        {
            if (Application.OpenForms["frmNotadeCredito"] != null)
            {
                Application.OpenForms["frmNotadeCredito"].Activate();
            }
            else
            {
                frmNotadeCredito form = new frmNotadeCredito();
                form.MdiParent = this.MdiParent;
                form.Proceso = 7;
                form.CodNotaS = Convert.ToInt32(dgvVentas.CurrentRow.Cells[codigo.Name].Value);
                form.Show();
            }
        }

        private void dgvVentas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && e.RowIndex != -1)
            {
                btnIrPedido.Enabled = true;
                if (dgvVentas.Rows[e.RowIndex].Cells[estado.Name].Value.ToString() == "ACTIVO")
                {
                    btnAnular.Text = "Anular";
                    btnAnular.Enabled = true;
                    btnAnular.ImageIndex = 4;
                }
                else
                {
                    btnAnular.Text = "Activar";
                    btnAnular.Enabled = true;
                    btnAnular.ImageIndex = 6;
                }
            } 
        }

        private void dtpDesde_ValueChanged(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void dtpHasta_ValueChanged(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("ListaGuias");
            // Columnas
            foreach (DataGridViewColumn column in dgvVentas.Columns)
            {
                DataColumn dc = new DataColumn(column.Name.ToString());
                dt.Columns.Add(dc);
            }
            // Datos
            for (int i = 0; i < dgvVentas.Rows.Count; i++)
            {
                DataGridViewRow row = dgvVentas.Rows[i];
                DataRow dr = dt.NewRow();
                for (int j = 0; j < dgvVentas.Columns.Count; j++)
                {
                    dr[j] = (row.Cells[j].Value == null) ? "" : row.Cells[j].Value.ToString();
                }
                dt.Rows.Add(dr);
            }

            ds.Tables.Add(dt);
            ds.WriteXml("C:\\XML\\ListaVentasRPT.xml", XmlWriteMode.WriteSchema);


            CRListaVentas rpt = new CRListaVentas();
            frmListaVentas frm = new frmListaVentas();
            rpt.SetDataSource(ds);
            frm.crvListaGuias.ReportSource = rpt;
            frm.Show();
        }

        private void dgvVentas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void btnVistaSucursales_Click(object sender, EventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow != null)
            {
                if (btnVistaSucursales.Text == "Activar Vista")
                {
                    if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow.Index != -1)
                    {
                        DialogResult dlgResult = MessageBox.Show("¿Esta seguro que desea activar la vista de este documento en otras sucursales?", "Notas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dlgResult == DialogResult.No)
                        {
                            return;
                        }
                        else
                        {
                            if (AdmVenta.VistaSucursal(Convert.ToInt32(venta.CodFacturaVenta), 1))
                            {
                                MessageBox.Show("El documento puede ser visualizado desde cualquier sucursal correctamente", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CargaLista();
                            }
                        }
                    }
                }
                //else if (btnVistaSucursales.Text == "Desactivar Vista")
                //{
                //    if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow.Index != -1)
                //    {
                //        DialogResult dlgResult = MessageBox.Show("¿Esta seguro que desea desactivar la vista de este documento en otras sucursales?", "Notas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //        if (dlgResult == DialogResult.No)
                //        {
                //            return;
                //        }
                //        else
                //        {
                //            if (AdmVenta.VistaSucursal(Convert.ToInt32(venta.CodFacturaVenta), 2))
                //            {
                //                MessageBox.Show("El documento puede ser visualizado desde cualquier sucursal correctamente", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //                CargaLista();
                //            }
                //        }
                //    }
                //}
            }
        }
    }
}
