using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmNotaIngresoAr : DevComponents.DotNetBar.OfficeForm
    {
        public Int32 Proceso = 0;
        public Int32 CodProveedor;
        public Int32 CodCliente;
        public Int32 CodDocumento;
        public Int32 CodTransaccion;
        public Int32 codFac;

        clsAdmMoneda admMon = new clsAdmMoneda();
        clsAdmFormaPago admForm = new clsAdmFormaPago();
        clsAdmTipoCambio admTipo = new clsAdmTipoCambio();
        clsAdmProveedor admProv = new clsAdmProveedor();
        clsAdmCliente admCli = new clsAdmCliente();
        clsAdmTipoDocumento admDoc = new clsAdmTipoDocumento();
        clsAdmFormaPago admpago = new clsAdmFormaPago();
        clsAdmNotaIngreso admNota = new clsAdmNotaIngreso();
        clsAdmFactura admFact = new clsAdmFactura();
        clsAdmTransaccion AdmTran = new clsAdmTransaccion();
        clsAdmNotaIngreso AdmNota = new clsAdmNotaIngreso();
        clsValidar ok = new clsValidar();

        clsDetalleNotaIngreso detaSelec = new clsDetalleNotaIngreso();
        clsTipoDocumento doc = new clsTipoDocumento();
        clsTipoCambio clsTipo = new clsTipoCambio();
        clsProveedor prov = new clsProveedor();
        clsCliente cli = new clsCliente();
        clsFormaPago pago = new clsFormaPago();
        clsNotaIngreso nota = new clsNotaIngreso();
        clsFactura fac = new clsFactura();
        clsTransaccion tran = new clsTransaccion();

        private Int32 proce = 0; //(1) Nota IngresoxCompra.
        public List<clsDetalleNotaIngreso> detalle = new List<clsDetalleNotaIngreso>();
        public List<clsDetalleFactura> detalleFactura = new List<clsDetalleFactura>();
        public List<Int32> codProd = new List<int>();
        public clsTalla talla;
        
        public frmNotaIngresoAr()
        {
            InitializeComponent();
        }

        private void frmNotaIngresoAr_Load(object sender, EventArgs e)
        {
            CargaMoneda();
            CargaFormaPagos();
            clsTipo = admTipo.CargaTipoCambio(dtpFecha.Value.Date, 2);
            txtTipoCambio.Text = clsTipo.Venta.ToString();
            if (Proceso == 1)
            {
                BloqueaBotones();
            }
            if (Proceso == 3)
            {
                CargaFactura();
            }
        }

        private void CargaFactura()
        {
            try
            {
                nota = AdmNota.CargaNotaIngreso(Convert.ToInt32(codFac));
                if (nota != null)
                {
                    txtNumDoc.Text = nota.CodNotaIngreso;
                    CodTransaccion = nota.CodTipoTransaccion;
                    CargaTransaccion();
                    if (txtCodProv.Enabled)
                    {
                        CodProveedor = nota.CodProveedor;
                        txtCodProv.Text = nota.RUCProveedor;
                        txtNombreProv.Text = nota.RazonSocialProveedor;
                        BuscaProveedor();
                    }
                    dtpFecha.Value = nota.FechaIngreso;
                    
                    cmbMoneda.SelectedValue = nota.Moneda;
                    txtTipoCambio.Text = nota.TipoCambio.ToString();
                    txtTipoCambio.Visible = true;
                    label16.Visible = true;
                    if (txtAutorizacion.Enabled)
                    {
                        //se guarda el codigo del autorizado y se cargan los datos de este
                    }
                    if (txtDocRef.Enabled)
                    {
                        CodDocumento = nota.CodTipoDocumento;
                        txtDocRef.Text = nota.SiglaDocumento;
                        txtNDocRef.Text = nota.NumDoc;
                        BuscaTipoDocumento();
                        //doc = Admdoc.BuscaTipoDocumento(txtDocRef.Text);
                        //if (doc != null)
                        //{
                        //    CodDocumento = doc.CodTipoDocumento;
                        //}
                    }
                    if (txtOrdenCompra.Enabled)
                    {
                        //se carga el codigo de la orden de compra
                        //txtOrdenCompra.Text = 
                    }
                    cmbFormaPago.SelectedValue = nota.FormaPago;
                    dtpFechaPago.Value = nota.FechaPago;
                    txtComentario.Text = nota.Comentario;
                    txtBruto.Text = String.Format("{0:#,##0.0000}", nota.MontoBruto);
                    txtDscto.Text = String.Format("{0:#,##0.0000}", nota.MontoDscto);
                    txtFlete.Text = String.Format("{0:#,##0.0000}", nota.Flete);
                    txtValorVenta.Text = String.Format("{0:#,##0.0000}", nota.Total - nota.Igv);
                    txtIGV.Text = String.Format("{0:#,##0.0000}", nota.Igv);
                    txtPrecioVenta.Text = String.Format("{0:#,##0.0000}", nota.Total);
                    CargarDetalle();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void CargarDetalle()
        {
            dgvDetalle.DataSource = admNota.CargaDetalle(Convert.ToInt32(nota.CodNotaIngreso));
            recorreDetalle();
            nota.Detalle = detalle;
        }

        private void CargaFormaPagos()
        {
            cmbFormaPago.DataSource = admForm.CargaFormaPagos(0);
            cmbFormaPago.DisplayMember = "descripcion";
            cmbFormaPago.ValueMember = "codFormaPago";
            cmbFormaPago.SelectedIndex = -1;
        }

        private void CargaMoneda()
        {
            cmbMoneda.DataSource = admMon.ListaMonedas();
            cmbMoneda.DisplayMember = "descripcion";
            cmbMoneda.ValueMember = "codMoneda";
            cmbMoneda.SelectedIndex = -1;
        }

        private void BloqueaBotones()
        {
            btnNuevo.Visible = false;
            
        }

        private void txtCodProv_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.F1)
                {
                    if (Application.OpenForms["frmProveedoresLista"] != null)
                    {
                        Application.OpenForms["frmProveedoresLista"].Activate();
                    }
                    frmProveedoresLista form = new frmProveedoresLista();
                    form.Proceso = 3;
                    form.Procede = 4;
                    form.ShowDialog();
                    if (CodProveedor != 0) { CargaProveedor(); ProcessTabKey(true); } else { BorrarProveedor(); }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void BorrarProveedor()
        {
            throw new NotImplementedException();
        }

        private void CargaProveedor()
        {
            prov = admProv.MuestraProveedor(CodProveedor);
            txtCodProv.Text = prov.Ruc;
            txtNombreProv.Text = prov.RazonSocial;
            txtCodProveedor.Text = prov.CodProveedor.ToString();
        }

        private void txtCodProv_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if(txtCodProv.Text == "")
                {
                    if(BuscaProveedor())
                    {
                        ProcessTabKey(true);
                    }else
                    {
                        MessageBox.Show("El proveedor no existe, Presione F1 para consultar la tabla de ayuda", "NOTA DE INGRESO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private bool BuscaProveedor()
        {
            prov = admProv.BuscaProveedor(txtCodProv.Text);
            if (prov != null)
            {
                txtNombreProv.Text = prov.RazonSocial;
                CodProveedor = prov.CodProveedor;
                return true;
            }
            else
            {
                txtNombreProv.Text = "";
                CodProveedor = 0;
                return false;
            } 
        }

        private void txtCodProv_Leave(object sender, EventArgs e)
        {
            if (CodProveedor == 0)
            {
                txtCodProv.Focus();
            }
            else
            {

            }
        }

        private void txtCodCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmClientesLista"] != null)
                {
                    Application.OpenForms["frmClientesLista"].Activate();
                }
                frmClientesLista form = new frmClientesLista();
                form.Proceso = 3;
                cli = form.cli;
                CodCliente = cli.CodCliente;
                if (CodCliente != 0) { CargaCliente(); ProcessTabKey(true); }
                form.Show();
            }
        }

        private void CargaCliente()
        {
            cli = admCli.MuestraCliente(CodCliente);
            if (cli != null)
            {
                txtCodCliente.Text = cli.CodigoPersonalizado;
                txtNombreCliente.Text = cli.RazonSocial;
            }
        }

        private void txtCodCliente_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtDocRef_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmDocumentos"] != null)
                {
                    Application.OpenForms["frmDocumentos"].Close();
                }
                else
                {
                    frmDocumentos form = new frmDocumentos();
                    form.Proceso = 3;
                    form.Procedencia = 1;
                    form.Transaccion = txtTransaccion.Text;
                    form.ShowDialog();
                    doc = form.doc;
                    CodDocumento = doc.CodTipoDocumento;
                    txtDocRef.Text = doc.Sigla;
                    if (CodDocumento != 0) { ProcessTabKey(true); } else { txtDocRef.Text = ""; }
                }
            } 
        }

        private void txtDocRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtDocRef.Text != "")
                {
                    if (BuscaTipoDocumento())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de Documento no existe, Presione F1 para consultar la tabla de ayuda", "NOTA DE INGRESO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private Boolean BuscaTipoDocumento()
        {
            doc = admDoc.BuscaTipoDocumento(txtDocRef.Text);
            if (doc != null)
            {
                CodDocumento = doc.CodTipoDocumento;
                return true;
            }
            else
            {
                CodDocumento = 0;
                return false;
            }
        }

        private void cmbFormaPago_SelectionChangeCommitted(object sender, EventArgs e)
        {
            pago = admpago.CargaFormaPago(Convert.ToInt32(cmbFormaPago.SelectedValue));
            dtpFechaPago.Value = dtpFecha.Value.AddDays(pago.Dias);
                
        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            clsTipo = admTipo.CargaTipoCambio(dtpFecha.Value.Date, 2);
            if (clsTipo != null)
            {
                txtTipoCambio.Text = clsTipo.Venta.ToString();
                dtpFechaPago.Value = dtpFecha.Value.AddDays(pago.Dias);
            }
            else
            {
                MessageBox.Show("No existe tipo de cambio registrado en esta fecha", "Tipo de Cambio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtpFecha.Value = DateTime.Now.Date;
                dtpFecha.Focus();
            }
        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {
            recorreDetalle();
            if (txtCodProv.Text != "")
            {
                if (Application.OpenForms["frmDetalleIngreso"] != null)
                {
                    Application.OpenForms["frmDetalleIngreso"].Activate();
                }
                frmDetalleIngreso form = new frmDetalleIngreso();
                //form.MdiParent = this;
                form.Procede = 6;
                form.Proceso = 1;
                form.comprasalida = 1;
                form.codproveedor = Convert.ToInt32(txtCodProveedor.Text);
                form.bvalorventa = cbValorVenta.Checked;
                //form.productoscargados = detalle;
                
                form.ShowDialog();
                serielote.Visible = false;
            }
            else
            {
                MessageBox.Show("Ingrese Proveedor");
            }
            
        }

        private void recorreDetalle()
        {
            detalle.Clear();
            detalleFactura.Clear();
            if (dgvDetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    añadeDetalle(row);
                }
            }
        }

        private void añadeDetalle(DataGridViewRow fila)
        {
            clsDetalleNotaIngreso deta = new clsDetalleNotaIngreso();
            clsDetalleFactura detafac = new clsDetalleFactura();
            detafac.CodFactura = fac.CodFacturaNueva;
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            detafac.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodNotaIngreso = Convert.ToInt32(nota.CodNotaIngreso);
            detafac.CodNotaIngreso = nota.CodNotaIngreso;
            deta.CodAlmacen = frmLogin.iCodAlmacen;
            detafac.CodAlmacen = frmLogin.iCodAlmacen;
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            deta.SerieLote = fila.Cells[serielote.Name].Value.ToString();
            detafac.SerieLote = "0";
            deta.Cantidad = Convert.ToDecimal(fila.Cells[cantidad.Name].Value);
            detafac.Cantidad = Convert.ToDecimal(fila.Cells[cantidad.Name].Value);
            deta.PrecioUnitario = Convert.ToDecimal(fila.Cells[preciounit.Name].Value);
            detafac.PrecioUnitario = Convert.ToDecimal(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDecimal(fila.Cells[importe.Name].Value);
            detafac.Subtotal = Convert.ToDecimal(fila.Cells[importe.Name].Value);
            deta.Descuento1 = Convert.ToDecimal(fila.Cells[dscto1.Name].Value);
            detafac.Descuento1 = Convert.ToDecimal(fila.Cells[dscto1.Name].Value);
            deta.Descuento2 = Convert.ToDecimal(fila.Cells[dscto2.Name].Value);
            detafac.Descuento2 = Convert.ToDecimal(fila.Cells[dscto2.Name].Value);
            deta.Descuento3 = Convert.ToDecimal(fila.Cells[dscto3.Name].Value);
            detafac.Descuento3 = Convert.ToDecimal(fila.Cells[dscto3.Name].Value);
            deta.MontoDescuento = Convert.ToDecimal(fila.Cells[montodscto.Name].Value);
            detafac.MontoDescuento = Convert.ToDecimal(fila.Cells[montodscto.Name].Value);
            deta.Igv = Convert.ToDecimal(fila.Cells[igv.Name].Value);
            detafac.Igv = Convert.ToDecimal(fila.Cells[igv.Name].Value);
            deta.Flete = Convert.ToDecimal(fila.Cells[flete.Name].Value);
            detafac.Flete = Convert.ToDecimal(fila.Cells[flete.Name].Value);
            deta.Importe = Convert.ToDecimal(fila.Cells[precioventa.Name].Value);
            detafac.Importe = Convert.ToDecimal(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDecimal(fila.Cells[precioreal.Name].Value);
            detafac.PrecioReal = Convert.ToDecimal(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDecimal(fila.Cells[valoreal.Name].Value);
            detafac.ValoReal = Convert.ToDecimal(fila.Cells[valoreal.Name].Value);
            deta.FechaIngreso = dtpFecha.Value;
            detafac.FechaIngreso = dtpFecha.Value;
            deta.Bonificacion = false;
            
            deta.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
            detafac.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
            detafac.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            detafac.CodUser = frmLogin.iCodUser;
            if (txtCodProveedor.Text != "") detafac.CodProveedor = Convert.ToInt32(txtCodProveedor.Text); else detafac.CodProveedor = 0;
            detalle.Add(deta);
            detalleFactura.Add(detafac);
        }

        private void dgvDetalle_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDetalle.Columns[e.ColumnIndex].Name == "precioventa")
            {
                if (Proceso == 1)
                {
                    calculatotales();
                }
            }
        }

        private void calculatotales()
        {
            /*if (proce == 1)
            {
                Decimal bruto = 0;
                Decimal descuen = 0;
                Decimal valor = 0;
                Decimal igvt = 0;
                Decimal preciot = 0;
                foreach (DataGridViewRow row in dgvDetalle2.Rows)
                {
                    if (Convert.ToDecimal(row.Cells[cantn.Name].Value) != 0)
                    {
                        bruto = bruto + Convert.ToDecimal(row.Cells[subtotal.Name].Value);
                        valor = valor + Convert.ToDecimal(row.Cells[valorventa1.Name].Value);
                        igvt = igvt + Convert.ToDecimal(row.Cells[igv1.Name].Value);
                        preciot = preciot + Convert.ToDecimal(row.Cells[importe1.Name].Value);
                    }

                }
                txtBruto.Text = String.Format("{0:#,##0.0000}", bruto);
                txtValorVenta.Text = String.Format("{0:#,##0.0000}", valor);
                txtIGV.Text = String.Format("{0:#,##0.0000}", igvt);
                txtPrecioVenta.Text = String.Format("{0:#,##0.0000}", preciot);
            }
            else
            {*/
                Decimal bruto = 0;
                Decimal descuen = 0;
                Decimal valor = 0;
                Decimal igvt = 0;
                Decimal preciot = 0;
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    bruto = bruto + Convert.ToDecimal(row.Cells[importe.Name].Value);
                    descuen = descuen + Convert.ToDecimal(row.Cells[montodscto.Name].Value);
                    valor = valor + Convert.ToDecimal(row.Cells[valorventa.Name].Value);
                    igvt = igvt + Convert.ToDecimal(row.Cells[igv.Name].Value);
                    preciot = preciot + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                }
                txtBruto.Text = String.Format("{0:#,##0.0000}", bruto);
                txtDscto.Text = String.Format("{0:#,##0.0000}", descuen);
                txtValorVenta.Text = String.Format("{0:#,##0.0000}", valor);
                txtIGV.Text = String.Format("{0:#,##0.0000}", igvt);
                txtPrecioVenta.Text = String.Format("{0:#,##0.0000}", preciot);
           // }
        }

        private void dgvDetalle_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (Proceso == 1 || Proceso == 7)
            {
                calculatotales();
            }
        }

        private void dgvDetalle_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (dgvDetalle.Rows.Count >= 1 && e.Row.Selected)
            {
                detaSelec.CodProducto = Convert.ToInt32(e.Row.Cells[codproducto.Name].Value);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (superValidator1.Validate())
                {
                    if (Proceso == 1)
                    {
                        nota.CodAlmacen = frmLogin.iCodAlmacen;
                        fac.CodAlmacen = frmLogin.iCodAlmacen;
                        nota.CodTipoTransaccion = tran.CodTransaccion;
                        fac.CodTipoTransaccion = tran.CodTransaccion;
                        nota.CodProveedor = prov.CodProveedor;
                        fac.CodProveedor = prov.CodProveedor;
                        nota.CodTipoDocumento = doc.CodTipoDocumento;
                        fac.CodTipoDocumento = doc.CodTipoDocumento;
                        nota.NumDoc = txtNDocRef.Text;
                        fac.DocumentoFactura = doc.Sigla + "-" + txtNDocRef.Text;
                        nota.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                        fac.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                        if (txtTipoCambio.Visible)
                        {
                            fac.TipoCambio = Convert.ToDecimal(txtTipoCambio.Text);
                            fac.FechaCancelado = dtpFecha.Value.Date;
                        }
                        nota.TipoCambio = Convert.ToDecimal(txtTipoCambio.Text);
                        nota.Cancelado = 0;
                        nota.FechaIngreso = dtpFecha.Value.Date;
                        nota.FechaPago = dtpFechaPago.Value.Date;
                        fac.FechaIngreso = dtpFecha.Value.Date;
                        fac.FechaPago = dtpFechaPago.Value.Date;
                        fac.Cancelado = 0;
                        fac.Comentario = txtComentario.Text;
                        fac.MontoBruto = Convert.ToDecimal(txtBruto.Text);
                        fac.MontoDscto = Convert.ToDecimal(txtDscto.Text);
                        nota.Comentario = txtComentario.Text;
                        if (txtFlete.Text != "")
                        {
                            if (Convert.ToDecimal(txtFlete.Text) > 0)
                            {
                                prorrateodeflete();
                                recalculadetalle();
                                calculatotales();
                            }
                        }
                        nota.MontoBruto = Convert.ToDecimal(txtBruto.Text);
                        nota.MontoDscto = Convert.ToDecimal(txtDscto.Text);
                        if (txtFlete.Text != "")
                        {
                            nota.Flete = Convert.ToDecimal(txtFlete.Text);
                            fac.Flete = Convert.ToDecimal(txtFlete.Text);
                        }
                        fac.Igv = Convert.ToDecimal(txtIGV.Text);
                        fac.Total = Convert.ToDecimal(txtPrecioVenta.Text);
                        fac.CodUser = frmLogin.iCodUser;

                        nota.Igv = Convert.ToDecimal(txtIGV.Text);
                        nota.Total = Convert.ToDecimal(txtPrecioVenta.Text);
                        nota.CodUser = frmLogin.iCodUser;
                        nota.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
                        fac.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
                        nota.codalmacenemisor = 0;
                        nota.Estado = 1;                        
                        fac.Estado = 1;
                        if (dgvDetalle.Rows.Count > 0)
                        {
                            if (admNota.insert(nota) && admFact.insert(fac))
                            {
                                recorreDetalle();

                                if (detalle.Count > 0)
                                {
                                    foreach (clsDetalleNotaIngreso det in detalle)
                                    {
                                        admNota.insertdetalle(det);
                                    }
                                }

                                if (detalleFactura.Count > 0)
                                {
                                    foreach (clsDetalleFactura det in detalleFactura)
                                    {
                                        admFact.insertdetalle(det);
                                    }
                                }
                                
                                
                                MessageBox.Show("Los datos se guardaron correctamente", "Nota de Ingreso",
                                                   MessageBoxButtons.OK, MessageBoxIcon.Information);

                                btnGuardar.Enabled = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe llenar el detalle", "Nota de Ingreso",
                                                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }

                    if (Proceso == 3)
                    {
                        if (AdmNota.update(nota))
                        {
                            recorreDetalle();
                            foreach (clsDetalleNotaIngreso det in nota.Detalle)
                            {
                                foreach (clsDetalleNotaIngreso det1 in detalle)
                                {
                                    if (det.CodDetalleIngreso == det1.CodDetalleIngreso)
                                    {
                                        AdmNota.updatedetalle(det1);
                                    }
                                }
                                AdmNota.deletedetalle(det.CodDetalleIngreso);
                            }
                            foreach (clsDetalleNotaIngreso deta in detalle)
                            {
                                if (deta.CodDetalleIngreso == 0)
                                {
                                    AdmNota.insertdetalle(deta);
                                }
                            }
                            MessageBox.Show("Los datos se actualizaron correctamente", "Nota de Ingreso",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void prorrateodeflete()
        {
            if (txtFlete.Text != "" && dgvDetalle.Rows.Count >= 1)
            {
                Decimal precior = 0;
                Decimal percentr = 0;
                Decimal fleter = 0;
                Decimal totalr = 0;
                Decimal dflete = Convert.ToDecimal(txtFlete.Text);

                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    totalr = totalr + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                }
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    precior = Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                    percentr = precior / totalr;
                    fleter = dflete * percentr;
                    row.Cells[flete.Name].Value = String.Format("{0:#,##0.0000}", fleter);
                }
            }
        }

        private void recalculadetalle()
        {
            if (proce == 1)
            {
                Decimal vvflete = 0;
                Decimal pvflete = 0;
                foreach (DataGridViewRow row in dgvDetalle2.Rows)
                {
                    if (Convert.ToDecimal(row.Cells[cantn.Name].Value) != 0)
                    {
                        vvflete = Convert.ToDecimal(row.Cells[valorventa1.Name].Value) +
                                  Convert.ToDecimal(row.Cells[flete1.Name].Value);
                        pvflete = Convert.ToDecimal(row.Cells[importe1.Name].Value) +
                                  Convert.ToDecimal(row.Cells[flete1.Name].Value);
                        if (Convert.ToDecimal(row.Cells[flete1.Name].Value) > 0.00m &&
                            row.Cells[flete1.Name].Value.ToString() != "")
                        {
                            row.Cells[valoreal1.Name].Value = vvflete / Convert.ToDecimal(row.Cells[cantn.Name].Value);
                            row.Cells[precioreal1.Name].Value = pvflete / Convert.ToDecimal(row.Cells[cantn.Name].Value);
                        }
                        else
                        {
                            row.Cells[valoreal1.Name].Value = Convert.ToDecimal(row.Cells[valorventa1.Name].Value) /
                                                              Convert.ToDecimal(row.Cells[cantn.Name].Value);
                            row.Cells[precioreal1.Name].Value = Convert.ToDecimal(row.Cells[importe1.Name].Value) /
                                                                Convert.ToDecimal(row.Cells[cantn.Name].Value);
                        }
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    row.Cells[valorventaconflete.Name].Value = Convert.ToDecimal(row.Cells[valorventa.Name].Value) + Convert.ToDecimal(row.Cells[flete.Name].Value);
                    row.Cells[pvconflete.Name].Value = Convert.ToDecimal(row.Cells[precioventa.Name].Value) + Convert.ToDecimal(row.Cells[flete.Name].Value);
                    if (Convert.ToDecimal(row.Cells[flete.Name].Value) > 0.00m && row.Cells[flete.Name].Value.ToString() != "")
                    {
                        row.Cells[valoreal.Name].Value = Convert.ToDecimal(row.Cells[valorventaconflete.Name].Value) / Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                        row.Cells[precioreal.Name].Value = Convert.ToDecimal(row.Cells[pvconflete.Name].Value) / Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                    }
                    else
                    {
                        row.Cells[valoreal.Name].Value = Convert.ToDecimal(row.Cells[valorventa.Name].Value) / Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                        row.Cells[precioreal.Name].Value = Convert.ToDecimal(row.Cells[precioventa.Name].Value) / Convert.ToDecimal(row.Cells[cantidad.Name].Value);
                    }
                }
            }

        }

        private void txtTransaccion_KeyDown(object sender, KeyEventArgs e)
        {
            
                if (e.KeyCode == Keys.F1)
                {
                    if (Application.OpenForms["frmTransacciones"] != null)
                    {
                        Application.OpenForms["frmTransacciones"].Activate();
                    }
                    else
                    {
                        frmTransacciones form = new frmTransacciones();
                        form.Proceso = 3;
                        form.ShowDialog();
                        tran = form.tran;
                        CodTransaccion = tran.CodTransaccion;
                        txtTransaccion.Text = tran.Sigla;
                        if (CodTransaccion != 0) { CargaTransaccion(); ProcessTabKey(true); } else { BorrarTransaccion(); }
                    }
                }
               
            
        }

        private void BorrarTransaccion()
        {
            throw new NotImplementedException();
        }

        private void CargaTransaccion()
        {
            tran = AdmTran.MuestraTransaccion(CodTransaccion);
            tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
            txtTransaccion.Text = tran.Sigla;
            lbNombreTransaccion.Text = tran.Descripcion;
            lbNombreTransaccion.Visible = true;
            
        }

        public void txtTransaccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtTransaccion.Text != "")
                {
                    if (BuscaTransaccion())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de transacción no existe, Presione F1 para consultar la tabla de ayuda", "NOTA DE INGRESO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private bool BuscaTransaccion()
        {
            tran = AdmTran.MuestraTransaccionS(txtTransaccion.Text, 0);
            if (tran != null)
            {
                CodTransaccion = tran.CodTransaccion;
                tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
                txtTransaccion.Text = tran.Sigla;
                lbNombreTransaccion.Text = tran.Descripcion;
                lbNombreTransaccion.Visible = true;
                foreach (Control t in groupBox1.Controls)
                {
                    if (t.Tag != null)
                    {
                        Int32 con = Convert.ToInt32(t.Tag);
                        if (tran.Configuracion.Contains(con))
                        {                            
                            t.Visible = true;
                        }
                        else
                        {
                            t.Visible = false;
                        }
                    }
                }
                return true;
            }
            else
            {
                lbNombreTransaccion.Text = "";
                lbNombreTransaccion.Visible = false;
                foreach (Control t in groupBox1.Controls)
                {
                    if (t.Tag != null)
                    {
                        t.Visible = false;
                    }
                }
                return false;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvDetalle.Rows.Count > 0)
            {
                dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow);
                codProd.Remove(detaSelec.CodProducto);
            }

            recorreDetalle();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvDetalle.Rows.Count > 0 & dgvDetalle.SelectedRows.Count  > 0)
            {
                DataGridViewRow row = dgvDetalle.SelectedRows[0];
                if (Application.OpenForms["frmDetalleIngreso"] != null)
                {
                    Application.OpenForms["frmDetalleIngreso"].Activate();
                }
                else
                {
                    frmDetalleIngreso form = new frmDetalleIngreso();
                    form.Proceso = 2;
                    form.Procede = 6;
                    form.bvalorventa = cbValorVenta.Checked;
                    form.txtCodigo.Text = row.Cells[codproducto.Name].Value.ToString();
                    form.txtReferencia.Text = row.Cells[referencia.Name].Value.ToString();
                    form.txtReferencia.ReadOnly = true;
                    form.txtControlStock.Text = row.Cells[serielote.Name].Value.ToString();
                    form.txtCantidad.Text = row.Cells[cantidad.Name].Value.ToString();
                    form.txtPrecio.Text = row.Cells[preciounit.Name].Value.ToString();
                    form.txtDscto1.Text = row.Cells[dscto1.Name].Value.ToString();
                    form.txtDscto2.Text = row.Cells[dscto2.Name].Value.ToString();
                    form.txtDscto3.Text = row.Cells[dscto3.Name].Value.ToString();
                    form.txtPrecioNeto.Text = row.Cells[importe.Name].Value.ToString();
                    form.txtCantidad.Focus();
                    form.ShowDialog();
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbValorVenta_CheckedChanged(object sender, EventArgs e)
        {
            if (cbValorVenta.Checked)
            {
                
                if (dgvDetalle.Rows.Count > 0)
                {
                                        
                    foreach (DataGridViewRow row in dgvDetalle.Rows)
                    {//cuando al valor venta se le resta el igv
                        Decimal bruto = 0, valorvent = 0, preciovent = 0, nigv = 0, precioneto = 0, montodescuento = 0, factorigv = 0, ds1 = 0, ds2 = 0, ds3 = 0, preunitario = 0, preciosinigv = 0, precior = 0, varlorr = 0;
                        Int32 cant = 0, codpro = 0, coduni = 0;
                        String descrip = "", unida = "", refe = "";

                        coduni = Convert.ToInt32(row.Cells[codunidad.Name].Value);
                        codpro = Convert.ToInt32(row.Cells[codproducto.Name].Value);
                        descrip = row.Cells[descripcion.Name].Value.ToString();
                        unida = row.Cells[unidad.Name].Value.ToString();
                        ds1 = Convert.ToDecimal(row.Cells[dscto1.Name].Value);
                        ds2 = Convert.ToDecimal(row.Cells[dscto2.Name].Value);
                        ds3 = Convert.ToDecimal(row.Cells[dscto3.Name].Value);
                        refe = row.Cells[referencia.Name].Value.ToString();
                        cant = Convert.ToInt32(row.Cells[cantidad.Name].Value);
                        preunitario = Convert.ToDecimal(row.Cells[preciounit.Name].Value);
                        precioneto = Convert.ToDecimal(row.Cells[precioventa.Name].Value);                        
                        montodescuento = Convert.ToDecimal(row.Cells[montodscto.Name].Value);

                        bruto = cant * preunitario;
                        factorigv = precioneto - bruto;

                        valorvent = bruto - factorigv;
                        preciovent = valorvent + factorigv;
                        precior = preciovent / Convert.ToDecimal(cant);
                        varlorr = valorvent / Convert.ToDecimal(cant);
                        nigv = factorigv;

                        row.Cells[coddetalle.Name].Value = "";
                        row.Cells[codunidad.Name].Value = coduni;
                        row.Cells[codproducto.Name].Value = codpro;
                        row.Cells[descripcion.Name].Value = descrip;
                        row.Cells[unidad.Name].Value = unida;
                        row.Cells[dscto1.Name].Value = ds1;
                        row.Cells[dscto2.Name].Value = ds2;
                        row.Cells[dscto3.Name].Value = ds3;
                        row.Cells[referencia.Name].Value = refe;
                        row.Cells[cantidad.Name].Value = cant;
                        row.Cells[preciounit.Name].Value = preunitario;
                        row.Cells[precioventa.Name].Value = refe;
                        row.Cells[montodscto.Name].Value = montodescuento;
                        row.Cells[igv.Name].Value = nigv;
                        row.Cells[importe.Name].Value = bruto;
                        row.Cells[valorventa.Name].Value = valorvent;
                        row.Cells[precioventa.Name].Value = preciovent;
                        row.Cells[precioreal.Name].Value = precior;
                        row.Cells[valoreal.Name].Value = varlorr;
                        row.Cells[serielote.Name].Value = 0;
                        row.Cells[moneda.Name].Value = "";

                       
                        
                    }  
                    
                }
                     
            }
            else
            {
                if (dgvDetalle.Rows.Count > 0)
                {
                    Int32 i = 0;
                    foreach (DataGridViewRow row in dgvDetalle.Rows)
                    {
                        Decimal bruto = 0, valorvent = 0, figv = 0, preciovent = 0, montodescuento = 0, factorigv = 0, ds1 = 0, ds2 = 0, ds3 = 0, preunitario = 0, precior = 0, varlorr = 0;
                        Int32 cant = 0, codpro = 0, coduni = 0, indiceFila = 0;
                        String descrip = "", unida = "", refe = "";

                        indiceFila = dgvDetalle.CurrentCell.RowIndex;
                        DataGridViewRow rowi = dgvDetalle.Rows[indiceFila];

                        coduni = Convert.ToInt32(row.Cells[codunidad.Name].Value);
                        codpro = Convert.ToInt32(row.Cells[codproducto.Name].Value);
                        descrip = row.Cells[descripcion.Name].Value.ToString();
                        unida = row.Cells[unidad.Name].Value.ToString();
                        ds1 = Convert.ToDecimal(row.Cells[dscto1.Name].Value);
                        ds2 = Convert.ToDecimal(row.Cells[dscto2.Name].Value);
                        ds3 = Convert.ToDecimal(row.Cells[dscto3.Name].Value);
                        refe = row.Cells[referencia.Name].Value.ToString();
                        cant = Convert.ToInt32(row.Cells[cantidad.Name].Value);
                        preunitario = Convert.ToDecimal(row.Cells[preciounit.Name].Value);                        
                        montodescuento = Convert.ToDecimal(row.Cells[montodscto.Name].Value);
                        factorigv = Convert.ToDecimal(row.Cells[igv.Name].Value);

                        bruto = cant * preunitario;        
                        valorvent = bruto;
                        preciovent = valorvent + factorigv;    
                        precior = preciovent / Convert.ToDecimal(cant);
                        varlorr = valorvent / Convert.ToDecimal(cant);
                        figv = factorigv;

                        row.Cells[coddetalle.Name].Value = "";
                        row.Cells[codunidad.Name].Value = coduni;
                        row.Cells[codproducto.Name].Value = codpro;
                        row.Cells[descripcion.Name].Value = descrip;
                        row.Cells[unidad.Name].Value = unida;
                        row.Cells[dscto1.Name].Value = ds1;
                        row.Cells[dscto2.Name].Value = ds2;
                        row.Cells[dscto3.Name].Value = ds3;
                        row.Cells[referencia.Name].Value = refe;
                        row.Cells[cantidad.Name].Value = cant;
                        row.Cells[preciounit.Name].Value = preunitario;
                        row.Cells[precioventa.Name].Value = refe;
                        row.Cells[montodscto.Name].Value = montodescuento;
                        row.Cells[igv.Name].Value = figv;
                        row.Cells[importe.Name].Value = bruto;
                        row.Cells[valorventa.Name].Value = valorvent;
                        row.Cells[precioventa.Name].Value = preciovent;
                        row.Cells[precioreal.Name].Value = precior;
                        row.Cells[valoreal.Name].Value = varlorr;
                        row.Cells[serielote.Name].Value = 0;
                        row.Cells[moneda.Name].Value = "";
                        
                    }                                           
                }
                    
            }
            calculatotales();
        }

        private void txtNDocRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void SoloNumeros(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

            if (e.KeyChar == '-')
            {

                e.Handled = false;

            }
            else if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
        }

        private void txtFlete_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.SOLONumeros(sender, e);
            if (e.KeyChar == (char)Keys.Return)
            {
                prorrateodeflete();
                recalculadetalle();
                calculatotales();
            } 
        }

       


        
    }
}