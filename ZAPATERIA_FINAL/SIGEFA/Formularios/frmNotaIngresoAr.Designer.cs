namespace SIGEFA.Formularios
{
    partial class frmNotaIngresoAr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNotaIngresoAr));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCodProveedor = new System.Windows.Forms.TextBox();
            this.txtNombreCliente = new System.Windows.Forms.TextBox();
            this.txtCodCliente = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbValorVenta = new System.Windows.Forms.CheckBox();
            this.lbAutorizado = new System.Windows.Forms.Label();
            this.cmbMoneda = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtAutorizacion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDetalle = new System.Windows.Forms.Button();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOrdenCompra = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumDoc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNDocRef = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDocRef = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNombreProv = new System.Windows.Forms.TextBox();
            this.txtCodProv = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbNombreTransaccion = new System.Windows.Forms.Label();
            this.txtTransaccion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpFechaPago = new System.Windows.Forms.DateTimePicker();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtFlete = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.txtPrecioVenta = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtIGV = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDscto = new System.Windows.Forms.TextBox();
            this.txtValorVenta = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvDetalle = new System.Windows.Forms.DataGridView();
            this.coddetalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codunidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serielote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montodscto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventaconflete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pvconflete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valoreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaingreso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coduser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecharegistro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDetalle2 = new System.Windows.Forms.DataGridView();
            this.codetord = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coprod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coduni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descuento1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descuento2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descuento3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montodscto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventa1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igv1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioreal1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valoreal1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flete1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaldoIngresado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadPendiente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaldoIngresado1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadPendiente1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bonificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.requiredFieldValidator4 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.requiredFieldValidator3 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.requiredFieldValidator2 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.txtTipoCambio = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.requiredFieldValidator5 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCodProveedor);
            this.groupBox1.Controls.Add(this.txtNombreCliente);
            this.groupBox1.Controls.Add(this.txtCodCliente);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.cbValorVenta);
            this.groupBox1.Controls.Add(this.lbAutorizado);
            this.groupBox1.Controls.Add(this.cmbMoneda);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtAutorizacion);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnDetalle);
            this.groupBox1.Controls.Add(this.txtComentario);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtOrdenCompra);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtNumDoc);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtNDocRef);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtDocRef);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtNombreProv);
            this.groupBox1.Controls.Add(this.txtCodProv);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbNombreTransaccion);
            this.groupBox1.Controls.Add(this.txtTransaccion);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpFecha);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(986, 209);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cabecera";
            // 
            // txtCodProveedor
            // 
            this.txtCodProveedor.Location = new System.Drawing.Point(559, 46);
            this.txtCodProveedor.Name = "txtCodProveedor";
            this.txtCodProveedor.Size = new System.Drawing.Size(67, 20);
            this.txtCodProveedor.TabIndex = 45;
            this.txtCodProveedor.Visible = false;
            // 
            // txtNombreCliente
            // 
            this.txtNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCliente.Enabled = false;
            this.txtNombreCliente.Location = new System.Drawing.Point(196, 70);
            this.txtNombreCliente.Name = "txtNombreCliente";
            this.txtNombreCliente.ReadOnly = true;
            this.txtNombreCliente.Size = new System.Drawing.Size(357, 20);
            this.txtNombreCliente.TabIndex = 9;
            this.txtNombreCliente.Tag = "2";
            this.txtNombreCliente.Visible = false;
            // 
            // txtCodCliente
            // 
            this.txtCodCliente.BackColor = System.Drawing.Color.Wheat;
            this.txtCodCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodCliente.Location = new System.Drawing.Point(102, 70);
            this.txtCodCliente.Name = "txtCodCliente";
            this.txtCodCliente.Size = new System.Drawing.Size(88, 20);
            this.txtCodCliente.TabIndex = 3;
            this.txtCodCliente.Tag = "1";
            this.txtCodCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCodCliente.Visible = false;
            this.txtCodCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodCliente_KeyDown);
            this.txtCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCliente_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 73);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 13);
            this.label18.TabIndex = 44;
            this.label18.Tag = "1";
            this.label18.Text = "Cliente";
            this.label18.Visible = false;
            // 
            // cbValorVenta
            // 
            this.cbValorVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbValorVenta.AutoSize = true;
            this.cbValorVenta.Location = new System.Drawing.Point(893, 98);
            this.cbValorVenta.Name = "cbValorVenta";
            this.cbValorVenta.Size = new System.Drawing.Size(81, 17);
            this.cbValorVenta.TabIndex = 17;
            this.cbValorVenta.Text = "Valor Venta";
            this.cbValorVenta.UseVisualStyleBackColor = true;
            this.cbValorVenta.CheckedChanged += new System.EventHandler(this.cbValorVenta_CheckedChanged);
            // 
            // lbAutorizado
            // 
            this.lbAutorizado.AutoSize = true;
            this.lbAutorizado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAutorizado.Location = new System.Drawing.Point(559, 125);
            this.lbAutorizado.Name = "lbAutorizado";
            this.lbAutorizado.Size = new System.Drawing.Size(67, 13);
            this.lbAutorizado.TabIndex = 39;
            this.lbAutorizado.Tag = "22";
            this.lbAutorizado.Text = "Autorizado";
            this.lbAutorizado.Visible = false;
            // 
            // cmbMoneda
            // 
            this.cmbMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMoneda.FormattingEnabled = true;
            this.cmbMoneda.Location = new System.Drawing.Point(869, 43);
            this.cmbMoneda.Name = "cmbMoneda";
            this.cmbMoneda.Size = new System.Drawing.Size(105, 21);
            this.cmbMoneda.TabIndex = 4;
            this.superValidator1.SetValidator3(this.cmbMoneda, this.requiredFieldValidator4);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(813, 46);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Moneda :";
            // 
            // txtAutorizacion
            // 
            this.txtAutorizacion.Location = new System.Drawing.Point(478, 121);
            this.txtAutorizacion.Name = "txtAutorizacion";
            this.txtAutorizacion.Size = new System.Drawing.Size(75, 20);
            this.txtAutorizacion.TabIndex = 15;
            this.txtAutorizacion.Tag = "22";
            this.txtAutorizacion.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(404, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 19;
            this.label3.Tag = "22";
            this.label3.Text = "Autorizacion:";
            this.label3.Visible = false;
            // 
            // btnDetalle
            // 
            this.btnDetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetalle.Location = new System.Drawing.Point(899, 151);
            this.btnDetalle.Name = "btnDetalle";
            this.btnDetalle.Size = new System.Drawing.Size(75, 23);
            this.btnDetalle.TabIndex = 18;
            this.btnDetalle.Text = "Detalle";
            this.btnDetalle.UseVisualStyleBackColor = true;
            this.btnDetalle.Click += new System.EventHandler(this.btnDetalle_Click);
            // 
            // txtComentario
            // 
            this.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComentario.Location = new System.Drawing.Point(102, 148);
            this.txtComentario.Multiline = true;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(451, 55);
            this.txtComentario.TabIndex = 16;
            this.txtComentario.Tag = "21";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 17;
            this.label9.Tag = "21";
            this.label9.Text = "Glosa";
            // 
            // txtOrdenCompra
            // 
            this.txtOrdenCompra.Location = new System.Drawing.Point(478, 96);
            this.txtOrdenCompra.Name = "txtOrdenCompra";
            this.txtOrdenCompra.ReadOnly = true;
            this.txtOrdenCompra.Size = new System.Drawing.Size(75, 20);
            this.txtOrdenCompra.TabIndex = 4;
            this.txtOrdenCompra.Tag = "18";
            this.txtOrdenCompra.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(394, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 15;
            this.label8.Tag = "18";
            this.label8.Text = "Orden Compra:";
            this.label8.Visible = false;
            // 
            // txtNumDoc
            // 
            this.txtNumDoc.Enabled = false;
            this.txtNumDoc.Location = new System.Drawing.Point(464, 17);
            this.txtNumDoc.Name = "txtNumDoc";
            this.txtNumDoc.ReadOnly = true;
            this.txtNumDoc.Size = new System.Drawing.Size(89, 20);
            this.txtNumDoc.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(400, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Num. Doc.";
            // 
            // txtNDocRef
            // 
            this.txtNDocRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNDocRef.Location = new System.Drawing.Point(197, 96);
            this.txtNDocRef.Name = "txtNDocRef";
            this.txtNDocRef.Size = new System.Drawing.Size(75, 20);
            this.txtNDocRef.TabIndex = 6;
            this.txtNDocRef.Tag = "11";
            this.superValidator1.SetValidator1(this.txtNDocRef, this.requiredFieldValidator5);
            this.txtNDocRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNDocRef_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(136, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 2;
            this.label6.Tag = "11";
            this.label6.Text = "Num. Ref.";
            this.label6.Visible = false;
            // 
            // txtDocRef
            // 
            this.txtDocRef.BackColor = System.Drawing.Color.Wheat;
            this.txtDocRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocRef.Location = new System.Drawing.Point(102, 96);
            this.txtDocRef.Name = "txtDocRef";
            this.txtDocRef.Size = new System.Drawing.Size(28, 20);
            this.txtDocRef.TabIndex = 5;
            this.txtDocRef.Tag = "10";
            this.superValidator1.SetValidator3(this.txtDocRef, this.requiredFieldValidator3);
            this.txtDocRef.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDocRef_KeyDown);
            this.txtDocRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocRef_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 8;
            this.label5.Tag = "10";
            this.label5.Text = "Doc. Ref.";
            // 
            // txtNombreProv
            // 
            this.txtNombreProv.Enabled = false;
            this.txtNombreProv.Location = new System.Drawing.Point(197, 44);
            this.txtNombreProv.Name = "txtNombreProv";
            this.txtNombreProv.ReadOnly = true;
            this.txtNombreProv.Size = new System.Drawing.Size(356, 20);
            this.txtNombreProv.TabIndex = 7;
            this.txtNombreProv.Tag = "9";
            // 
            // txtCodProv
            // 
            this.txtCodProv.BackColor = System.Drawing.Color.Wheat;
            this.txtCodProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodProv.Location = new System.Drawing.Point(102, 44);
            this.txtCodProv.Name = "txtCodProv";
            this.txtCodProv.Size = new System.Drawing.Size(89, 20);
            this.txtCodProv.TabIndex = 2;
            this.txtCodProv.Tag = "8";
            this.txtCodProv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodProv_KeyDown);
            this.txtCodProv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodProv_KeyPress);
            this.txtCodProv.Leave += new System.EventHandler(this.txtCodProv_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 5;
            this.label4.Tag = "8";
            this.label4.Text = "Proveedor";
            // 
            // lbNombreTransaccion
            // 
            this.lbNombreTransaccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreTransaccion.Location = new System.Drawing.Point(136, 20);
            this.lbNombreTransaccion.Name = "lbNombreTransaccion";
            this.lbNombreTransaccion.Size = new System.Drawing.Size(258, 15);
            this.lbNombreTransaccion.TabIndex = 4;
            this.lbNombreTransaccion.Text = "NombreTransaccion";
            this.lbNombreTransaccion.Visible = false;
            // 
            // txtTransaccion
            // 
            this.txtTransaccion.BackColor = System.Drawing.Color.Wheat;
            this.txtTransaccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTransaccion.Location = new System.Drawing.Point(102, 17);
            this.txtTransaccion.Name = "txtTransaccion";
            this.txtTransaccion.Size = new System.Drawing.Size(28, 20);
            this.txtTransaccion.TabIndex = 1;
            this.txtTransaccion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTransaccion_KeyDown);
            this.txtTransaccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTransaccion_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Transacción";
            // 
            // dtpFecha
            // 
            this.dtpFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(893, 17);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(81, 20);
            this.dtpFecha.TabIndex = 3;
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(844, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fecha :";
            // 
            // dtpFechaPago
            // 
            this.dtpFechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaPago.Location = new System.Drawing.Point(248, 122);
            this.dtpFechaPago.Name = "dtpFechaPago";
            this.dtpFechaPago.Size = new System.Drawing.Size(81, 20);
            this.dtpFechaPago.TabIndex = 14;
            this.dtpFechaPago.Tag = "16";
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFormaPago.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(102, 122);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(140, 20);
            this.cmbFormaPago.TabIndex = 7;
            this.cmbFormaPago.Tag = "16";
            this.superValidator1.SetValidator3(this.cmbFormaPago, this.requiredFieldValidator2);
            this.cmbFormaPago.SelectionChangeCommitted += new System.EventHandler(this.cmbFormaPago_SelectionChangeCommitted);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 125);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 13);
            this.label17.TabIndex = 41;
            this.label17.Tag = "16";
            this.label17.Text = "Forma de Pago";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSalir);
            this.groupBox3.Controls.Add(this.btnNuevo);
            this.groupBox3.Controls.Add(this.btnGuardar);
            this.groupBox3.Controls.Add(this.btnEditar);
            this.groupBox3.Controls.Add(this.btnEliminar);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(0, 477);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(986, 47);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSalir.ImageIndex = 5;
            this.btnSalir.ImageList = this.imageList1;
            this.btnSalir.Location = new System.Drawing.Point(912, 9);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(68, 32);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // btnNuevo
            // 
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageIndex = 1;
            this.btnNuevo.ImageList = this.imageList1;
            this.btnNuevo.Location = new System.Drawing.Point(6, 10);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(71, 32);
            this.btnNuevo.TabIndex = 6;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevo.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageIndex = 4;
            this.btnGuardar.ImageList = this.imageList1;
            this.btnGuardar.Location = new System.Drawing.Point(829, 9);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(77, 32);
            this.btnGuardar.TabIndex = 11;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.ImageIndex = 0;
            this.btnEditar.ImageList = this.imageList1;
            this.btnEditar.Location = new System.Drawing.Point(83, 10);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(66, 32);
            this.btnEditar.TabIndex = 7;
            this.btnEditar.Text = "Editar";
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.ImageIndex = 2;
            this.btnEliminar.ImageList = this.imageList1;
            this.btnEliminar.Location = new System.Drawing.Point(155, 10);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 32);
            this.btnEliminar.TabIndex = 8;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtFlete);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtBruto);
            this.groupBox2.Controls.Add(this.txtPrecioVenta);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtIGV);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtDscto);
            this.groupBox2.Controls.Add(this.txtValorVenta);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.dgvDetalle);
            this.groupBox2.Controls.Add(this.dgvDetalle2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 209);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(986, 268);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detalle";
            // 
            // txtFlete
            // 
            this.txtFlete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFlete.Location = new System.Drawing.Point(583, 190);
            this.txtFlete.Name = "txtFlete";
            this.txtFlete.Size = new System.Drawing.Size(75, 20);
            this.txtFlete.TabIndex = 65;
            this.txtFlete.Tag = "7";
            this.txtFlete.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFlete_KeyPress);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(544, 192);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 13);
            this.label19.TabIndex = 66;
            this.label19.Tag = "7";
            this.label19.Text = "Flete:";
            // 
            // txtBruto
            // 
            this.txtBruto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBruto.Location = new System.Drawing.Point(272, 190);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.ReadOnly = true;
            this.txtBruto.Size = new System.Drawing.Size(78, 20);
            this.txtBruto.TabIndex = 23;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrecioVenta.Location = new System.Drawing.Point(869, 242);
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.ReadOnly = true;
            this.txtPrecioVenta.Size = new System.Drawing.Size(111, 20);
            this.txtPrecioVenta.TabIndex = 22;
            this.txtPrecioVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(776, 245);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "P. Venta :";
            // 
            // txtIGV
            // 
            this.txtIGV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIGV.Location = new System.Drawing.Point(869, 216);
            this.txtIGV.Name = "txtIGV";
            this.txtIGV.ReadOnly = true;
            this.txtIGV.Size = new System.Drawing.Size(111, 20);
            this.txtIGV.TabIndex = 20;
            this.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(776, 219);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "I.G.V. :";
            // 
            // txtDscto
            // 
            this.txtDscto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDscto.Location = new System.Drawing.Point(444, 190);
            this.txtDscto.Name = "txtDscto";
            this.txtDscto.ReadOnly = true;
            this.txtDscto.Size = new System.Drawing.Size(75, 20);
            this.txtDscto.TabIndex = 18;
            this.txtDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtValorVenta
            // 
            this.txtValorVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorVenta.Location = new System.Drawing.Point(869, 190);
            this.txtValorVenta.Name = "txtValorVenta";
            this.txtValorVenta.ReadOnly = true;
            this.txtValorVenta.Size = new System.Drawing.Size(111, 20);
            this.txtValorVenta.TabIndex = 17;
            this.txtValorVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(776, 193);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "V. Venta :";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(373, 193);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Descuento :";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(228, 193);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Bruto :";
            // 
            // dgvDetalle
            // 
            this.dgvDetalle.AllowUserToAddRows = false;
            this.dgvDetalle.AllowUserToResizeColumns = false;
            this.dgvDetalle.AllowUserToResizeRows = false;
            this.dgvDetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coddetalle,
            this.codproducto,
            this.referencia,
            this.descripcion,
            this.codunidad,
            this.moneda,
            this.unidad,
            this.serielote,
            this.cantidad,
            this.preciounit,
            this.importe,
            this.dscto1,
            this.dscto2,
            this.dscto3,
            this.montodscto,
            this.valorventa,
            this.valorventaconflete,
            this.igv,
            this.flete,
            this.precioventa,
            this.pvconflete,
            this.precioreal,
            this.valoreal,
            this.fechaingreso,
            this.coduser,
            this.fecharegistro});
            this.dgvDetalle.Location = new System.Drawing.Point(3, 16);
            this.dgvDetalle.Name = "dgvDetalle";
            this.dgvDetalle.ReadOnly = true;
            this.dgvDetalle.RowHeadersVisible = false;
            this.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalle.Size = new System.Drawing.Size(980, 168);
            this.dgvDetalle.TabIndex = 0;
            this.dgvDetalle.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalle_CellValueChanged);
            this.dgvDetalle.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvDetalle_RowsAdded);
            this.dgvDetalle.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvDetalle_RowStateChanged);
            // 
            // coddetalle
            // 
            this.coddetalle.DataPropertyName = "codDetalle";
            this.coddetalle.HeaderText = "CodDetalle";
            this.coddetalle.Name = "coddetalle";
            this.coddetalle.ReadOnly = true;
            this.coddetalle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.coddetalle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.coddetalle.Visible = false;
            // 
            // codproducto
            // 
            this.codproducto.DataPropertyName = "codProducto";
            this.codproducto.HeaderText = "CodProducto";
            this.codproducto.Name = "codproducto";
            this.codproducto.ReadOnly = true;
            this.codproducto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codproducto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codproducto.Visible = false;
            // 
            // referencia
            // 
            this.referencia.DataPropertyName = "referencia";
            this.referencia.HeaderText = "Codigo";
            this.referencia.Name = "referencia";
            this.referencia.ReadOnly = true;
            this.referencia.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.referencia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.referencia.Visible = false;
            this.referencia.Width = 90;
            // 
            // descripcion
            // 
            this.descripcion.DataPropertyName = "producto";
            this.descripcion.HeaderText = "Descripcion";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            this.descripcion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.descripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.descripcion.Width = 250;
            // 
            // codunidad
            // 
            this.codunidad.DataPropertyName = "codUnidadMedida";
            this.codunidad.HeaderText = "Cod. Unidad";
            this.codunidad.Name = "codunidad";
            this.codunidad.ReadOnly = true;
            this.codunidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codunidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codunidad.Visible = false;
            // 
            // moneda
            // 
            this.moneda.DataPropertyName = "moneda";
            this.moneda.HeaderText = "Moneda";
            this.moneda.Name = "moneda";
            this.moneda.ReadOnly = true;
            this.moneda.Visible = false;
            // 
            // unidad
            // 
            this.unidad.DataPropertyName = "unidad";
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.ReadOnly = true;
            this.unidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.unidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.unidad.Width = 80;
            // 
            // serielote
            // 
            this.serielote.DataPropertyName = "serielote";
            this.serielote.HeaderText = "Serie/Lote";
            this.serielote.Name = "serielote";
            this.serielote.ReadOnly = true;
            this.serielote.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.serielote.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.serielote.Width = 80;
            // 
            // cantidad
            // 
            this.cantidad.DataPropertyName = "cantidad";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.cantidad.DefaultCellStyle = dataGridViewCellStyle1;
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cantidad.Width = 80;
            // 
            // preciounit
            // 
            this.preciounit.DataPropertyName = "preciounitario";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N4";
            dataGridViewCellStyle2.NullValue = null;
            this.preciounit.DefaultCellStyle = dataGridViewCellStyle2;
            this.preciounit.HeaderText = "P. Unit.";
            this.preciounit.Name = "preciounit";
            this.preciounit.ReadOnly = true;
            this.preciounit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.preciounit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.preciounit.Width = 80;
            // 
            // importe
            // 
            this.importe.DataPropertyName = "subtotal";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N4";
            dataGridViewCellStyle3.NullValue = null;
            this.importe.DefaultCellStyle = dataGridViewCellStyle3;
            this.importe.HeaderText = "Importe";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            this.importe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.importe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dscto1
            // 
            this.dscto1.DataPropertyName = "descuento1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N4";
            this.dscto1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dscto1.HeaderText = "% Dscto1";
            this.dscto1.Name = "dscto1";
            this.dscto1.ReadOnly = true;
            this.dscto1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto1.Visible = false;
            // 
            // dscto2
            // 
            this.dscto2.DataPropertyName = "descuento2";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N4";
            this.dscto2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dscto2.HeaderText = "% Dscto2";
            this.dscto2.Name = "dscto2";
            this.dscto2.ReadOnly = true;
            this.dscto2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto2.Visible = false;
            // 
            // dscto3
            // 
            this.dscto3.DataPropertyName = "descuento3";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N4";
            this.dscto3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dscto3.HeaderText = "% Dscto3";
            this.dscto3.Name = "dscto3";
            this.dscto3.ReadOnly = true;
            this.dscto3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto3.Visible = false;
            // 
            // montodscto
            // 
            this.montodscto.DataPropertyName = "montodscto";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N4";
            dataGridViewCellStyle7.NullValue = null;
            this.montodscto.DefaultCellStyle = dataGridViewCellStyle7;
            this.montodscto.HeaderText = "Monto Dscto";
            this.montodscto.Name = "montodscto";
            this.montodscto.ReadOnly = true;
            this.montodscto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.montodscto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // valorventa
            // 
            this.valorventa.DataPropertyName = "valorventa";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N4";
            dataGridViewCellStyle8.NullValue = null;
            this.valorventa.DefaultCellStyle = dataGridViewCellStyle8;
            this.valorventa.HeaderText = "V. Venta";
            this.valorventa.Name = "valorventa";
            this.valorventa.ReadOnly = true;
            this.valorventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valorventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // valorventaconflete
            // 
            this.valorventaconflete.DataPropertyName = "vvconflete";
            this.valorventaconflete.HeaderText = "vvconflete";
            this.valorventaconflete.Name = "valorventaconflete";
            this.valorventaconflete.ReadOnly = true;
            this.valorventaconflete.Visible = false;
            // 
            // igv
            // 
            this.igv.DataPropertyName = "igv";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N4";
            this.igv.DefaultCellStyle = dataGridViewCellStyle9;
            this.igv.HeaderText = "IGV";
            this.igv.Name = "igv";
            this.igv.ReadOnly = true;
            this.igv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.igv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // flete
            // 
            this.flete.DataPropertyName = "flete";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.flete.DefaultCellStyle = dataGridViewCellStyle10;
            this.flete.HeaderText = "Flete";
            this.flete.Name = "flete";
            this.flete.ReadOnly = true;
            // 
            // precioventa
            // 
            this.precioventa.DataPropertyName = "importe";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N4";
            this.precioventa.DefaultCellStyle = dataGridViewCellStyle11;
            this.precioventa.HeaderText = "P. Venta";
            this.precioventa.Name = "precioventa";
            this.precioventa.ReadOnly = true;
            this.precioventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // pvconflete
            // 
            this.pvconflete.DataPropertyName = "pvconflete";
            this.pvconflete.HeaderText = "pvconflete";
            this.pvconflete.Name = "pvconflete";
            this.pvconflete.ReadOnly = true;
            this.pvconflete.Visible = false;
            // 
            // precioreal
            // 
            this.precioreal.DataPropertyName = "precioreal";
            this.precioreal.HeaderText = "P. real";
            this.precioreal.Name = "precioreal";
            this.precioreal.ReadOnly = true;
            this.precioreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.precioreal.Visible = false;
            // 
            // valoreal
            // 
            this.valoreal.DataPropertyName = "valoreal";
            this.valoreal.HeaderText = "V. real";
            this.valoreal.Name = "valoreal";
            this.valoreal.ReadOnly = true;
            this.valoreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valoreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valoreal.Visible = false;
            // 
            // fechaingreso
            // 
            this.fechaingreso.DataPropertyName = "fechaingreso";
            this.fechaingreso.HeaderText = "FechaIngre";
            this.fechaingreso.Name = "fechaingreso";
            this.fechaingreso.ReadOnly = true;
            this.fechaingreso.Visible = false;
            // 
            // coduser
            // 
            this.coduser.DataPropertyName = "codUser";
            this.coduser.HeaderText = "CodUser";
            this.coduser.Name = "coduser";
            this.coduser.ReadOnly = true;
            this.coduser.Visible = false;
            // 
            // fecharegistro
            // 
            this.fecharegistro.DataPropertyName = "fecharegistro";
            this.fecharegistro.HeaderText = "Fecha Reg";
            this.fecharegistro.Name = "fecharegistro";
            this.fecharegistro.ReadOnly = true;
            this.fecharegistro.Visible = false;
            // 
            // dgvDetalle2
            // 
            this.dgvDetalle2.AllowUserToAddRows = false;
            this.dgvDetalle2.AllowUserToOrderColumns = true;
            this.dgvDetalle2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDetalle2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalle2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codetord,
            this.coprod,
            this.codref,
            this.desc,
            this.uni,
            this.coduni,
            this.cant,
            this.stock,
            this.cantn,
            this.preciounitario,
            this.subtotal,
            this.descuento1,
            this.descuento2,
            this.descuento3,
            this.montodscto1,
            this.valorventa1,
            this.igv1,
            this.importe1,
            this.precioreal1,
            this.valoreal1,
            this.flete1,
            this.SaldoIngresado,
            this.cantidadPendiente,
            this.SaldoIngresado1,
            this.cantidadPendiente1,
            this.Bonificacion});
            this.dgvDetalle2.Location = new System.Drawing.Point(3, 17);
            this.dgvDetalle2.Name = "dgvDetalle2";
            this.dgvDetalle2.RowHeadersVisible = false;
            this.dgvDetalle2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalle2.Size = new System.Drawing.Size(977, 167);
            this.dgvDetalle2.TabIndex = 68;
            this.dgvDetalle2.Visible = false;
            // 
            // codetord
            // 
            this.codetord.DataPropertyName = "codDetalleOrden";
            this.codetord.HeaderText = "codDetalle";
            this.codetord.Name = "codetord";
            this.codetord.Visible = false;
            // 
            // coprod
            // 
            this.coprod.DataPropertyName = "codProducto";
            this.coprod.HeaderText = "codProducto";
            this.coprod.Name = "coprod";
            this.coprod.ReadOnly = true;
            this.coprod.Visible = false;
            // 
            // codref
            // 
            this.codref.DataPropertyName = "referencia";
            this.codref.HeaderText = "Referencia";
            this.codref.Name = "codref";
            this.codref.ReadOnly = true;
            this.codref.Width = 150;
            // 
            // desc
            // 
            this.desc.DataPropertyName = "producto";
            this.desc.HeaderText = "Descripcion";
            this.desc.Name = "desc";
            this.desc.ReadOnly = true;
            this.desc.Width = 300;
            // 
            // uni
            // 
            this.uni.DataPropertyName = "unidad";
            this.uni.HeaderText = "Unidad";
            this.uni.Name = "uni";
            this.uni.ReadOnly = true;
            // 
            // coduni
            // 
            this.coduni.DataPropertyName = "codUnidadMedida";
            this.coduni.HeaderText = "codUnidad";
            this.coduni.Name = "coduni";
            this.coduni.Visible = false;
            // 
            // cant
            // 
            this.cant.DataPropertyName = "cantidad";
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            this.cant.DefaultCellStyle = dataGridViewCellStyle12;
            this.cant.HeaderText = "Q Original";
            this.cant.Name = "cant";
            this.cant.ReadOnly = true;
            // 
            // stock
            // 
            this.stock.DataPropertyName = "codControlStock";
            dataGridViewCellStyle13.Format = "N2";
            dataGridViewCellStyle13.NullValue = null;
            this.stock.DefaultCellStyle = dataGridViewCellStyle13;
            this.stock.HeaderText = "Stock";
            this.stock.Name = "stock";
            this.stock.Visible = false;
            // 
            // cantn
            // 
            this.cantn.DataPropertyName = "cantidadn";
            dataGridViewCellStyle14.Format = "N2";
            dataGridViewCellStyle14.NullValue = null;
            this.cantn.DefaultCellStyle = dataGridViewCellStyle14;
            this.cantn.HeaderText = "Q Ingreso";
            this.cantn.MaxInputLength = 11;
            this.cantn.Name = "cantn";
            // 
            // preciounitario
            // 
            this.preciounitario.DataPropertyName = "preciounitario";
            this.preciounitario.HeaderText = "preciounitario";
            this.preciounitario.Name = "preciounitario";
            this.preciounitario.Visible = false;
            // 
            // subtotal
            // 
            this.subtotal.DataPropertyName = "subtotal";
            dataGridViewCellStyle15.Format = "N4";
            dataGridViewCellStyle15.NullValue = null;
            this.subtotal.DefaultCellStyle = dataGridViewCellStyle15;
            this.subtotal.HeaderText = "subtotal";
            this.subtotal.Name = "subtotal";
            this.subtotal.Visible = false;
            // 
            // descuento1
            // 
            this.descuento1.DataPropertyName = "descuento1";
            this.descuento1.HeaderText = "descuento1";
            this.descuento1.Name = "descuento1";
            this.descuento1.Visible = false;
            // 
            // descuento2
            // 
            this.descuento2.DataPropertyName = "descuento2";
            this.descuento2.HeaderText = "descuento2";
            this.descuento2.Name = "descuento2";
            this.descuento2.Visible = false;
            // 
            // descuento3
            // 
            this.descuento3.DataPropertyName = "descuento3";
            this.descuento3.HeaderText = "descuento3";
            this.descuento3.Name = "descuento3";
            this.descuento3.Visible = false;
            // 
            // montodscto1
            // 
            this.montodscto1.DataPropertyName = "montodscto1";
            this.montodscto1.HeaderText = "montodscto1";
            this.montodscto1.Name = "montodscto1";
            this.montodscto1.Visible = false;
            // 
            // valorventa1
            // 
            this.valorventa1.DataPropertyName = "valorventa1";
            dataGridViewCellStyle16.Format = "N4";
            dataGridViewCellStyle16.NullValue = null;
            this.valorventa1.DefaultCellStyle = dataGridViewCellStyle16;
            this.valorventa1.HeaderText = "valorventa1";
            this.valorventa1.Name = "valorventa1";
            this.valorventa1.Visible = false;
            // 
            // igv1
            // 
            this.igv1.DataPropertyName = "igv1";
            dataGridViewCellStyle17.Format = "N4";
            dataGridViewCellStyle17.NullValue = null;
            this.igv1.DefaultCellStyle = dataGridViewCellStyle17;
            this.igv1.HeaderText = "igv1";
            this.igv1.Name = "igv1";
            this.igv1.Visible = false;
            // 
            // importe1
            // 
            this.importe1.DataPropertyName = "importe1";
            dataGridViewCellStyle18.Format = "N4";
            dataGridViewCellStyle18.NullValue = null;
            this.importe1.DefaultCellStyle = dataGridViewCellStyle18;
            this.importe1.HeaderText = "importe1";
            this.importe1.Name = "importe1";
            this.importe1.Visible = false;
            // 
            // precioreal1
            // 
            this.precioreal1.DataPropertyName = "precioreal1";
            dataGridViewCellStyle19.Format = "N4";
            dataGridViewCellStyle19.NullValue = null;
            this.precioreal1.DefaultCellStyle = dataGridViewCellStyle19;
            this.precioreal1.HeaderText = "precioreal1";
            this.precioreal1.Name = "precioreal1";
            this.precioreal1.Visible = false;
            // 
            // valoreal1
            // 
            this.valoreal1.DataPropertyName = "valoreal1";
            dataGridViewCellStyle20.Format = "N4";
            dataGridViewCellStyle20.NullValue = null;
            this.valoreal1.DefaultCellStyle = dataGridViewCellStyle20;
            this.valoreal1.HeaderText = "valoreal1";
            this.valoreal1.Name = "valoreal1";
            this.valoreal1.Visible = false;
            // 
            // flete1
            // 
            this.flete1.DataPropertyName = "flete1";
            dataGridViewCellStyle21.Format = "N4";
            dataGridViewCellStyle21.NullValue = null;
            this.flete1.DefaultCellStyle = dataGridViewCellStyle21;
            this.flete1.HeaderText = "flete1";
            this.flete1.Name = "flete1";
            this.flete1.Visible = false;
            // 
            // SaldoIngresado
            // 
            this.SaldoIngresado.DataPropertyName = "SaldoIngresado";
            dataGridViewCellStyle22.Format = "N2";
            dataGridViewCellStyle22.NullValue = null;
            this.SaldoIngresado.DefaultCellStyle = dataGridViewCellStyle22;
            this.SaldoIngresado.HeaderText = "Q Acumulada";
            this.SaldoIngresado.MaxInputLength = 11;
            this.SaldoIngresado.Name = "SaldoIngresado";
            this.SaldoIngresado.ReadOnly = true;
            // 
            // cantidadPendiente
            // 
            this.cantidadPendiente.DataPropertyName = "cantidadPendiente";
            dataGridViewCellStyle23.Format = "N2";
            dataGridViewCellStyle23.NullValue = null;
            this.cantidadPendiente.DefaultCellStyle = dataGridViewCellStyle23;
            this.cantidadPendiente.HeaderText = "Q Por Atender";
            this.cantidadPendiente.Name = "cantidadPendiente";
            this.cantidadPendiente.ReadOnly = true;
            // 
            // SaldoIngresado1
            // 
            this.SaldoIngresado1.DataPropertyName = "SaldoIngresado1";
            dataGridViewCellStyle24.Format = "N2";
            dataGridViewCellStyle24.NullValue = null;
            this.SaldoIngresado1.DefaultCellStyle = dataGridViewCellStyle24;
            this.SaldoIngresado1.HeaderText = "SaldoIngresado1";
            this.SaldoIngresado1.Name = "SaldoIngresado1";
            this.SaldoIngresado1.Visible = false;
            // 
            // cantidadPendiente1
            // 
            this.cantidadPendiente1.DataPropertyName = "cantidadPendiente1";
            dataGridViewCellStyle25.Format = "N2";
            dataGridViewCellStyle25.NullValue = null;
            this.cantidadPendiente1.DefaultCellStyle = dataGridViewCellStyle25;
            this.cantidadPendiente1.HeaderText = "cantidadPendiente1";
            this.cantidadPendiente1.Name = "cantidadPendiente1";
            this.cantidadPendiente1.Visible = false;
            // 
            // Bonificacion
            // 
            this.Bonificacion.DataPropertyName = "Bonificacion";
            dataGridViewCellStyle26.NullValue = "0";
            this.Bonificacion.DefaultCellStyle = dataGridViewCellStyle26;
            this.Bonificacion.HeaderText = "Bonificacion";
            this.Bonificacion.Name = "Bonificacion";
            this.Bonificacion.Visible = false;
            // 
            // superValidator1
            // 
            this.superValidator1.ErrorProvider = this.errorProvider1;
            this.superValidator1.Highlighter = this.highlighter1;
            // 
            // requiredFieldValidator4
            // 
            this.requiredFieldValidator4.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator4.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator3
            // 
            this.requiredFieldValidator3.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator3.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator2
            // 
            this.requiredFieldValidator2.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator2.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // txtTipoCambio
            // 
            this.txtTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTipoCambio.Location = new System.Drawing.Point(893, 73);
            this.txtTipoCambio.Name = "txtTipoCambio";
            this.txtTipoCambio.ReadOnly = true;
            this.txtTipoCambio.Size = new System.Drawing.Size(81, 20);
            this.txtTipoCambio.TabIndex = 46;
            this.txtTipoCambio.Tag = "15";
            this.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(791, 73);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 13);
            this.label16.TabIndex = 47;
            this.label16.Tag = "15";
            this.label16.Text = "Tipo/Cambio :";
            // 
            // requiredFieldValidator5
            // 
            this.requiredFieldValidator5.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator5.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // frmNotaIngresoAr
            // 
            this.ClientSize = new System.Drawing.Size(986, 524);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtTipoCambio);
            this.Controls.Add(this.dtpFechaPago);
            this.Controls.Add(this.cmbFormaPago);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Name = "frmNotaIngresoAr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nota de Ingreso";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmNotaIngresoAr_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCodProveedor;
        public System.Windows.Forms.TextBox txtNombreCliente;
        public System.Windows.Forms.TextBox txtCodCliente;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox cbValorVenta;
        private System.Windows.Forms.DateTimePicker dtpFechaPago;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbAutorizado;
        public System.Windows.Forms.ComboBox cmbMoneda;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtAutorizacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDetalle;
        private System.Windows.Forms.TextBox txtComentario;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOrdenCompra;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumDoc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNDocRef;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtDocRef;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtNombreProv;
        public System.Windows.Forms.TextBox txtCodProv;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbNombreTransaccion;
        public System.Windows.Forms.TextBox txtTransaccion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtFlete;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.TextBox txtPrecioVenta;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtIGV;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDscto;
        private System.Windows.Forms.TextBox txtValorVenta;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.DataGridView dgvDetalle2;
        private System.Windows.Forms.DataGridViewTextBoxColumn codetord;
        private System.Windows.Forms.DataGridViewTextBoxColumn coprod;
        private System.Windows.Forms.DataGridViewTextBoxColumn codref;
        private System.Windows.Forms.DataGridViewTextBoxColumn desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn uni;
        private System.Windows.Forms.DataGridViewTextBoxColumn coduni;
        private System.Windows.Forms.DataGridViewTextBoxColumn cant;
        private System.Windows.Forms.DataGridViewTextBoxColumn stock;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantn;
        private System.Windows.Forms.DataGridViewTextBoxColumn preciounitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn descuento1;
        private System.Windows.Forms.DataGridViewTextBoxColumn descuento2;
        private System.Windows.Forms.DataGridViewTextBoxColumn descuento3;
        private System.Windows.Forms.DataGridViewTextBoxColumn montodscto1;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorventa1;
        private System.Windows.Forms.DataGridViewTextBoxColumn igv1;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe1;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioreal1;
        private System.Windows.Forms.DataGridViewTextBoxColumn valoreal1;
        private System.Windows.Forms.DataGridViewTextBoxColumn flete1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaldoIngresado;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadPendiente;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaldoIngresado1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadPendiente1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bonificacion;
        public System.Windows.Forms.DataGridView dgvDetalle;
        private System.Windows.Forms.ImageList imageList1;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private System.Windows.Forms.TextBox txtTipoCambio;
        private System.Windows.Forms.Label label16;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator2;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator4;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator3;
        private System.Windows.Forms.DataGridViewTextBoxColumn coddetalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn codproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn codunidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn serielote;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn preciounit;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto3;
        private System.Windows.Forms.DataGridViewTextBoxColumn montodscto;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorventaconflete;
        private System.Windows.Forms.DataGridViewTextBoxColumn igv;
        private System.Windows.Forms.DataGridViewTextBoxColumn flete;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn pvconflete;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioreal;
        private System.Windows.Forms.DataGridViewTextBoxColumn valoreal;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaingreso;
        private System.Windows.Forms.DataGridViewTextBoxColumn coduser;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecharegistro;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator5;
    }
}