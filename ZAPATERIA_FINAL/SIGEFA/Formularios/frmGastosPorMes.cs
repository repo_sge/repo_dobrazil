using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace SIGEFA.Formularios
{
    public partial class frmGastosPorMes : DevComponents.DotNetBar.OfficeForm
    {
        clsAdmTipoGasto admTipo = new clsAdmTipoGasto();
        DataTable dtDetalle ;

        public frmGastosPorMes()
        {
            InitializeComponent();
        }

        private void frmGastosPorMes_Load(object sender, EventArgs e)
        {
            CargarTipoDeGasto();
            int rowIndex = this.dgvGastosGenerales.Rows.Add();
            var row = this.dgvGastosGenerales.Rows[rowIndex];
            row.Cells["Total"].Value = 0;
            
            CrearTabla();
            
        }

        private void CrearTabla()
        {
            
            this.dtDetalle = new DataTable("Detalle");
            this.dtDetalle.Columns.Add("Items", System.Type.GetType("System.Int32"));
            this.dtDetalle.Columns.Add("Mes", System.Type.GetType("System.Int32"));
            this.dtDetalle.Columns.Add("codTipoGasto", System.Type.GetType("System.Int32"));
            this.dtDetalle.Columns.Add("DescripcionGasto", System.Type.GetType("System.String"));
            this.dtDetalle.Columns.Add("Total", System.Type.GetType("System.Decimal"));
            
            this.dgvGastosGenerales.DataSource = this.dtDetalle;

           
        }

        private void CargarTipoDeGasto()
        {
            cbtipogasto.DataSource = admTipo.MostrarTiposGastos();
            cbtipogasto.DisplayMember = "descripcion";
            cbtipogasto.ValueMember = "codtipogasto";
            cbtipogasto.SelectedIndex = -1;
        }


        Int32 cont = 1;
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            
            Int32 codgasto = Convert.ToInt32(cbtipogasto.SelectedValue);
            String descripcion = cbtipogasto.Text;
            Double total = Convert.ToDouble(txtTotal.Text);
            Int32 mes = Convert.ToInt32(udmeses.Text);

            DataRow row = this.dtDetalle.NewRow();

            row["Items"] = cont;
            row["Mes"] = mes;
            row["codTipoGasto"] = codgasto;
            row["DescripcionGasto"] = descripcion;
            row["Total"] = total;
            this.dtDetalle.Rows.Add(row);            
            cont++;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dgvGastosGenerales.Rows.Count > 0)
            {
                int indice = this.dgvGastosGenerales.CurrentCell.RowIndex;
                DataRow row = this.dtDetalle.Rows[indice];
                this.dtDetalle.Rows.Remove(row);
                cont--;
            }
        }

        private void dgvGastosGenerales_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //CargaTotal();
        }

        private void CargaTotal()
        {
            Double resul = 0;
            resul = dgvGastosGenerales.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDouble(x.Cells["Total"].Value));
            DataGridViewRow rowtotal = dgvGastosGenerales.Rows[dgvGastosGenerales.Rows.Count-1];            
            rowtotal.Cells["Total"].Value = resul;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CargaTotal();
        }

       

       

    }
}