﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.InterMySql
{
    class MysqlTallaProducto : ITallaProducto
    {

        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public int actualizaStockTallaProducto(clsTallaProducto tallaproducto, decimal cantidad, int opc)
        {
            int rows = 0;
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("actualizaStockTallaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_idtalla", tallaproducto.IdTalla);
                cmd.Parameters.AddWithValue("_idproducto", tallaproducto.IdProducto);
                cmd.Parameters.AddWithValue("_cantidad", cantidad);
                cmd.Parameters.AddWithValue("_idalmacen", tallaproducto.IdAlmacen);
                cmd.Parameters.AddWithValue("_opc", opc);
                int x = cmd.ExecuteNonQuery();

                return rows;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int actualizaStockTallaProductoVenta(clsTallaProducto tallaproducto, decimal cantidad)
        {
            int rows = 0;
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("actualizaStockTallaProductoVenta", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_idtalla", tallaproducto.IdTalla);
                cmd.Parameters.AddWithValue("_idproducto", tallaproducto.IdProducto);
                cmd.Parameters.AddWithValue("_cantidad", cantidad);
                cmd.Parameters.AddWithValue("_idalmacen", tallaproducto.IdAlmacen);

                int x = cmd.ExecuteNonQuery();

                return rows;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(clsTallaProducto tallaproducto)
        {
            int rows = 0;
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("eliminaTallaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_idtalla", tallaproducto.IdTalla);
                cmd.Parameters.AddWithValue("_idproducto", tallaproducto.IdProducto);

                int x = cmd.ExecuteNonQuery();

                return rows;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public int Insert(clsTallaProducto tallaproducto)
        {
            int rows = 0;
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaTallaxProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_idtalla", tallaproducto.IdTalla);
                cmd.Parameters.AddWithValue("_idproducto", tallaproducto.IdProducto);
                cmd.Parameters.AddWithValue("_idalmacen", tallaproducto.IdAlmacen);

                int x = cmd.ExecuteNonQuery();

                return rows;

            }catch(Exception ex)
            {
                throw ex;
            }
        }

        public decimal muestraStockTalla(clsTallaProducto tallaproducto)
        {
            Decimal stock = 0;
            try
            {

                con.conectarBD();
                cmd = new MySqlCommand("muestraStockTalla", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_idtalla", tallaproducto.IdTalla);
                cmd.Parameters.AddWithValue("_idproducto", tallaproducto.IdProducto);
                cmd.Parameters.AddWithValue("_idalmacen", tallaproducto.IdAlmacen);
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {

                    while (dr.Read())
                    {
                        stock = dr.GetDecimal(0);
                    }

                }


                return stock;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public List<clsTalla> tallaxidtallaxproducto(int idproducto, int idalmacen)
        {
            List<clsTalla> lst = null;
            clsTalla talla = null;
            //tabla = new DataTable();
            
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("tallaxidtallaxproducto", con.conector);
                cmd.Parameters.AddWithValue("_idproducto", idproducto);
                cmd.Parameters.AddWithValue("_idalmacen", idalmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                //adap = new MySqlDataAdapter(cmd);
                //adap.Fill(tabla);

                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lst = new List<clsTalla>();

                    while (dr.Read())
                    {
                        talla = new clsTalla();
                        talla.codTalla = dr.GetInt32(0);
                        talla.Nombre = dr.GetString(1);
                        talla.CodLinea = dr.GetInt32(3);
                        talla.Valor = dr.GetString(4);
                        talla.Estado = dr.GetInt32(5);
                        talla.TipoTalla = dr.GetInt32(6);
                        lst.Add(talla);
                    }

                }

                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public List<clsTallaProducto> tallaxproducto(int idproducto)
        {
            List<clsTallaProducto> lst = null;
            clsTallaProducto tallaproducto = null;
            try
            {
                
                con.conectarBD();
                cmd = new MySqlCommand("MuestraTallaxProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_idproducto", idproducto);
                //cmd.Parameters.AddWithValue("_idalmacen", idproducto);
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lst = new List<clsTallaProducto>();

                    while (dr.Read())
                    {
                        tallaproducto = new clsTallaProducto();
                        tallaproducto.IdTalla_producto = dr.GetInt32(0);
                        tallaproducto.IdTalla = dr.GetInt32(1);
                        tallaproducto.IdProducto = dr.GetInt32(2);
                        tallaproducto.IdAlmacen = dr.GetInt32(3);
                        tallaproducto.Stock = dr.GetDecimal(4);
                        tallaproducto.FechaRegistro = dr.GetDateTime(5);
                        lst.Add(tallaproducto);
                    }

                }

                
                return lst;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public List<clsTallaProducto> tallaxproductoxalmacen(int idproducto, int idalmacen)
        {
            List<clsTallaProducto> lst = null;
            clsTallaProducto tallaproducto = null;
            try
            {

                con.conectarBD();
                cmd = new MySqlCommand("tallaxproductoxalmacen", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_idproducto", idproducto);
                cmd.Parameters.AddWithValue("_idalmacen", idalmacen);
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lst = new List<clsTallaProducto>();

                    while (dr.Read())
                    {
                        tallaproducto = new clsTallaProducto();
                        tallaproducto.IdTalla_producto = dr.GetInt32(0);
                        tallaproducto.IdTalla = dr.GetInt32(1);
                        tallaproducto.IdProducto = dr.GetInt32(2);
                        tallaproducto.IdAlmacen = dr.GetInt32(3);
                        tallaproducto.Stock = dr.GetDecimal(4);
                        tallaproducto.FechaRegistro = dr.GetDateTime(5);
                        lst.Add(tallaproducto);
                    }

                }


                return lst;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
