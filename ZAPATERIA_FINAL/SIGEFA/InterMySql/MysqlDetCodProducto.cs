﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using SIGEFA.Conexion;
using MySql.Data.MySqlClient;
using System.Data;

namespace SIGEFA.InterMySql
{
    class MysqlDetCodProducto:IDetCodProducto
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adp = null;
        DataTable tabla = null;

        public Boolean Insert(int codpro,String detcodproducto)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("GuardaDetalleCodigoProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codprod", codpro);
                oParam = cmd.Parameters.AddWithValue("detcodproducto", detcodproducto);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Update(Entidades.clsDetCodProducto DetalleCodProducto)
        {
            throw new NotImplementedException();
        }

        public Boolean Delete(Entidades.clsDetCodProducto DetalleCodProducto)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Entidades.clsDetCodProducto DetalleCodProducto)
        {
            throw new NotImplementedException();
        }
    }
}
