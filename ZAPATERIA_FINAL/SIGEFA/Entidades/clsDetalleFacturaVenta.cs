﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsDetalleFacturaVenta
    {
        #region propiedades


        private Int32 iCodDetalleVenta;
        private Int32 iCodProducto;
        private String sReferencia;
        private String sDescripcion;
        private Int32 iCodVenta;
        private Int32 iCodAlmacen;
        private Int32 iUnidadIngresada;
        private String sSerieLote;
        private decimal dCantidad;
        private decimal dCantidadPendiente;
        private Int32 iCodUnidad;
        private String sUnidad;
        private decimal dPrecioUnitario;
        private decimal dSubtotal;
        private decimal dDescuento1;
        private decimal dDescuento2;
        private decimal dDescuento3;
        private decimal dMontoDescuento;
        private decimal dIgv;
        private decimal dImporte;
        private decimal dPrecioVenta;
        private decimal dValorVenta;
        private decimal dPrecioReal;
        private decimal dValoReal;
        private DateTime dFechaRegistro;
        private Int32 iCodUser;
        private Int32 iCodNotaSalida;
        private Int32 iMoneda;
        private Int32 iCodDetalleCotizacion;
        private Int32 iCodDetallePedido;
        private Int32 icodDetalleSeparacion;
        private clsTipoImpuesto tipoimpuesto;
        private Int32 codTalla;

        public Int32 CodDetallePedido
        {
            get { return iCodDetallePedido; }
            set { iCodDetallePedido = value; }
        }
        public Int32 CodDetalleVenta
        {
            get { return iCodDetalleVenta; }
            set { iCodDetalleVenta = value; }
        }
        public Int32 CodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
        public String Referencia
        {
            get { return sReferencia; }
            set { sReferencia = value; }
        }
        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }
        public Int32 CodVenta
        {
            get { return iCodVenta; }
            set { iCodVenta = value; }
        }
        public Int32 CodAlmacen
        {
            get { return iCodAlmacen; }
            set { iCodAlmacen = value; }
        }
        public Int32 UnidadIngresada
        {
            get { return iUnidadIngresada; }
            set { iUnidadIngresada = value; }
        }
        public String Unidad
        {
            get { return sUnidad; }
            set { sUnidad = value; }
        }
        public String SerieLote
        {
            get { return sSerieLote; }
            set { sSerieLote = value; }
        }
        public decimal Cantidad
        {
            get { return dCantidad; }
            set { dCantidad = value; }
        }
        public Int32 CodUnidad
        {
            get { return iCodUnidad; }
            set { iCodUnidad = value; }
        }
        public decimal PrecioUnitario
        {
            get { return dPrecioUnitario; }
            set { dPrecioUnitario = value; }
        }
        public decimal Subtotal
        {
            get { return dSubtotal; }
            set { dSubtotal = value; }
        }
        public decimal Descuento1
        {
            get { return dDescuento1; }
            set { dDescuento1 = value; }
        }
        public decimal Descuento2
        {
            get { return dDescuento2; }
            set { dDescuento2 = value; }
        }
        public decimal Descuento3
        {
            get { return dDescuento3; }
            set { dDescuento3 = value; }
        }
        public decimal MontoDescuento
        {
            get { return dMontoDescuento; }
            set { dMontoDescuento = value; }
        }
        public decimal Igv
        {
            get { return dIgv; }
            set { dIgv = value; }
        }
        public decimal Importe
        {
            get { return dImporte; }
            set { dImporte = value; }
        }
        public decimal PrecioVenta
        {
            get { return dPrecioVenta; }
            set { dPrecioVenta = value; }
        }
        public decimal ValorVenta
        {
            get { return dValorVenta; }
            set { dValorVenta = value; }
        }
        public decimal PrecioReal
        {
            get { return dPrecioReal; }
            set { dPrecioReal = value; }
        }
        public decimal ValoReal
        {
            get { return dValoReal; }
            set { dValoReal = value; }
        }
        public DateTime FechaRegistro
        {
            get { return dFechaRegistro; }
            set { dFechaRegistro = value; }
        }
        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }

        public decimal CantidadPendiente
        {
            get { return dCantidadPendiente; }
            set { dCantidadPendiente = value; }
        }

        public Int32 CodNotaSalida
        {
            get { return iCodNotaSalida; }
            set { iCodNotaSalida = value; }
        }

        public Int32 Moneda
        {
            get { return iMoneda; }
            set { iMoneda = value; }
        }

        public Int32 CodDetalleCotizacion
        {
            get { return iCodDetalleCotizacion; }
            set { iCodDetalleCotizacion = value; }
        }

        #endregion propiedades

        public Int32 CodDetalleSeparacion
        {
            get { return icodDetalleSeparacion; }
            set { icodDetalleSeparacion = value; }
        }

        public int CodTalla { get => codTalla; set => codTalla = value; }
        internal clsTipoImpuesto Tipoimpuesto { get => tipoimpuesto; set => tipoimpuesto = value; }

        public string Impuesto { get; set; }

        public int TipoUnidad { get; set; }

        public int CodTipoArticulo { get; set; }
    }
}
