﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsDetCodProducto
    {
        private Int32 iCodProducto;
        private String iDetCodProducto;

        public String IDetCodProducto
        {
            get { return iDetCodProducto; }
            set { iDetCodProducto = value; }
        }

        public Int32 ICodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
        
    }
}
