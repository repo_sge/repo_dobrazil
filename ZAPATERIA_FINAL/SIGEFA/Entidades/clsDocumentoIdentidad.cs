﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Entidades
{
    public class clsDocumentoIdentidad
    {

        private Int32 iCodDocumentoIdentidad;
        private Int32 iCodigoSunat;
        private Int32 iCodigoTipoDocumento;
        private String sDescripcion;
        private Int32 iLongitud;

        public int CodDocumentoIdentidad { get => iCodDocumentoIdentidad; set => iCodDocumentoIdentidad = value; }
        public int CodigoSunat { get => iCodigoSunat; set => iCodigoSunat = value; }
        public string Descripcion { get => sDescripcion; set => sDescripcion = value; }
        public int Longitud { get => iLongitud; set => iLongitud = value; }
        public int CodigoTipoDocumento { get => iCodigoTipoDocumento; set => iCodigoTipoDocumento = value; }
    }
}
