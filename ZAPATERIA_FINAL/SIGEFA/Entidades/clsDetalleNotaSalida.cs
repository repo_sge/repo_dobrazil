﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsDetalleNotaSalida
    {
        #region propiedades


        private Int32 iCodDetalleSalida;
        private Int32 iCodProducto;
        private String sReferencia;
        private String sDescripcion;
        private Int32 iCodNotaSalida;
        private Int32 iCodAlmacen;
        private Int32 iUnidadIngresada;
        private String sSerieLote;
        private DateTime dFechaSalida;
        private Decimal dCantidad;
        private Int32 iCodUnidad;
        private String sUnidad;
        private Decimal dPrecioUnitario;
        private Decimal dSubtotal;
        private Decimal dDescuento1;
        private Decimal dDescuento2;
        private Decimal dDescuento3;
        private Decimal dMontoDescuento;
        private Decimal dIgv;
        private Decimal dImporte;
        private Decimal dPrecioVenta;
        private Decimal dValorVenta;
        private Decimal dPrecioReal;
        private Decimal dValoReal;
        private DateTime dFechaRegistro;
        private Int32 iCodUser;
        private Int32 iCodVenta;
        private Int32 iCodCoti;
        private Int32 iCodLista;
        private Decimal dValorRealSoles;
        private Int32 iCodDetalleCotizacion;
        private Decimal dValorPromedio;
        private Decimal dValorPromedioSoles;
        private Decimal dCantidadPendiente;
        private int codTalla;

        public Int32 CodDetalleSalida
        {
            get { return iCodDetalleSalida; }
            set { iCodDetalleSalida = value; }
        }
        public Int32 CodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
        public String Referencia
        {
            get { return sReferencia; }
            set { sReferencia = value; }
        }
        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }
        public Int32 CodNotaSalida
        {
            get { return iCodNotaSalida; }
            set { iCodNotaSalida = value; }
        }
        public Int32 CodAlmacen
        {
            get { return iCodAlmacen; }
            set { iCodAlmacen = value; }
        }
        public Int32 UnidadIngresada
        {
            get { return iUnidadIngresada; }
            set { iUnidadIngresada = value; }
        }
        public String Unidad
        {
            get { return sUnidad; }
            set { sUnidad = value; }
        }
        public String SerieLote
        {
            get { return sSerieLote; }
            set { sSerieLote = value; }
        }
        public Decimal Cantidad
        {
            get { return dCantidad; }
            set { dCantidad = value; }
        }
        public Int32 CodUnidad
        {
            get { return iCodUnidad; }
            set { iCodUnidad = value; }
        }
        public Decimal PrecioUnitario
        {
            get { return dPrecioUnitario; }
            set { dPrecioUnitario = value; }
        }
        public Decimal Subtotal
        {
            get { return dSubtotal; }
            set { dSubtotal = value; }
        }
        public Decimal Descuento1
        {
            get { return dDescuento1; }
            set { dDescuento1 = value; }
        }
        public Decimal Descuento2
        {
            get { return dDescuento2; }
            set { dDescuento2 = value; }
        }
        public Decimal Descuento3
        {
            get { return dDescuento3; }
            set { dDescuento3 = value; }
        }
        public Decimal MontoDescuento
        {
            get { return dMontoDescuento; }
            set { dMontoDescuento = value; }
        }
        public Decimal Igv
        {
            get { return dIgv; }
            set { dIgv = value; }
        }
        public Decimal Importe
        {
            get { return dImporte; }
            set { dImporte = value; }
        }
        public Decimal PrecioVenta
        {
            get { return dPrecioVenta; }
            set { dPrecioVenta = value; }
        }
        public Decimal ValorVenta
        {
            get { return dValorVenta; }
            set { dValorVenta = value; }
        }
        public Decimal PrecioReal
        {
            get { return dPrecioReal; }
            set { dPrecioReal = value; }
        }
        public Decimal ValoReal
        {
            get { return dValoReal; }
            set { dValoReal = value; }
        }
        public DateTime FechaRegistro
        {
            get { return dFechaRegistro; }
            set { dFechaRegistro = value; }
        }
        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }

        public DateTime FechaSalida
        {
            get { return dFechaSalida; }
            set { dFechaSalida = value; }
        }

        public int CodVenta
        {
            get { return iCodVenta; }
            set { iCodVenta = value; }
        }

        public int CodCoti
        {
            get { return iCodCoti; }
            set { iCodCoti = value; }
        }

        public int CodLista
        {
            get { return iCodLista; }
            set { iCodLista = value; }
        }

        public Decimal ValorRealSoles
        {
            get { return dValorRealSoles; }
            set { dValorRealSoles = value; }
        }

        public int CodDetalleCotizacion
        {
            get { return iCodDetalleCotizacion; }
            set { iCodDetalleCotizacion = value; }
        }

        public Decimal ValorPromedio
        {
            get { return dValorPromedio; }
            set { dValorPromedio = value; }
        }

        public Decimal ValorPromedioSoles
        {
            get { return dValorPromedioSoles; }
            set { dValorPromedioSoles = value; }
        }

        public Decimal CantidadPendiente
        {
            get { return dCantidadPendiente; }
            set { dCantidadPendiente = value; }
        }

        public int CodTalla { get => codTalla; set => codTalla = value; }

        #endregion propiedades
    }
}
