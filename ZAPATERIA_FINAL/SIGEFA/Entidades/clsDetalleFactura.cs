﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsDetalleFactura
    {
        #region propiedades

        private Int32 iCodDetalleFactura;
        private Int32 iCodProducto;
        private Int32 iCodFactura;
        private String iCodNotaIngreso;
        private Int32 iCodAlmacen;
        private Int32 iUnidadIngresada;
        private Int32 iMoneda;
        private String sSerieLote;
        private Decimal dCantidad;
        private Int32 iCodUnidad;
        private Decimal dPrecioUnitario;
        private Decimal dSubtotal;
        private Decimal dDescuento1;
        private Decimal dDescuento2;
        private Decimal dDescuento3;
        private Decimal dMontoDescuento;
        private Decimal dIgv;
        private Decimal dFlete;
        private Decimal dImporte;
        private Decimal dPrecioReal;
        private Decimal dValoReal;
        private Boolean bEstado;
        private DateTime dFechaIngreso;
        private DateTime dFechaRegistro;
        private Int32 iCodUser;
        private Int32 coddetalleOrden;
        private clsTipoImpuesto tipoimpuesto;


        public Int32 CoddetalleOrden
        {
            get { return coddetalleOrden; }
            set { coddetalleOrden = value; }
        }

        public Int32 CodDetalleFactura
        {
            get { return iCodDetalleFactura; }
            set { iCodDetalleFactura = value; }
        }
        public Int32 CodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
       
        public Int32 CodAlmacen
        {
            get { return iCodAlmacen; }
            set { iCodAlmacen = value; }
        }
        public Int32 Moneda
        {
            get { return iMoneda; }
            set { iMoneda = value; }
        }
        public Int32 UnidadIngresada
        {
            get { return iUnidadIngresada; }
            set { iUnidadIngresada = value; }
        }
        public String SerieLote
        {
            get { return sSerieLote; }
            set { sSerieLote = value; }
        }
        public Decimal Cantidad
        {
            get { return dCantidad; }
            set { dCantidad = value; }
        }
        public Int32 CodUnidad
        {
            get { return iCodUnidad; }
            set { iCodUnidad = value; }
        }
        public Decimal PrecioUnitario
        {
            get { return dPrecioUnitario; }
            set { dPrecioUnitario = value; }
        }
        public Decimal Subtotal
        {
            get { return dSubtotal; }
            set { dSubtotal = value; }
        }
        public Decimal Descuento1
        {
            get { return dDescuento1; }
            set { dDescuento1 = value; }
        }
        public Decimal Descuento2
        {
            get { return dDescuento2; }
            set { dDescuento2 = value; }
        }
        public Decimal Descuento3
        {
            get { return dDescuento3; }
            set { dDescuento3 = value; }
        }
        public Decimal MontoDescuento
        {
            get { return dMontoDescuento; }
            set { dMontoDescuento = value; }
        }
        public Decimal Igv
        {
            get { return dIgv; }
            set { dIgv = value; }
        }
        public Decimal Flete
        {
            get { return dFlete; }
            set { dFlete = value; }
        }
        public Decimal Importe
        {
            get { return dImporte; }
            set { dImporte = value; }
        }
        public Decimal PrecioReal
        {
            get { return dPrecioReal; }
            set { dPrecioReal = value; }
        }
        public Decimal ValoReal
        {
            get { return dValoReal; }
            set { dValoReal = value; }
        }
        public Boolean Estado
        {
            get { return bEstado; }
            set { bEstado = value; }
        }
        public DateTime FechaIngreso
        {
            get { return dFechaIngreso; }
            set { dFechaIngreso = value; }
        }
        public DateTime FechaRegistro
        {
            get { return dFechaRegistro; }
            set { dFechaRegistro = value; }
        }
        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }
        public Int32 CodProveedor
        { get; set; }

        public String CodNotaIngreso
        {
            get { return iCodNotaIngreso; }
            set { iCodNotaIngreso = value; }
        }

        public int CodFactura
        {
            get { return iCodFactura; }
            set { iCodFactura = value; }
        }

        internal clsTipoImpuesto Tipoimpuesto { get => tipoimpuesto; set => tipoimpuesto = value; }

        #endregion propiedades

        #region IEquatable<clsDetalleFactura> Members

        public bool Equals(clsDetalleFactura other)
        {
            if (this.CodDetalleFactura == other.CodDetalleFactura && this.CodAlmacen == other.CodAlmacen)
                return true;
            return false;
        }

        public bool CodProEquals(Int32 codigo)
        {
            if (this.CodProducto == codigo)
                return true;
            return false;
        }

        #endregion
    }
}
